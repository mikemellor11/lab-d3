var Chart = require('chart');

describe('chart.js', function () {
	var myChart,
		selector;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
    });

    beforeEach(function(){
        fixture.load('chart.html');

        selector = document.getElementById('chart-1');
		myChart = new Chart(selector);
    });

    afterEach(function(){
        fixture.cleanup();
    });

    // ATTS
    it('height set to fixed amount', function () {
        myChart.att({
				width: selector.parentNode.clientWidth,
				height: 200
			}).init().render();

		expect(myChart.att().height).to.equal(200);
    });

    it('width set to fixed amount', function () {
        myChart.att({
				width: 200
			}).init().render();

		expect(myChart.att().width).to.equal(200);
    });

    it('height not set to fixed amount gets auto calculated', function () {
        myChart.att({
				width: 16
			}).init().render();

		expect(myChart.att().height).to.equal(9);
    });

    it('aspect ratio should change auto height', function () {
        myChart.att({
				width: 100,
				aspectRatio: 1
			}).init().render();

		expect(myChart.att().height).to.equal(100);
    });
});