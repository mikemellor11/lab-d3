var Chart = require('chart');

describe('chart.js - axis', function () {
	var myChart,
		selector;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
    });

    beforeEach(function(){
        fixture.load('chart.html');

        selector = document.getElementById('chart-1');
		myChart = new Chart(selector);
    });

    afterEach(function(){
        fixture.cleanup();
    });
});