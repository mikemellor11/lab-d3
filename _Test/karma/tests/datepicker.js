var Datepicker = require('charts/datepicker');

describe('datepicker.js', function () {
	var myChart;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
        data = fixture.load(
            '0level.json',
            '1level.json',
            '2level.json',
            '3level.json',
            'edge.json',
            '2levelalt.json',
            '2levelnokey.json',
            '2levelaltnokey.json'
        );
    });

    beforeEach(function(){
        fixture.load('chart.html');

		myChart = new Datepicker('#chart-3');
    });

    afterEach(function(){
        fixture.cleanup();
    });

    it('value set in data should determine start date', function () {
        myChart.data([{value: "2000-05-05"}])
        	.init();

		expect(myChart.store.picked.year).to.equal(2000);
		expect(myChart.store.picked.month).to.equal(5);
		expect(myChart.store.picked.day).to.equal(5);
    });

    it('if no value set in data when init called date should be todays date', function () {
        myChart.data([]).init();

		expect(myChart.store.picked.year).to.equal(new Date().getFullYear());
		expect(myChart.store.picked.month).to.equal(new Date().getMonth() + 1);
		expect(myChart.store.picked.day).to.equal(new Date().getDate());
    });

    it('', function () {
        myChart.data(data[1]).init().render();
    });
});