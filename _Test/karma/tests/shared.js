var Shared = require('shared');
var d3 = require('d3');

describe('shared.js', function () {
    var data,
        parseDate = d3.timeParse("%Y-%m-%d")
        formatDate = d3.timeFormat("%Y-%m-%d");

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
        data = fixture.load(
        	'0level.json',
        	'1level.json',
        	'2level.json',
        	'3level.json',
        	'edge.json'
    	);
    });

    beforeEach(function(){
    });

    afterEach(function(){
        fixture.cleanup();
    });

    // PROPERTYATSTRING
    it('propertyAtString should return correct property nested in object', function () {
    	expect(Shared.propertyAtString({"test": {"hello": 5}}, "test.hello")).to.be.equal(5);
    	expect(Shared.propertyAtString([{"test": {"hello": 5}}], "[0].test.hello")).to.be.equal(5);
    	expect(Shared.propertyAtString([{"test": [{"hello": 5}]}], "[0].test[0].hello")).to.be.equal(5);
    });

    it('propertyAtString should return undefined if property doesnt exist', function () {
    	expect(Shared.propertyAtString([{"test": [{"hello": 5}]}], "[0].test[0].fake")).to.be.equal(undefined);
    });

    // FORMATKEYS
    it('formatKeys should return string with argument injected into string', function () {
    	expect(Shared.formatKeys('hello {test} goodbye', {test: 'you'})).to.be.equal('hello you goodbye');
    });

    it('formatKeys should return string with multiple arguments injected into string', function () {
    	expect(Shared.formatKeys('hello {test} goodbye {asdf}', {test: 'you', asdf: 'me'})).to.be.equal('hello you goodbye me');
    });

    it('formatKeys should return missing key in string if not found in keys passed in', function () {
    	expect(Shared.formatKeys('hello {test} goodbye {asdf} {fake}', {test: 'you', asdf: 'me'})).to.be.equal('hello you goodbye me {fake}');
    });

    // ROUNDEDRECT
    it('roundedRect should return a path as a string with 0 rounded corners', function () {
    	expect(Shared.roundedRect(0, 0, 10, 10, 0, 0, 0, 0)).to.be.equal('M0 0 L10 0 Q10 0 10 0 L10 10 Q10 10 10 10 L0 10 Q0 10 0 10 L0 0 Q0 0 0 0 Z');
    });

    it('roundedRect should return a path as a string with rounded corners of 5 pixels', function () {
    	expect(Shared.roundedRect(0, 0, 10, 10, 5, 5, 5, 5)).to.be.equal('M5 0 L5 0 Q10 0 10 5 L10 5 Q10 10 5 10 L5 10 Q0 10 0 5 L0 5 Q0 0 5 0 Z');
    });

    it('roundedRect should return null if incorrect number of params passed in', function () {
    	expect(Shared.roundedRect()).to.be.equal(null);
    });

    // X
    it('x should return value if no plot specified', function () {
    	expect(Shared.x(data[1][0], 'value')).to.be.equal(10);
    	expect(Shared.x(data[4][0].values[1], 'x')).to.be.equal(30);
    });

    // Y
    it('x should return value if no plot specified', function () {
    	expect(Shared.y(data[1][0], 'value')).to.be.equal(10);
    	expect(Shared.y(data[4][0].values[0], 'y')).to.be.equal(20);
    });

    // MIN
    it('min should return lowest value with plot value given', function () {
        expect(Shared.min(data[1], 'x', 'value')).to.be.equal(10);
        expect(formatDate(Shared.min(data[2], 'x', 'value', parseDate))).to.be.equal("2016-06-05");
        expect(formatDate(Shared.min(data[3], 'x', 'value', parseDate))).to.be.equal("2017-06-05");
        expect(Shared.min(data[4], 'x', 'x')).to.be.equal(10);
        expect(Shared.min(data[4], 'y', 'y')).to.be.equal(20);
    });

    // MAX
    it('max should return highest value with plot value given', function () {
        expect(Shared.max(data[1], 'x', 'value')).to.be.equal(20);
        expect(formatDate(Shared.max(data[2], 'x', 'value', parseDate))).to.be.equal("2018-09-05");
        expect(formatDate(Shared.max(data[3], 'x', 'value', parseDate))).to.be.equal("2018-09-25");
        expect(Shared.max(data[4], 'x', 'x')).to.be.equal(60);
        expect(Shared.max(data[4], 'y', 'y')).to.be.equal(70);
    });

    // FLATTENVALUES
    it('flattenValues should return a single level array', function () {
        expect(Shared.flattenValues([])).to.be.deep.equal([[], 0]);
    	expect(Shared.flattenValues(data[1])).to.be.deep.equal([data[1], 0]);
    	expect(Shared.flattenValues(data[2])).to.be.deep.equal([[
    			{
                    "key": 0,
                    "label": "0",
                    "value": ["2017-06-05", "2017-08-05"],
                    "minmax": ["2017-06-05", "2017-09-05"]
                },
                {
                    "key": 1,
                    "label": "1",
                    "value": ["2017-07-05", "2017-09-05"],
                    "minmax": ["2016-06-05", "2017-09-05"]
                },
                {
	                "key": 0,
	                "label": "0",
	                "value": ["2018-06-05", "2018-08-05"]
	            },
	            {
	                "key": 1,
	                "label": "1",
	                "value": ["2018-07-05", "2018-09-05"]
	            }
    		], 1]);
    	expect(Shared.flattenValues(data[3])).to.be.deep.equal([[
    				{
                        "key": 0,
                        "label": "0",
                        "value": ["2017-06-05", "2017-08-05"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2017-07-05", "2017-09-05"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2018-06-05", "2018-08-05"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2018-07-05", "2018-09-05"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2017-06-15", "2017-08-15"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2017-07-15", "2017-09-15"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2018-06-25", "2018-08-25"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2018-07-25", "2018-09-25"]
                    }
    		], 2]);

		expect(Shared.flattenValues(data[4])).to.be.deep.equal([[
				{
	                "label": "0",
	                "x": 10,
	                "y": 20
	            },
	            {
	                "label": "1",
	                "x": 30,
	                "y": 30
	            },
	            {
	                "label": "0",
	                "x": 40,
	                "y": 50
	            },
	            {
	                "label": "1",
	                "x": 60,
	                "y": 70
	            }
            ], 1]);
    });

    // FLATTENVALUESLENGTHS
    it('flattenValuesLengths should return a an array of level lengths', function () {
        expect(Shared.flattenValuesLengths([])).to.be.deep.equal([]);
    	expect(Shared.flattenValuesLengths(data[1])).to.be.deep.equal([]);
    	expect(Shared.flattenValuesLengths(data[2])).to.be.deep.equal([
	    		[
	    			{
                        "key": 0,
                        "label": "0",
                        "value": ["2017-06-05", "2017-08-05"],
                        "minmax": ["2017-06-05", "2017-09-05"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2017-07-05", "2017-09-05"],
                        "minmax": ["2016-06-05", "2017-09-05"]
                    },
	                {
		                "key": 0,
		                "label": "0",
		                "value": ["2018-06-05", "2018-08-05"]
		            },
		            {
		                "key": 1,
		                "label": "1",
		                "value": ["2018-07-05", "2018-09-05"]
		            }
	    		]
    		]);
    	expect(Shared.flattenValuesLengths(data[3])).to.be.deep.equal([
    			[
    				{
                        "key": 0,
                        "label": "0",
                        "value": ["2017-06-05", "2017-08-05"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2017-07-05", "2017-09-05"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2018-06-05", "2018-08-05"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2018-07-05", "2018-09-05"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2017-06-15", "2017-08-15"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2017-07-15", "2017-09-15"]
                    },
                    {
                        "key": 0,
                        "label": "0",
                        "value": ["2018-06-25", "2018-08-25"]
                    },
                    {
                        "key": 1,
                        "label": "1",
                        "value": ["2018-07-25", "2018-09-25"]
                    }
    			],
    			[
    				{
		                "key": 0,
		                "label": "0",
		                "values": [
		                    {
		                        "key": 0,
		                        "label": "0",
		                        "value": ["2017-06-05", "2017-08-05"]
		                    },
		                    {
		                        "key": 1,
		                        "label": "1",
		                        "value": ["2017-07-05", "2017-09-05"]
		                    }
		                ]
		            },
		            {
		                "key": 1,
		                "label": "1",
		                "values": [
		                    {
		                        "key": 0,
		                        "label": "0",
		                        "value": ["2018-06-05", "2018-08-05"]
		                    },
		                    {
		                        "key": 1,
		                        "label": "1",
		                        "value": ["2018-07-05", "2018-09-05"]
		                    }
		                ]
		            },
		            {
		                "key": 0,
		                "label": "0",
		                "values": [
		                    {
		                        "key": 0,
		                        "label": "0",
		                        "value": ["2017-06-15", "2017-08-15"]
		                    },
		                    {
		                        "key": 1,
		                        "label": "1",
		                        "value": ["2017-07-15", "2017-09-15"]
		                    }
		                ]
		            },
		            {
		                "key": 1,
		                "label": "1",
		                "values": [
		                    {
		                        "key": 0,
		                        "label": "0",
		                        "value": ["2018-06-25", "2018-08-25"]
		                    },
		                    {
		                        "key": 1,
		                        "label": "1",
		                        "value": ["2018-07-25", "2018-09-25"]
		                    }
		                ]
		            }
    			]
    		]);
		expect(Shared.flattenValuesLengths(data[4])).to.be.deep.equal([
				[
					{
		                "label": "0",
		                "x": 10,
		                "y": 20
		            },
		            {
		                "label": "1",
		                "x": 30,
		                "y": 30
		            },
		            {
		                "label": "0",
		                "x": 40,
		                "y": 50
		            },
		            {
		                "label": "1",
		                "x": 60,
		                "y": 70
		            }
	            ]
			]);
    });

    // FINDKEY
    it('findKey should return false if no object with key found in array', function () {
    	expect(Shared.findKey([{test: 'asdf'}, {asdf: 'asdf'}, {zxcv: 'wert'}], 'fake')).to.be.equal(false);
    });

    it('findKey should return true if an object with the key found', function () {
    	expect(Shared.findKey([{test: 'asdf'}, {asdf: 'asdf'}, {zxcv: 'wert'}], 'asdf')).to.be.equal(true);
    });
});