var Chart = require('chart');

describe('chart.js - scale', function () {
	var myChart,
		selector;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
    });

    beforeEach(function(){
        fixture.load('chart.html');

        selector = document.getElementById('chart-1');
		myChart = new Chart(selector);
    });

    afterEach(function(){
        fixture.cleanup();
    });

    // SCALE
    var shared = {
		width: 100,
		height: 100,
		margin: {
			left: 0,
			right: 0,
			top: 0,
			bottom: 0
		},
		autoAxis: null,
		min: {
			x: 0,
			y: 0
		},
		max: {
			x: 10,
			y: 10
		}
	};

	// // LINEAR
    it('linear y scale should return correct positions', function () {
        myChart.att(shared).init().render();

		expect(myChart.store.scale.y(0)).to.equal(100);
		expect(myChart.store.scale.y(10)).to.equal(0);
		expect(myChart.store.scale.y(5)).to.equal(50);
    });

    it('linear x scale should return correct positions', function () {
        myChart.att(shared).att({
        		scale: {
					x: 'linear'
				}
        	}).init().render();

		expect(myChart.store.scale.x(0)).to.equal(0);
		expect(myChart.store.scale.x(10)).to.equal(100);
		expect(myChart.store.scale.x(5)).to.equal(50);
    });

    it('reversed linear y scale should return correct positions', function () {
        myChart.att(shared).att({
        		axis: {
        			y: {
        				reverse: true
        			}
        		}
        	}).init().render();

		expect(myChart.store.scale.y(10)).to.equal(100);
		expect(myChart.store.scale.y(0)).to.equal(0);
    });

    it('reversed linear x scale should return correct positions', function () {
        myChart.att(shared).att({
        		axis: {
        			x: {
        				reverse: true
        			}
        		},
        		scale: {
					x: 'linear'
				}
        	}).init().render();

		expect(myChart.store.scale.x(10)).to.equal(0);
		expect(myChart.store.scale.x(0)).to.equal(100);
    });

    // // DATE
    it('date y scale should return correct positions', function () {
    	var parseDate = myChart.store.att.parseDate;

        myChart.att(shared).att({
        		scale: {
					y: 'date'
				},
				min: {
					y: "2000-01-01"
				},
				max: {
					y: "2000-01-03"
				}
        	}).init().render();

		expect(myChart.store.scale.y(parseDate("2000-01-01"))).to.equal(100);
		expect(myChart.store.scale.y(parseDate("2000-01-03"))).to.equal(0);
		expect(myChart.store.scale.y(parseDate("2000-01-02"))).to.equal(50);
    });

    it('date x scale should return correct positions', function () {
    	var parseDate = myChart.store.att.parseDate;

        myChart.att(shared).att({
        		scale: {
					x: 'date'
				},
				min: {
					x: "2000-01-01"
				},
				max: {
					x: "2000-01-03"
				}
        	}).init().render();

		expect(myChart.store.scale.x(parseDate("2000-01-01"))).to.equal(0);
		expect(myChart.store.scale.x(parseDate("2000-01-03"))).to.equal(100);
		expect(myChart.store.scale.x(parseDate("2000-01-02"))).to.equal(50);
    });
});