var Gantt = require('charts/gantt');

describe('gantt.js', function () {
    var myChart, 
    	data;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
        data = fixture.load(
            '0level.json',
            '1level.json',
            '2level.json',
            '3level.json',
            'edge.json',
            '2levelalt.json',
            '2levelnokey.json',
            '2levelaltnokey.json'
        );
    });

    beforeEach(function(){
        fixture.load('chart.html');

		myChart = new Gantt('#chart-1');
        myChart.att({
            itemSize: 10,
            padding: {
                outer: 10
            }
        });
    });

    afterEach(function(){
        fixture.cleanup();
    });

    it('y should return the correct y position based on index and minmax being present', function () {
        expect(myChart.y(data[1], 0)).to.be.equal(0);
        expect(myChart.y(data[1], 1)).to.be.equal(40);
        expect(myChart.y(data[1], 2)).to.be.equal(60);

        expect(myChart.y(data[2], 0)).to.be.equal(0);
        expect(myChart.y(data[2], 1)).to.be.equal(100);
        expect(myChart.y(data[2], 2)).to.be.equal(160);

        expect(myChart.y(data[3], 0)).to.be.equal(0);
        expect(myChart.y(data[3], 1)).to.be.equal(140);
        expect(myChart.y(data[3], 2)).to.be.equal(280);

        expect(myChart.y(data[5], 0)).to.be.equal(0);
        expect(myChart.y(data[5], 1)).to.be.equal(40);
        expect(myChart.y(data[5], 2)).to.be.equal(100);
        expect(myChart.y(data[5], 3)).to.be.equal(180);
    });
});