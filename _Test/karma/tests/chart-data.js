var Chart = require('chart');

describe('chart.js - data', function () {
	var myChart,
		selector,
		select = document.querySelectorAll.bind(document);

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
        data = fixture.load(
        	'0level.json',
        	'1level.json',
        	'2level.json',
        	'3level.json',
        	'edge.json',
        	'2levelalt.json',
        	'2levelnokey.json',
        	'2levelaltnokey.json'
    	);
    });

    beforeEach(function(){
        fixture.load('chart.html');

        selector = document.getElementById('chart-1');
		myChart = new Chart(selector);
		myChart.att({
			transitionSpeed: 0
		});
    });

    afterEach(function(){
        fixture.cleanup();
    });

    it('single level data should have 1 level of svg groups and no level above', function () {
        myChart.data(data[1]).init().render();

		expect(select('.level--0').length).to.equal(2);
		expect(select('.level--1').length).to.equal(0);
    });

    it('double level data should have 2 level of svg groups and no level above', function () {
        myChart.data(data[2]).init().render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(2);
    });

    it('triple level data should have 3 level of svg groups and no level above', function () {
        myChart.data(data[3]).init().render();

		expect(select('.level--0').length).to.equal(8);
		expect(select('.level--1').length).to.equal(4);
		expect(select('.level--2').length).to.equal(2);
    });

    it('data with missing values should add an empty level--$--hold to svg but no levels within', function () {
        myChart.data(data[4]).init().render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(3);
		expect(select('.level--1--hold')[0].childNodes.length).to.equal(2);
		expect(select('.level--1--hold')[1].childNodes.length).to.equal(0);
		expect(select('.level--1--hold')[2].childNodes.length).to.equal(2);
    });

    it('data changed to another should add and remove the respective data sets', function () {
        myChart.data(data[2]).init().render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(2);

		myChart.data(data[5]).render();

		expect(select('.level--0').length).to.equal(6);
		expect(select('.level--1').length).to.equal(3);

		myChart.data(data[2]).render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(2);
    });

    it('data changed to another should maintain position with key', function () {
        myChart.data(data[2]).init().render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(2);

		myChart.data(data[5]).render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(1);
		expect(select('.level--1')[2].querySelectorAll('.level--0').length).to.equal(3);

		myChart.data(data[2]).render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(2);
    });

    it('data changed to another should be position by index without a key', function () {
        myChart.data(data[6]).init().render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(2);

		myChart.data(data[7]).render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(1);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[2].querySelectorAll('.level--0').length).to.equal(3);

		myChart.data(data[6]).render();

		expect(select('.level--1')[0].querySelectorAll('.level--0').length).to.equal(2);
		expect(select('.level--1')[1].querySelectorAll('.level--0').length).to.equal(2);
    });

    it('data level changes should correctly remove everything from draw and start again', function () {
		myChart.data(data[2]).init().render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(2);

		myChart.data(data[1]).render();

		expect(select('.level--0').length).to.equal(2);
		expect(select('.level--1').length).to.equal(0);

		myChart.data(data[2]).render();

		expect(select('.level--0').length).to.equal(4);
		expect(select('.level--1').length).to.equal(2);
    });
});