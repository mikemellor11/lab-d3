var Brush = require('charts/brush');

describe('test', function () {
    var myChart;

    before(function(){
        fixture.setBase('_Test/karma/fixtures');
        data = fixture.load(
            '0level.json',
            '1level.json',
            '2level.json',
            '3level.json',
            'edge.json',
            '2levelalt.json',
            '2levelnokey.json',
            '2levelaltnokey.json'
        );
    });

    beforeEach(function(){
        fixture.load('chart.html');

		myChart = new Brush('#chart-1');
    });

    afterEach(function(){
        fixture.cleanup();
    });

    it('', function () {
        myChart.init().render();
    });
});