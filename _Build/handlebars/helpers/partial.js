var hb = require('handlebars');

module.exports = function (partial, context) {
	var output;

	if(partial.fn){
		output = hb.compile(hb.Utils.escapeExpression(partial.fn(this)));
	} else {
		if(typeof hb.partials[partial] !== 'string'){
			return hb.partials[partial]();
		}

		output = hb.compile(hb.partials[partial]);
	}
	    
    return output(context);
};