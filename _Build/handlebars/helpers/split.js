module.exports = function (value, split, index, option) {
	return (option) ? value.split(split)[index] : value.split(split);
};