(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(
			require('d3'), 
			require('shared'), 
			require('chart')
		);
	} else {
		root.Chart_Map = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (
		d3, 
		Shared, 
		Chart
	) {
	"use strict";

	function Chart_Map(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Chart_Map)) { 
			return new Chart_Map(selector);
		}

		Chart.call(this, selector);

		this.store.chart.each(function(){
			d3.select(this).classed('labD3__chart_map', true);
		});

		var that = this;

		Shared.extend(this.store, {
			lastTextFormat: null,
			geomWidth: 0,
			geomHeight: 0,
			geometry: null,
			path: null,
			zoom: null,
			activated: [],
			active: null,
			clicked: function(d) {
				var local = that.store, att = local.att, data = local.data, chart = local.chart;

				if(local.active){
					// RESET
					if(local.active.id === d.id){
						if(att.zoom.click){
							local.reset(this, d);
						} else {
							// RESET WITHOUT THE ZOOMING OUT THEN RETURN
							d3.selectAll('.feature-' + local.active.id).classed("active", false);

							for(var i = 0; i < local.activated.length; i++){
								if(local.activated[i].id === local.active.id){
									local.activated.splice(i, 1);
									break;
								}
							}
							local.active = null;

							if(att.callback.reset){
								att.callback.reset(this, d);
							}
						}
						return;
					// RESET WITH THE INTENTION OF ZOOMING ON NEW
					} else {
						// IF MULTICLICK DONT REMOVE ACTIVATED AREAS
						if(!att.multiclick){
							d3.selectAll('.feature-' + local.active.id).classed('active', false);
							local.active = null;
							local.activated = [];
						// NO ACTIVE IF CLICKED IS ACTIVATED REMOVE AND RETURN
						} else {
							for(i = 0; i < local.activated.length; i++){
								if(local.activated[i].id === d.id){
									d3.selectAll('.feature-' + d.id).classed("active", false);
									local.activated.splice(i, 1);

									if(att.callback.reset){
										att.callback.reset(this, d);
									}

									return;
								}
							}
						}
					}
				} else if(local.activated.length){
					// NO ACTIVE BUT STILL ACTIVE ELEMENTS NEED TO CHECK IF ITS A REMOVE CHECK
					for(i = 0; i < local.activated.length; i++){
						if(local.activated[i].id === d.id){
							d3.selectAll('.feature-' + local.activated[i].id).classed("active", false);
							local.activated.splice(i, 1);

							if(att.callback.reset){
								att.callback.reset(this, d);
							}

							return;
						}
					}
				}

				local.active = d;
				local.activated.push(d);

				d3.selectAll('.feature-' + local.active.id).classed("active", true);

				if(att.zoom.click){
					var bounds = local.path.bounds(d);

					if(d.properties.offset && d.properties.offset.anchor){
						var center = local.path.centroid(d);
						if(!isNaN(center[0])){
							if(d.properties.offset){
								center[0] += (d.properties.offset.x * local.geomWidth) || 0;
								center[1] += (d.properties.offset.y * local.geomHeight) || 0;
							}
						}

						if((center[0] - att.anchorRadius) < bounds[0][0]){
							bounds[0][0] = center[0] - att.anchorRadius;
						}

						if((center[1] - att.anchorRadius) < bounds[0][1]){
							bounds[0][1] = center[1] - att.anchorRadius;
						}

						if((center[0] + att.anchorRadius) > bounds[1][0]){
							bounds[1][0] = center[0] + att.anchorRadius;
						}

						if((center[1] + att.anchorRadius) > bounds[1][1]){
							bounds[1][1] = center[1] + att.anchorRadius;
						}
					}

					var dx = bounds[1][0] - bounds[0][0],
						dy = bounds[1][1] - bounds[0][1],
						x = (bounds[0][0] + bounds[1][0]) / 2,
						y = (bounds[0][1] + bounds[1][1]) / 2,
						scale = Math.max(
								1, 
								Math.min(
										8, 
										0.9 / Math.max(dx / local.width, dy / local.height)
									)
							),
						translate = [local.width / 2 - scale * x, local.height / 2 - scale * y];

					var anim = local.chart;

			    	if(att.transitionSpeed || att.delaySpeed){
						anim = anim.transition()
							.ease(d3['ease' + att.transitionType])
							.duration(att.transitionSpeed);
			    	} else {
			    		anim.interrupt();
			    	}

					anim.call(local.zoom.transform, d3.zoomIdentity.translate(translate[0],translate[1]).scale(scale));
				}

				if(att.callback.clicked){
					att.callback.clicked(this, d);
				}
			},
			reset: function(node, d) {
				var local = that.store, att = local.att, data = local.data, chart = local.chart;

				if(local.active){
					d3.selectAll('.feature-' + local.active.id).classed('active', false);

					for(var i = 0; i < local.activated.length; i++){
						if(local.activated[i].id === local.active.id){
							local.activated.splice(i, 1);
						}
					}
					local.active = null;
				}

				var anim = local.chart;

		    	if(att.transitionSpeed || att.delaySpeed){
					anim = anim.transition()
						.ease(d3['ease' + att.transitionType])
						.duration(att.transitionSpeed);
		    	} else {
		    		anim.interrupt();
		    	}

				anim.call(local.zoom.transform, d3.zoomIdentity);

				if(att.callback.reset){
					att.callback.reset(node, d);
				}
			},
			resetAll: function(){
				var local = that.store, att = local.att, data = local.data, chart = local.chart;

				for(var i = 0; i < local.activated.length; i++){
					d3.selectAll('.feature-' + local.activated[i].id).classed("active", false);
				}

				local.activated = [];
				local.active = null;

				this.reset();
			},
			zoomed: function() {
				var local = that.store, att = local.att, data = local.data, chart = local.chart;

				local.geometry.style("stroke-width", 1 / d3.event.transform.k + "px");
				local.geometry.selectAll('text').style("font-size", 16 / d3.event.transform.k + "px");
				local.geometry.selectAll('text').attr("dy", ((16 / d3.event.transform.k) * 0.4) + "px");
				local.geometry.attr("transform", d3.event.transform);
			}
		});

		this.att({
			scale: {
				x: null,
				y: null
			},
			margin: {
				top: 0,
				left: 0,
				right: 0,
				bottom: 0
			},
			textFormat: "{id}",
			threshhold: {
				instant: false,
				upper: 0.8,
				middle: 0.6,
				lower: 0
			},
			hide: {
				empty: true,
				labels: false
			},
			multiclick: false,
			callback: {
				reset: null,
				clicked: null
			},
			zoom: {
				free: true,
				click: true
			},
			anchorRadius: 15
		});
	}

	Chart_Map.prototype = Object.create(Chart.prototype);

	Chart_Map.prototype.init = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		if(local.inited){
			return this;
		}

		Chart.prototype.init.call(this);

		local.path = d3.geoPath().projection(local.projection);

		local.zoom = d3.zoom()
			.scaleExtent([1, 8])
			.on("zoom", local.zoomed);

		if(att.zoom.free){
			chart.call(local.zoom)
				.on("dblclick.zoom", null);
		}

		local.geometry = local.draw.append('g')
			.attr('class', 'geometry');

		return this;
	};

	Chart_Map.prototype.render = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		local.margin.left = att.margin.left;
		local.margin.bottom = att.margin.bottom;

		local.height = att.height - att.margin.top - local.margin.bottom;
		local.width = att.width - local.margin.left - att.margin.right;

		local.bottom.attr("transform", "translate(" + local.margin.left + "," + att.margin.top + ")");
		local.draw.attr("transform", "translate(" + local.margin.left + "," + att.margin.top + ")");
		local.top.attr("transform", "translate(" + local.margin.left + "," + att.margin.top + ")");

		local.zoom.translateExtent([[0, 0], [local.width, local.height]]);

		local.projection.fitSize([local.width, local.height], local.geoJson);

		var selection = local.geometry.selectAll("path")
			.data(local.geoJson.features);

		var path = selection.enter().append('path')
			.attr('class', function(d){
				return 'feature feature-' + d.id;
			})
			.on('click', local.clicked)
			.merge(selection);

		selection = local.geometry.selectAll(".label")
			.data(local.geoJson.features);

		var label = selection.enter().append('g')
			.attr('class', function(d){
				return 'label feature-' + d.id;
			});

		label.append('line');

		label.append('circle')
			.attr('r', att.anchorRadius);

		label.append('text')
			.attr('dy', (16 * 0.4) + 'px')
			.attr('class', function(d){
				if(d.properties.offset && d.properties.offset.anchor){
					return 'anchor';
				}
			})
			.attr('display', function(d, i){
				if(isNaN(local.path.centroid(d)[0])){
					return 'none';
				}
			});

		label = label.merge(selection);

		path.attr("d", local.path)
			.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
			.tween("text", function(d, i) {
	            var that = this;
	            var _that = d3.select(this);

	            _that.classed('missing', !data[d.id]);

	            if(att.threshhold.instant){
	            	var value = parseFloat(data[d.id] && data[d.id][att.plot.value] || 0);
	            	var total = att.totalCount;
	            	var normal = value / total;

	            	local.dataLast.set(that, value);

	            	if(normal > att.threshhold.upper){
	            		_that.classed('lower middle', false).classed('upper', true);
	            	} else if(normal > att.threshhold.middle) {
	            		_that.classed('lower upper', false).classed('middle', true);
	            	} else if(normal > att.threshhold.lower){
	            		_that.classed('middle upper', false).classed('lower', true);
	            	} else {
	            		_that.classed('lower middle upper', false);
	            	}

	            	return null;
	            } else {
	            	var j = d3.interpolate(
	            		local.dataLast.get(this) || 0, 
	            		data[d.id] && data[d.id][att.plot.value] || 0
            		);

		            return function(t) {
		            	var value = parseFloat(j(t));
		            	var total = att.totalCount;
		            	var normal = value / total;

		            	local.dataLast.set(that, value);

		            	if(normal > att.threshhold.upper){
		            		_that.classed('lower middle', false).classed('upper', true);
		            	} else if(normal > att.threshhold.middle) {
		            		_that.classed('lower upper', false).classed('middle', true);
		            	} else if(normal > att.threshhold.lower){
		            		_that.classed('middle upper', false).classed('lower', true);
		            	} else {
		            		_that.classed('lower middle upper', false);
		            	}
		            };
	            }
	        });

		var bounds = local.path.bounds(local.geoJson);

		local.geomHeight = bounds[1][1] - bounds[0][1];
		local.geomWidth = bounds[1][0] - bounds[0][0];

		label.select('text')
			.attr('transform', function(d, i){
				var center = local.path.centroid(d);
				if(!isNaN(center[0])){
					if(d.properties.offset){
						center[0] += (d.properties.offset.x * local.geomWidth) || 0;
						center[1] += (d.properties.offset.y * local.geomHeight) || 0;
					}

					return "translate(" + center + ")";
				}
			});

		var text = label.select('text');

		text = text.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
			.tween("text", function(d, i) {
	            var that = this;
	            var _that = d3.select(that.parentNode);

	            _that.classed('missing', !data[d.id]);

	            if(att.threshhold.instant){
	            	var value = parseFloat(data[d.id] && data[d.id][att.plot.value] || 0);
	            	var total = att.totalCount;
	            	var normal = value / total;

	                if(normal > att.threshhold.upper){
	            		_that.classed('lower middle', false).classed('upper', true);
	            	} else if(normal > att.threshhold.middle) {
	            		_that.classed('lower upper', false).classed('middle', true);
	            	} else if(normal > att.threshhold.lower){
	            		_that.classed('middle upper', false).classed('lower', true);
	            	} else {
	            		_that.classed('lower middle upper', false);
	            	}
	            }

	            var j = d3.interpolate(
	            		local.dataLast.get(this) || 0, 
	            		data[d.id] && data[d.id][att.plot.value] || 0
            		);

	            return function(t) {
	            	var value = parseFloat(j(t));
	            	var total = att.totalCount;
	            	var normal = value / total;
	            	var percent = Math.round(normal * 100);

	            	if(!att.threshhold.instant){
		            	if(normal > att.threshhold.upper){
		            		_that.classed('lower middle', false).classed('upper', true);
		            	} else if(normal > att.threshhold.middle) {
		            		_that.classed('lower upper', false).classed('middle', true);
		            	} else if(normal > att.threshhold.lower){
		            		_that.classed('middle upper', false).classed('lower', true);
		            	} else {
		            		_that.classed('lower middle upper', false);
		            	}
	            	}

	            	local.dataLast.set(that, value);

	            	var textObject = {
	            		value: value.toFixed(att.decimalPlaces),
	            		percent: percent,
	            		total: total,
	            		id: d.id,
	            		name: d.properties.name
	            	};

	                that.textContent = Shared.formatKeys(att.textFormat, textObject);
	            };
	        })
	        .attr('opacity', function(d, i){
				if(att.hide.labels){
					return 0;
				} else {
					if(isNaN(local.path.centroid(d)[0])){
						return 0;
					}

					if(local.flatData.length && (att.hide.empty && !data[d.id])){
						return 0;
					}
				}

				return 1;
			});

        var filter = label.filter(function(d) { return !(d.properties.offset && d.properties.offset.anchor); });

        filter.select('line')
    		.remove();

		filter.select('circle')
    		.remove();

        label.select('line')
        	.attr('x1', function(d){
        		var center = local.path.centroid(d);
        		
				if(!isNaN(center[0])){
					return center[0];
				}

        		return 0;
        	})
        	.attr('y1', function(d){
        		var center = local.path.centroid(d);
        		
				if(!isNaN(center[1])){
					return center[1];
				}

        		return 0;
        	})
        	.attr('x2', function(d){
        		var center = local.path.centroid(d);

				if(!isNaN(center[0])){
					if(d.properties.offset){
						center[0] += (d.properties.offset.x * local.geomWidth) || 0;
					}

					return center[0];
				}

        		return 0;
        	})
        	.attr('y2', function(d){
        		var center = local.path.centroid(d);

				if(!isNaN(center[1])){
					if(d.properties.offset){
						center[1] += (d.properties.offset.y * local.geomHeight) || 0;
					}

					return center[1];
				}

				return 0;
        	})
        	.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
        	.attr('opacity', function(d, i){
				if(att.hide.labels){
					return 0;
				} else {
					if(isNaN(local.path.centroid(d)[0])){
						return 0;
					}

					if(local.flatData.length && (att.hide.empty && !data[d.id])){
						return 0;
					}
				}

				return 1;
			});

    	label.select('circle')
    		.on('click', local.clicked)
        	.attr('cx', function(d){
        		var center = local.path.centroid(d);
				if(!isNaN(center[0])){
					if(d.properties.offset){
						center[0] += (d.properties.offset.x * local.geomWidth) || 0;
					}

					return center[0];
				}
        		return 0;
        	})
        	.attr('cy', function(d){
        		var center = local.path.centroid(d);
				if(!isNaN(center[1])){
					if(d.properties.offset){
						center[1] += (d.properties.offset.y * local.geomHeight) || 0;
					}

					return center[1];
				}
        		return 0;
        	})
        	.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
			.attr('r', att.anchorRadius)
    		.attr('opacity', function(d, i){
				if(att.hide.labels){
					return 0;
				} else {
					if(isNaN(local.path.centroid(d)[0])){
						return 0;
					}

					if(local.flatData.length && (att.hide.empty && !data[d.id])){
						return 0;
					}
				}

				return 1;
			});

		local.lastTextFormat = att.textFormat;

		chart.each(function(){
			this.dispatchEvent(local.rendered);
		});

		return this;
	};

	Chart_Map.prototype.resize = function (value) {
		var local = this.store, att = local.att;

		Chart.prototype.resize.call(this);

		if(att.zoom.click && local.active){
			this.renderSync();

			var d = local.active;

			var bounds = local.path.bounds(d),
				dx = bounds[1][0] - bounds[0][0],
				dy = bounds[1][1] - bounds[0][1],
				x = (bounds[0][0] + bounds[1][0]) / 2,
				y = (bounds[0][1] + bounds[1][1]) / 2,
				scale = Math.max(
						1, 
						Math.min(
								8, 
								0.9 / Math.max(dx / local.width, dy / local.height)
							)
					),
				translate = [local.width / 2 - scale * x, local.height / 2 - scale * y];

			local.chart.call(
				local.zoom.transform, 
				d3.zoomIdentity.translate(
						translate[0],
						translate[1]
					)
				.scale(scale));
		} else {
			if(local.inited){
				var save = {
					transitionSpeed: att.transitionSpeed,
					delaySpeed: att.delaySpeed
				};

				this.att({
					transitionSpeed: 0,
					delaySpeed: 0	
				});

				local.reset();

				this.att(save);
			}
		}

		return this;
	};

	Chart_Map.prototype.reset = function(node, d){
		this.store.reset(node, d);
	};

	Chart_Map.prototype.resetAll = function(){
		this.store.resetAll();
	};

	Chart_Map.prototype.resetSpecific = function(key){
		var local = this.store, att = local.att;

		for(var i = 0; i < local.activated.length; i++){
			if(local.activated[i].id === key){
				d3.selectAll('.feature-' + local.activated[i].id).classed("active", false);
				local.activated.splice(i, 1);

				if(att.callback.reset){
					att.callback.reset();
				}
			}
		}
	};

	Chart_Map.prototype.data = function(value){
		Chart.prototype.data.call(this, value);

		this.store.data = d3.nest()
			.key(function(d) { return d.key; })
			.rollup(function(d){ return d[0]; })
			.object(this.store.flatData);

		return this;
	};

	return Chart_Map;
});