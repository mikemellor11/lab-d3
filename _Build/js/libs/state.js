var ls = window.localStorage;

module.exports = exports = {
    get: function(name){
        return JSON.parse(ls.getItem('labd3' + name));
    },
    set: function(name, value){
        ls.setItem('labd3' + name, value);
    },
    toggle: function(name){
        var flip = !this.get(name);
        ls.setItem('labd3' + name, flip);
        return flip;
    },
    reset: function(){
        ls.clear();
    }
};