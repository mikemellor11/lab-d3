function createMap(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 0, right: 0, bottom: 0, left: 0};
	var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = 0;
	var height = 0;
	var data = [];
	var metaData = [];
	var transitionSpeed = 800;
	var stagger = transitionSpeed * 0.70;
	var scale;
	var mapRatio = 0.65;
	var active = d3.select(null);
	var path;
	var g;
	var zoom;
	var _width;
	var _height;
	var dataFilter = 'none';
	var draw = chart.append("g");
	var palette = ['#c5c5c5', '#dfdfdf', '#e9e9e9', '#d9d9d9','#c7c7c7','#e0e0e0', '#cecece', '#e9e9e9', '#c7c7c7'];
	var currentIndex = 0;

	function my(hasPie){
		_width = width - margin.left - margin.right;
  		_height = _width * mapRatio;

		var projection = d3.geo.miller()
			.scale(_width)
			.translate([_width * 0.3, _height * 2.01])
			.precision(1);

		zoom = d3.behavior.zoom()
		    .translate([0, 0])
		    .scale(1)
		    .scaleExtent([1, 8])
		    .on("zoom", zoomed);

		path = d3.geo.path()
		    .projection(projection);

		chart.attr("viewBox", "0 0 " + _width + " " + _height);

		var globe = draw.selectAll("path")
		  	.data(topojson.feature(data, data.objects.countries).features);

		globe.enter().append("path").attr("class", "feature");

		globe.attr("d", path)
	        .attr('fill', function(d, i){return palette[i % palette.length];})
	        .each(function(d, i) {
	            var country = d3.select(this);

	            if(metaData[i] !== undefined && metaData[i] !== null){
	            	country.classed('highlight', true).on("click", function(d){clicked(d, this ,i);});

		           	var coords = getCentroid(country);

		           	if(metaData[i].offsetLabel){
		           		if(metaData[i].offsetLabel.x)
		           			coords[0] *= metaData[i].offsetLabel.x;

		           		if(metaData[i].offsetLabel.y)
		           			coords[1] *= metaData[i].offsetLabel.y;
		           	}

		           	metaData[i].coords = coords;
	            }
	        });

	    var sectionsBot = draw.selectAll(".sectionsBot").data(d3.keys(metaData));

		var g_SectionBot = sectionsBot.enter().append("g")
			.attr('class', 'sectionsBot')
			.style('opacity', 0);

		g_SectionBot.append('text')
    		.attr("transform", function(d) { return "translate(" + metaData[d].coords + ")"; })
    		.attr('dy', '0.3em')
    		.attr('font-size', function(d){ return (metaData[d].fontSize) ? metaData[d].fontSize * (_width * 0.02) : (_width * 0.02); })
    		.text(function(d){ return metaData[d].name; });

    	sectionsBot.select('text')
			.attr('font-size', function(d){ return (metaData[d].fontSize) ? metaData[d].fontSize * (_width * 0.02) : (_width * 0.02); })
			.attr("transform", function(d) { return "translate(" + metaData[d].coords + ")"; });

		sectionsBot.transition()
			.duration(transitionSpeed)
			.style("opacity", 1);

		var sections = draw.selectAll(".sections").data(d3.keys(metaData));

		var g_Section = sections.enter().append("g")
			.attr('class', 'sections')
			.style('opacity', 0);

    	var filterSpot = g_Section.append('g')
    		.attr('opacity', 1)
    		.attr("transform", function(d) { return "translate(" + ((metaData[d].offsetFilter) ? metaData[d].coords[0] * metaData[d].offsetFilter : metaData[d].coords[0]) + ", " + metaData[d].coords[1] + ")"; })
			.attr('class', 'filterSpot');

		filterSpot.append("circle")
  			.attr("stroke", "none")
  			.attr("fill", '#424846')
			.attr("r", 0);

		filterSpot.append("ellipse")
			.attr("stroke", "none")
  			.attr("fill", '#424846')
			.attr("rx", 0)
			.attr("ry", 0)
			.attr("cy", 0);

		filterSpot.append('text')
			.attr('font-size', (_width * 0.0125))
			.attr('dy', '0.3em')
			.style('opacity', 0)
			.text(function(d){ return (metaData[d].kar) ? metaData[d].kar[dataFilter] : ""; });

		sections.transition()
			.duration(transitionSpeed)
			.style("opacity", 1);

		sections.select('g').select('text')	
			.text(function(d){ return (metaData[d].kar && dataFilter !== 'none') ? metaData[d].kar[dataFilter] : this.textContent; })
			.transition()
			.duration(transitionSpeed)
			.style("opacity", function(d){ return (metaData[d].kar && dataFilter !== 'none') ? 1 : 0; });

		sections.select('circle')
			.transition()
			.duration(transitionSpeed)
			.attr("r", function(d){ return (metaData[d].kar && dataFilter !== 'none') ? (d3.select(this.parentNode).select('text').node().getBBox().width * 0.5) + (_width * 0.0075) : 0; });

		sections.select('.filterSpot')
			.transition()
			.duration(function(){if(hasPie){return 0;} return transitionSpeed;})
			.attr("transform", function(d) { return (metaData[d].kar && dataFilter !== 'none') ? "translate(" + ((metaData[d].offsetFilter) ? metaData[d].coords[0] * metaData[d].offsetFilter : metaData[d].coords[0]) + ", " + (metaData[d].coords[1] - (d3.select(this).select('text').node().getBBox().width * 0.5) - (((metaData[d].fontSize) ? metaData[d].fontSize * (_width * 0.02) : (_width * 0.02)) * 0.5) - (_width * 0.02)) + ")" : "translate(" + metaData[d].coords[0] + ", " + (metaData[d].coords[1] - (((metaData[d].fontSize) ? metaData[d].fontSize * (_width * 0.02) : (_width * 0.02)) * 0.5)) + ")"; });

		sections.select('ellipse')
			.transition()
			.duration(transitionSpeed)
			.attr("rx", function(d){return (metaData[d].kar && dataFilter !== 'none') ? (d3.select(this.parentNode).select('text').node().getBBox().width * 0.2) + (_width * 0.0025) : 0; })
			.attr("ry", function(d){return (metaData[d].kar && dataFilter !== 'none') ? (d3.select(this.parentNode).select('text').node().getBBox().width * 0.5) + (_width * 0.006) : 0; })
			.attr("cy", function(d){return (metaData[d].kar && dataFilter !== 'none') ? (_width * 0.0075) : 0; });

		sections.exit()
			.transition()
			.duration(transitionSpeed)
    		.style("opacity", 0)
    		.remove();
	}

	function clicked(d, thisPath, index, force) {

		if (active.node() === thisPath || force) { 
			d3.select('#map__holder').style('width', '100%');
			d3.select('#map__holder').style('padding-bottom', '65%');
			d3.select('#map__holder').style('position', 'relative');
			d3.select('.map__statsHolder').classed('show', false);
			pieChart.data([]).call(pieChart);
			d3.select('.fa-chevron-up').classed('fa-rotate-270', false);
			d3.select('.js-mapButton').html('Compare Data');
			d3.selectAll('.filterSpot').transition()
				.duration(transitionSpeed)
				.attr('opacity', 1);
			return reset(); 
		}

		if(d3.select('.map__statsHolder').classed('show') && !force){
			return;
		}

		d3.selectAll('.filterSpot').transition()
			.duration(transitionSpeed)
			.attr('opacity', 0);
		
		d3.selectAll('.tabs h4').classed('tabs__active', false);
		d3.select('.tabs h4:nth-child(1)').classed('tabs__active', true);
		d3.select('.js-toggleMapChecks').classed('show', false);
		d3.select('.fa-chevron-up').classed('fa-rotate-180', false);
		d3.select('.fa-chevron-up').classed('fa-rotate-270', true);
		d3.select('.js-mapButton').html('Full Map');

		d3.selectAll('.js-kar').classed('hidden', true);
		d3.selectAll('.js-ish').classed('hidden', true);
		d3.selectAll('.js-edt').classed('hidden', true);
		d3.select('.js-none').classed('hidden', true);
		d3.select('.map__statsColorbar').style('background', '#95C3E4');
		if(metaData[index].kar){
			d3.selectAll('.js-kar').classed('hidden', false);
		} else {
			d3.select('.js-none').classed('hidden', false);
		}

		setStats(index);

		active.classed("active", false);
		active = d3.select(thisPath).classed("active", true);

		var bounds = path.bounds(d),
			dx = bounds[1][0] - bounds[0][0],
			dy = bounds[1][1] - bounds[0][1],
			x = (bounds[0][0] + bounds[1][0]) / 2,
			y = (bounds[0][1] + bounds[1][1]) / 2;

		if(metaData[index].offsetZoom) {
			if(metaData[index].offsetZoom.scale) {
				dx *= metaData[index].offsetZoom.scale;
				dy *= metaData[index].offsetZoom.scale;
			}

			if(metaData[index].offsetZoom.x)
				x *= metaData[index].offsetZoom.x;

			if(metaData[index].offsetZoom.y)
				y *= metaData[index].offsetZoom.y;
		}

		var scale = .9 / Math.max(dx / _width, dy / _height),
			translate = [_width / 2 - scale * x, _height / 2 - scale * y];

		draw.transition()
			.duration(transitionSpeed)
			.style("stroke-width", 1.5 / scale + "px")
  			.call(zoom.translate(translate).scale(scale).event)
  			.each("end", function(){
  				d3.select('#map__holder').style('width', '35%');
  				d3.select('#map__holder').style('padding-bottom', (65 * 0.35) + '%');
  				d3.select('#map__holder').style('position', 'absolute');
  				d3.select('.map__statsHolder').classed('show', true);
  				pieChart.width(document.getElementById('pieHolder').clientWidth * 0.75);

  				setTimeout(function(){
  					pieChart();
  				}, 800);
  			});
	}

	function setStats(index){
		currentIndex = index;

		d3.selectAll('.d3-generated').remove();

		if(metaData[index].kar){
			pieChart.data([{
				label: "HD PMP",
				data: metaData[index].kar['HD PMP']
			}, 
			{
				label: "PD PMP",
				data: metaData[index].kar['PD PMP']
			}]);
			
			var getTable = d3.selectAll('.js-kar tr:last-child td')[0];
			getTable[0].innerHTML = metaData[index].kar['Cost Ratio'];
			getTable[1].innerHTML = metaData[index].kar['Dialysis Patients'];
			getTable[2].innerHTML = metaData[index].kar['PD Utilities'];
			getTable[3].innerHTML = metaData[index].kar['PD Patients'];
			getTable[4].innerHTML = metaData[index].kar['APD Utilities'];
			var PDPMP = metaData[index].kar['PD PMP']
			var HDPMP = metaData[index].kar['HD PMP'];
			var totalPie = PDPMP + HDPMP;
			
			d3.select('.js-PDPMP').html(PDPMP + ' (' + Math.round(((PDPMP / totalPie) * 100)) + '%)');
			d3.select('.js-HDPMP').html(HDPMP + ' (' + Math.round(((HDPMP / totalPie) * 100)) + '%)');
			d3.select('.js-total').html(totalPie);
		}

		if(metaData[index].ish){buildRows(metaData[index].ish, d3.select('.js-ish'));}
		if(metaData[index].edt){buildRows(metaData[index].edt, d3.select('.js-edt'));}
	}

	function reset() {
	  active.classed("active", false);
	  active = d3.select(null);

	  draw.transition()
	      .duration(750)
	      .style("stroke-width", "1.5px")
	      .call(zoom.translate([0, 0]).scale(1).event);
	}

	function zoomed() {
		draw.style("stroke-width", 1.5 / d3.event.scale + "px");
		draw.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}

	function getCentroid(selection) {
	    // get the DOM element from a D3 selection
	    // you could also use "this" inside .each()
	    var element = selection.node(),
	        // use the native SVG interface to get the bounding box
	        bbox = element.getBBox();
	    // return the center of the bounding box
	    return [bbox.x + bbox.width/2, bbox.y + bbox.height/2];
	}

	d3.selectAll(".js-toggleMapChecks").on("change", function(e) {
		dataFilter = d3.select('input[name="compareData"]:checked').node().value;
		my();
	});

	function buildRows(table, curTable){
		table.forEach(function(d){
			var holdRow = curTable.append('tr').attr('class', 'd3-generated');

			d.row.forEach(function(dl){
				var holdCol = holdRow.append('td')
					.attr('rowSpan', function(){if(dl.rowSpan) return dl.rowSpan;})
					.attr('colspan', function(){if(dl.colSpan) return dl.colSpan;})
					.attr('class', function(){if(dl.classes) return dl.classes.join(' ');});
				
				(dl.col.length === 0 || !dl.col.trim()) ? holdCol.html('N/A') : holdCol.html(dl.col);

				if(dl.table){
					var holdTable = holdCol.append('table');
					buildRows(dl.table, holdTable);
				}
			});
		});
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.padding = function(value) {
		if (!arguments.length) return padding;
		padding = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		return my;
	};

	my.metaData = function(value) {
		if (!arguments.length) return metaData;
		metaData = value;
		return my;
	};

	my.scale = function(value) {
		if (!arguments.length) return scale;
		scale = value;
		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	my.currentIndex = function(value) {
		if (!arguments.length) return currentIndex;
		currentIndex = value;
		return my;
	};

	my.force = function(){
		clicked(null, null, null, true);
	}

	return my;
}