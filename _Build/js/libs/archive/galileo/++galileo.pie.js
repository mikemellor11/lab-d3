function createPie(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 0, right: 0, bottom: 0, left: 0};
	var width = 0;
	var height = 0;
	var data = [];
	var transitionSpeed = 800;
	var arc;
	var delaySpeed = transitionSpeed;
	var totalPie = 0;

	var pie = d3.layout.pie()
	    .sort(null)
	    .value(function(d) {return d.data; });

	var group = chart.append("g");

	var tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return '<h5>' + d.data.label + ' - <span class="clearBold">' + d.data.data + ' (' + Math.round(((d.data.data / totalPie) * 100)) + '%)</span></h5>';
		});

	function my(){
		totalPie = 0;
		var radius = width * 0.5;

		for(var i = 0, len = data.length; i < len; i++){
			totalPie += +data[i].data;
		}

		arc = d3.svg.arc()
		    .outerRadius(radius)
		    .innerRadius(radius * 0.5);

		group.attr("transform", "translate(" + (width + margin.left + margin.right) * 0.5 + "," + (height + margin.top + margin.bottom) * 0.5 + ")");

		chart.attr("viewBox", "0 0 " + width + " " + height);

		chart.call(tip);

		var pies = group.selectAll(".arc").data(pie(data));

		var gPie = pies.enter()
			.append("g")
		  	.attr("class", "arc");

		pies.exit().remove();

		gPie.append("path")
			.each(function(d) {this._current = {value: d.data, startAngle:d.startAngle, endAngle:d.startAngle, padAngle:d.padAngle};})
			.attr('class', function(d, i){ return i % 2 ? "fillA" : "fillB"; })
			.on('mouseover', tip.show)
      		.on('mouseout', tip.hide);
			
		pies.select('path')
			.transition()
			.delay(function(d, i) { return i * delaySpeed; })
			.duration(transitionSpeed)
			.attrTween("d", arcTween);
	}

	function arcTween(a) {
		var i = d3.interpolate(this._current, a);
		this._current = i(0);
		return function(t) {
			return arc(i(t));
		};
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value - margin.left - margin.right;
		height = value - margin.top - margin.bottom;

		chart.attr("viewBox", "0 0 " + width + " " + height);
		    
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value - margin.top - margin.bottom;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.delaySpeed = function(value) {
		if (!arguments.length) return delaySpeed;
		delaySpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	return my;
}