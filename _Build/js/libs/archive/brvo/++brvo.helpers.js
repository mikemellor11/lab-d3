function p(x,y){
	return x+" "+y+" ";
}

function roundedRect(x, y, w, h, r1, r2, r3, r4){
	var strPath = "M"+p(x+r1,y); //A
	strPath+="L"+p(x+w-r2,y)+"Q"+p(x+w,y)+p(x+w,y+r2); //B
	strPath+="L"+p(x+w,y+h-r3)+"Q"+p(x+w,y+h)+p(x+w-r3,y+h); //C
	strPath+="L"+p(x+r4,y+h)+"Q"+p(x,y+h)+p(x,y+h-r4); //D
	strPath+="L"+p(x,y+r1)+"Q"+p(x,y)+p(x+r1,y); //A
	strPath+="Z";

	return strPath;
}

function gloss(x, y, w, h, h2, r1, r2){
	if(h2 < (r2 * 0.5)){
		h2 = h; // stops rounded rect clipping second height position
	}
	var strPath = "M"+p(x+r1,y); //A
	strPath+="L"+p(x+w-r2,y)+"Q"+p(x+w,y)+p(x+w,y+r2); //B
	strPath+="L"+p(x+w,y+h); //C
	strPath+="Q"+p(x,y+h)+p(x,y+h2); //D
	strPath+="L"+p(x,y+r1)+"Q"+p(x,y)+p(x+r1,y); //A
	strPath+="Z";

	return strPath;
}

function createGradient(svg, selector, x1, y1, x2, y2, offsetA, colorA, opacityA, offsetB, colorB, opacityB){
	if(document.getElementById(selector) !== null) { return; }
	var gradient = d3.select('#globalGradients').append("svg:defs")
	    .append("svg:linearGradient")
	    .attr("id", selector)
	    .attr("x1", x1)
	    .attr("y1", y1)
	    .attr("x2", x2)
	    .attr("y2", y2);

	gradient.append("svg:stop")
	    .attr("offset", offsetA)
	    .attr("stop-color", colorA)
	    .attr("stop-opacity", opacityA);

	gradient.append("svg:stop")
	    .attr("offset", offsetB)
	    .attr("stop-color", colorB)
	    .attr("stop-opacity", opacityB);
}

function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        x = text.attr("x"),
        y = text.attr("y"),
        dy = 0;//parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}