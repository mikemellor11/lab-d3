function createAster(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 0, right: 0, bottom: 0, left: 0};
	var width = 0;
	var height = 0;
	var data = null;
	var step = null;
	var transitionSpeed = 600;
	var stagger = transitionSpeed * 0.20;
	var arc;
	var path;
	var _mashData = null;
	var x;
	var y;
	var radius;
	var partition;
	var saveClick = 'Home';
	var quickDepth = 100;
	var group;
	var checkCheckedBool = true;

	var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return d.key;
  })

	var draw = chart.append("g")
		.attr("class", "draw")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	function my(){
		var _margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};
		var _width = width - _margin.left - _margin.right;
		var _height = height - _margin.top - _margin.bottom;
		radius = Math.min(_width, _height) / 2;
    	var colors = {
			"Home": "#bd1d1f",
			"Mild": "#da4e4e",
			"Moderate": "#bb2d2d",
			"Severe": "#961212",
			"Cannot grade": "#7b615c",
			"Center subfield": "#bd1d1f",
			"Inner subfield": "#de4640",
			"Outer subfield": "#f56e5f",
			"A": "#A29100",
			"B": "#0099A8",
			"C": "#62A60A"
		};

		draw.call(tip);

		x = d3.scale.linear()
		    .range([0, 2 * Math.PI]);

		y = d3.scale.linear()
		    .range([0, radius - 100]);

		chart.attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

    	draw.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")

		/*_mashData = data[0];
		_mashData.values.push(data[1]);
		_mashData.values.push({key:"blank", values:[data[2]]})*/

		_mashData = {key:"Home", values:[data[0]]};
		_mashData.values.push({key:"blank", values:[data[1]]});
		_mashData.values[1].values.push({key:"blank", values:[data[2]]});

    	partition = d3.layout.partition()
		    .sort(null)
		    .value(function(d, i) {
		    	return 1; 
		    })
		    .children(function(d){
		    	return d.values;
		    });

		arc = d3.svg.arc()
		    .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
		    .endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
		    .innerRadius(function(d) { return Math.max(0, y(d.y)); })
		    .outerRadius(function(d) { if(!d.values){return Math.max(0, y(d.y + d.dy + (d.size * 0.01)));} return Math.max(0, y(d.y + d.dy)); });

    	var holdPath = draw.datum(_mashData).selectAll("g").data(partition.nodes);
				
		group = holdPath.enter().append("g")
			.attr('class', function(d){if(d.key === 'A' || d.key === 'B' || d.key === 'C'){return 'AsterBar'} return null;});

		path = group.append("path")
					.attr("d", arc)
					.style("stroke", "#fff")
					.style("fill", function(d) {if(d.key === 'Inner subfield' || d.key === 'Center subfield' || d.key === 'Outer subfield' || d.key === 'blank'){ return "white"; } return colors[d.key]; })
					.style("fill-rule", "evenodd")
					.style('opacity', function(d){
						if(d.key === 'Inner subfield' || d.key === 'Center subfield' || d.key === 'Outer subfield'){return 0.4}if(d.key === 'blank' || d.key === 'Home'){ return 0; } return colors[d.key];
					})
					.each(stash)
					.on("click", click)
					.on("mouseover", mouseover)
					.on("mouseleave", mouseleave);

		draw.selectAll('.AsterBar').append('text')
			.attr('class', 'arcText')
			.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
			.text(function(d, i){if(d.children){return d.key;} return d.size;});

		group.select('.arcText')
			.style('opacity', 0)
			.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; });

		d3.selectAll("form").on("change", function change() {
			var checkChecked = [];

			d3.select(this).selectAll('input').each(function(){
				checkChecked.push(this.checked);
			});

			checkCheckedBool = true;

			var value = function(d){
				if(checkChecked[0] === true && d.parent.key === 'Mild'){
					return 1;
				} else if (checkChecked[1] === true && d.parent.key === 'Moderate'){
					return 1;
				} else if (checkChecked[2] === true && d.parent.key === 'Severe'){
					return 1;
				} else if (checkChecked[3] === true && d.parent.key === 'Cannot grade'){
					return 1;
				}
				checkCheckedBool = false;
				return 0;
			}

			draw.selectAll('.arcText').transition().style("opacity", 0);

			path.data(partition.value(value).nodes)
				.transition()
				.duration(750)
				.attrTween("d", arcTween2);		
		});
	}

	function click(d) {
		if(saveClick === d.key){return;}
		saveClick = d.key;
		path.transition()
			.duration(750)
			.attrTween("d", arcTween2);

		draw.selectAll('.arcText').transition().style("opacity", 0);
	}

	// Fade all but the current sequence, and show it in the breadcrumb trail.
	function mouseover(d) {
		if(d.key === 'Home'){
			d3.select(this).style('opacity', 0);
		}else if(d.key === 'Inner subfield' || d.key === 'Center subfield' || d.key === 'Outer subfield'){
			d3.select(this).style('opacity', 0.6);
			tip.show(d);
		}else if(d.key === 'Mild' || d.key === 'Moderate' || d.key === 'Severe' || d.key === 'Cannot grade'){
			d3.select(this).style('opacity', 0.8);
			tip.show(d);
		}
	}

	function mouseleave(d) {
		if(d.key === 'Home'){
			d3.select(this).style('opacity', 0);
		}else if(d.key === 'Inner subfield' || d.key === 'Center subfield' || d.key === 'Outer subfield'){
			d3.select(this).style('opacity', 0.4);
			tip.hide(d);
		}else if(d.key === 'Mild' || d.key === 'Moderate' || d.key === 'Severe' || d.key === 'Cannot grade'){
			d3.select(this).style('opacity', 1);
			tip.hide(d);
		}
	}

	// Stash the old values for transition.
	function stash(d) {
	  d.x0 = d.x;
	  d.dx0 = d.dx;
	}

	// Interpolate the arcs in data space.
	function arcTween2(a) {
		var data;
		if(saveClick === 'Inner subfield'){
			data = _mashData.values[1].values[0];
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#inner').style('opacity', 1);
			quickDepth = 135;
		} else if(saveClick === 'Outer subfield'){
			data = _mashData.values[1].values[1].values[0];
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#outer').style('opacity', 1);
			quickDepth = 155;
		} else if(saveClick === 'Center subfield'){
			data = _mashData.values[0];
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#center').style('opacity', 1);
			quickDepth = 115;
		} else{
			d3.selectAll('.outincenHold img').style('opacity', 1);
			draw.selectAll('.arcText').transition().style("opacity", 0);
			data = _mashData;
			quickDepth = 100;
		}

		var i = d3.interpolate({x: a.x0, dx: a.dx0}, a);
		var xd = d3.interpolate(x.domain(), [data.x, data.x + data.dx]);
		var yd = d3.interpolate(y.domain(), [data.y, 1]);
		var yr = d3.interpolate(y.range(), [data.y ? 20 : 0, (radius - quickDepth)]);
		return function(t) {
			var b = i(t);
			a.x0 = b.x;
			a.dx0 = b.dx;
			x.domain(xd(t));
			y.domain(yd(t)).range(yr(t));

			if(t >= 1){
				group.select('.arcText')
					.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
					.transition()
					.duration(1000)
					.style('opacity', function(d){if(quickDepth === 100){if(!checkCheckedBool && d.value > 0){return 1;} return 0;} if(d.parent.parent.key === saveClick){
						if(d.value > 0){
							return 1;
						}
					} return 0;});
			}
			return arc(b);
		};
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.padding = function(value) {
		if (!arguments.length) return padding;
		padding = value;
		return my;
	};

	my.barGap = function(value) {
		if (!arguments.length) return barGap;
		barGap = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = mapData(value);
		return my;
	};

	my.step = function(value) {
		if (!arguments.length) return step;
		step = value;
		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	my.moveOn = function(value) {
		if (!arguments.length) return moveOn;
		moveOn = value;
		click();
		return my;
	}

	my.inner = function(){
		if(saveClick === 'Inner subfield'){d3.selectAll('.outincenHold img').style('opacity', 1);saveClick = 'Home';}
		else{
			saveClick = 'Inner subfield';
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#inner').style('opacity', 1);
		}
		path.transition()
			.duration(750)
			.attrTween("d", arcTween2);

		draw.selectAll('.arcText').transition().style("opacity", 0)

		return my;
	}

	my.outer = function(){
		if(saveClick === 'Outer subfield'){d3.selectAll('.outincenHold img').style('opacity', 1);saveClick = 'Home';}
		else{
			saveClick = 'Outer subfield';
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#outer').style('opacity', 1);
		}
		path.transition()
			.duration(750)
			.attrTween("d", arcTween2);

		draw.selectAll('.arcText').transition().style("opacity", 0)

		return my;
	}

	my.center = function(){
		if(saveClick === 'Center subfield'){d3.selectAll('.outincenHold img').style('opacity', 1);saveClick = 'Home';}
		else{
			saveClick = 'Center subfield';
			d3.selectAll('.outincenHold img').style('opacity', 0.5);
			d3.select('#center').style('opacity', 1);
		}
		path.transition()
			.duration(750)
			.attrTween("d", arcTween2);

		draw.selectAll('.arcText').transition().style("opacity", 0)

		return my;
	}

	function mapData(value){
		var idIndex = 0;
		var stackedData = [];
		var data = parseData(value[0]);

		// IF MORE THAN ONE DATA SET PRESENT, LOOP OVER DATA SETS, PARSE IT AS ABOVE, BUT INSTEAD OF KEEPING IT, INJECT IT INTO THE VALUES ARRAY OF EACH BAR
		if(value.length > 1) {
			for(var i = 1; i < value.length; i++){
				stackedData.push(parseData(value[i]));
			}
			for(var j = 0; j < data.length; j++){
				for(var k = 0; k < data[j].values.length; k++){
					for(var l = 0; l < data[j].values[k].values.length; l++){
						for(var i = 0; i < stackedData.length; i++){
							idIndex = i * 1000;
							data[j].values[k].values[l].values.push({
								id:idIndex++, value:stackedData[i][j].values[k].values[l].values[0].value
							});
						}
					}
				}
			}
		}
		return data;
	}

	function parseData(value){
		value = value.filter(function(e, i){
			var sectionFound = ((step.sectionFilter) ? false : true);
			var groupFound = ((step.groupFilter) ? false : true);
			for(var y in e){
				if(step.sectionFilter){
					for(var i = 0; i < step.sectionFilter.length; i++){
						if(e[y] === step.sectionFilter[i]){
							sectionFound = true;
						}
					}
				}
				if(step.groupFilter){
					for(var i = 0; i < step.groupFilter.length; i++){
						if(e[y] === step.groupFilter[i]){
							groupFound = true;
						}
					}
				}
			}
			if(sectionFound && groupFound){
				return e;
			}
		});
		return d3.nest()
			.key(function(d) {
				return d.Section;
			})
			.key(function(d){
				return d.Group;
			})
			.rollup(function(d) {
				var keys = [];
				for(var y in d[0]) {
					if(y !== 'Section' && y !== 'Group'){
						var barFound = ((step.barFilter) ? false : true);
						if(step.barFilter){
							for(var i = 0; i < step.barFilter.length; i++){
								if(y === step.barFilter[i]){
									barFound = true;
								}
							}
						}
						if(barFound){
							if(+d[0][y] && typeof(+d[0][y]) === 'number'){
								keys.push({
									key:y, size:+d[0][y]
								});
							}
						}
					}
				}
				return keys;
				})
			.entries(value);
	}

	function getColor(bar, i){
		if(step.barColors){
			return d3.rgb(step.barColors[bar]).brighter(i * 0.5);
		}
		if(bar === 'A'){
			return d3.rgb('#ff7527').brighter(i * 0.5);
		} else if(bar === 'B'){
			return d3.rgb('#e1a300').brighter(i * 0.5);
		} else if(bar === 'C'){
			return d3.rgb('#81b70b').brighter(i * 0.5);
		}
	}

	function getSectionIndex(i){
		return ((step.sectionOrder) ? step.sectionOrder[i] : i);
	}

	function getGroupIndex(i){
		return ((step.groupOrder) ? step.groupOrder[i] : i);
	}

	function getBarIndex(i){
		return ((step.barOrder) ? step.barOrder[i] : i);
	}

	return my;
}