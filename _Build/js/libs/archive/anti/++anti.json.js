var one = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA change from baseline to Month 12",
		"leftLabel": "There is a wide variation in the mean number of letters gained from baseline reported across clinical trials of anti-VEGF in DME",
		"yMax": 20,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 0,
			"group": ["A"]
		}, 
		{
			"value": [10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 1,
			"group": "A"
		}, 
		{
			"value": [6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 2,
			"group": "A"
		}, 
		{
			"value": [11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 3,
			"group": "A"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 4,
			"group": "A"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 5,
			"group": "A"
		}, 
		{
			"value": [10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 6,
			"group": "A"
		}, 
		{
			"value": [9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 7,
			"group": "A"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "A"
		}, 
		{
			"value": [6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "A"
		}, 
		{
			"value": [7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "A"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 11,
			"group": "A"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 12,
			"group": "A"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 13,
			"group": "A"
		}, 
		{
			"value": [10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 14,
			"group": "A"
		}
	]
};

var two = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA change from baseline to Month 12",
		"leftLabel": "There is a wide variation in the mean number of letters gained from baseline reported across clinical trials of anti-VEGF in DME",
		"yMax": 20,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 0,
			"group": "B"
		}, 
		{
			"value": [10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 1,
			"group": "B"
		}, 
		{
			"value": [6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 2,
			"group": "B"
		}, 
		{
			"value": [11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 3,
			"group": "B"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 4,
			"group": "B"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 5,
			"group": "B"
		},
		{
			"value": [10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 6,
			"group": "B"
		},  
		{
			"value": [9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 7,
			"group": "C"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "C"
		}, 
		{
			"value": [6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "B"
		}, 
		{
			"value": [7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "B"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 11,
			"group": "C"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 12,
			"group": "C"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 13,
			"group": "C"
		}, 
		{
			"value": [10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 14,
			"group": "C"
		}
	]
};

var three = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA change from baseline to Month 12",
		"leftLabel": "Different VA entry criteria were used in these trials…",
		"yMax": 20,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 6
			},
			{
				"label": "39-73 letters",
				"start": 7,
				"end": 7
			},
			{
				"label": "39-78 letters",
				"start": 8,
				"end": 10
			},
			{
				"label": "40-85 letters",
				"start": 11,
				"end": 14
			}
		]
	},
	"data": [
		{
			"value": [9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 6,
			"group": "B"
		}, 
		{
			"value": [10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": "B"
		}, 
		{
			"value": [6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 8,
			"group": "B"
		}, 
		{
			"value": [11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 11,
			"group": "B"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 12,
			"group": "B"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 13,
			"group": "B"
		},
		{
			"value": [10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 14,
			"group": "B"
		},  
		{
			"value": [9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "C"
		}, 
		{
			"value": [12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "C"
		}, 
		{
			"value": [6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "B"
		}, 
		{
			"value": [7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "B"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "C"
		}, 
		{
			"value": [10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "C"
		}, 
		{
			"value": [12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "C"
		}, 
		{
			"value": [10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "C"
		}
	]
};

var four = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA at baseline",
		"leftLabel": "…resulting in variable baseline VA",
		"yMax": 70,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 6
			},
			{
				"label": "39-73 letters",
				"start": 7,
				"end": 7
			},
			{
				"label": "39-78 letters",
				"start": 8,
				"end": 10
			},
			{
				"label": "40-85 letters",
				"start": 11,
				"end": 14
			}
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 6,
			"group": "D"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": "D"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 8,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 11,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 12,
			"group": "D"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 13,
			"group": "D"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 14,
			"group": "D"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "E"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "E"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "D"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "D"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "E"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "E"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "E"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "E"
		}
	]
};

var five = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA at baseline",
		"leftLabel": "…resulting in variable baseline VA",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 6
			},
			{
				"label": "39-73 letters",
				"start": 7,
				"end": 7
			},
			{
				"label": "39-78 letters",
				"start": 8,
				"end": 10
			},
			{
				"label": "40-85 letters",
				"start": 11,
				"end": 14
			}
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 6,
			"group": "D"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": "D"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 8,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 11,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 12,
			"group": "D"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 13,
			"group": "D"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 14,
			"group": "D"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "E"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "E"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "D"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "D"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "E"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "E"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "E"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "E"
		}
	]
};

var six = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA at baseline",
		"leftLabel": "Yet surprisingly, entry criteria allowing the inclusion of patients with worse visual acuity may not ultimately result in a low mean baseline visual acuity",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 6
			},
			{
				"label": "39-73 letters",
				"start": 7,
				"end": 7
			},
			{
				"label": "39-78 letters",
				"start": 8,
				"end": 10
			},
			{
				"label": "40-85 letters",
				"start": 11,
				"end": 14
			}
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 6,
			"group": "G"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": "H"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 8,
			"group": "I"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 11,
			"group": "J"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 12,
			"group": "J"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 13,
			"group": "J"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 14,
			"group": "J"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "F"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "F"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "I"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "I"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "F"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "F"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "F"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "F"
		}
	]
};

var seven = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA at baseline",
		"leftLabel": "Yet surprisingly, entry criteria allowing the inclusion of patients with worse visual acuity may not ultimately result in a low mean baseline visual acuity",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": "G"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": "H"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": "I"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": "J"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": "J"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": "J"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": "J"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": "F"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "F"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "I"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "I"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": "F"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": "F"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": "F"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": "F"
		}
	]
};

var eight = {
	"attributes": 
	{
		"rightLabel": "Mean BCVA at baseline",
		"leftLabel": "Yet surprisingly, entry criteria allowing the inclusion of patients with worse visual acuity may not ultimately result in a low mean baseline visual acuity",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": "D"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": "D"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": "D"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": "D"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": "D"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": "E"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "E"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "D"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "D"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": "E"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": "E"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": "E"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": "E"
		}
	]
};

var nine = {
	"attributes": 
	{
		"leftLabel": "However, gains in VA are affected by baseline VA. While there are differences in baseline VA across studies, gains in VA also vary…",
		"yMax": 80,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63, 9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": ["D", "B"]
		}, 
		{
			"value": [60.2, 10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.8, 6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 12.0], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": ["D", "B"]
		}, 
		{
			"value": [54.7, 12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": ["D", "B"]
		},
		{
			"value": [57.5, 10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": ["D", "B"]
		},  
		{
			"value": [58.8, 9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.6, 12.0], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": ["E", "C"]
		}, 
		{
			"value": [63.9, 6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.7, 7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": ["D", "B"]
		}, 
		{
			"value": [58.8, 10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.4, 10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": ["E", "C"]
		}, 
		{
			"value": [58.9, 12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": ["E", "C"]
		}, 
		{
			"value": [60.8, 10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": ["E", "C"]
		}
	]
};

var ten = {
	"attributes": 
	{
		"leftLabel": "…and mean overall VA at 12 months tends to plateau around 70 letters",
		"rightLabel": "Average end VA ~70 letters",
		"average": 70,
		"displayNum": false,
		"yMax": 80,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [72], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": ["B"]
		}, 
		{
			"value": [70.5], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": ["B"]
		}, 
		{
			"value": [71.6],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": ["B"]
		}, 
		{
			"value": [68.8], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": ["B"]
		}, 
		{
			"value": [68.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": ["B"]
		}, 
		{
			"value": [67.2], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": ["B"]
		},
		{
			"value": [68.4], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": ["B"]
		},  
		{
			"value": [68.5], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": ["C"]
		}, 
		{
			"value": [71.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": ["C"]
		}, 
		{
			"value": [70.7], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": ["B"]
		}, 
		{
			"value": [72.1], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": ["B"]
		}, 
		{
			"value": [69.5], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": ["C"]
		}, 
		{
			"value": [70.1], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": ["C"]
		}, 
		{
			"value": [71.4], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": ["C"]
		}, 
		{
			"value": [71.3], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": ["C"]
		}
	]
};

var eleven = {
	"attributes": 
	{
		"leftLabel": "But Protocol T is unusual. While it allowed the entry of patients with low visual acuity...",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 6
			},
			{
				"label": "39-73 letters",
				"start": 7,
				"end": 7
			},
			{
				"label": "39-78 letters",
				"start": 8,
				"end": 10
			},
			{
				"label": "40-85 letters",
				"start": 11,
				"end": 14
			}
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 6,
			"group": "G"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": "H"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 8,
			"group": "I"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 11,
			"group": "J"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 12,
			"group": "J"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 13,
			"group": "J"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 14,
			"group": "J"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "F"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "F"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 9,
			"group": "I"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 10,
			"group": "I"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "F"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "F"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "F"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "F"
		}
	]
};

var twelve = {
	"attributes": 
	{
		"leftLabel": "But Protocol T is unusual. While it allowed the entry of patients with low visual acuity...",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
			{
				"label": "24-73 letters",
				"start": 0,
				"end": 5
			},
			{
				"label": "24-78 letters",
				"start": 6,
				"end": 9
			},
			{
				"label": "39-73 letters",
				"start": 10,
				"end": 10
			},
			{
				"label": "39-78 letters",
				"start": 11,
				"end": 13
			},
			{
				"label": "40-85 letters",
				"start": 14,
				"end": 17
			}
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 9,
			"group": "G"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 10,
			"group": "H"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 11,
			"group": "I"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 14,
			"group": "J"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 15,
			"group": "J"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 16,
			"group": "J"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 17,
			"group": "J"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 0,
			"group": "F"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 1,
			"group": "F"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "I"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "I"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 2,
			"group": "F"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 3,
			"group": "F"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 4,
			"group": "F"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 5,
			"group": "F"
		},
		{
			"value": [65.1], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 6,
			"group": "G"
		}, 
		{
			"value": [65], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 7,
			"group": "G"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "Prot. T1.25 mg PRN",
			"index": 8,
			"group": "G"
		}
	]
};

var thirt = {
	"attributes": 
	{
		"leftLabel": "...mean BCVA at baseline was high…",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": "G"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": "H"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": "I"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": "J"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": "J"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": "J"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": "J"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": "F"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "F"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "I"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "I"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": "F"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": "F"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": "F"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": "F"
		},
		{
			"value": [65.1], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 17,
			"group": "G"
		}, 
		{
			"value": [65], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 16,
			"group": "G"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": "G"
		}
	]
};

var fourt = {
	"attributes": 
	{
		"displayNum": "N",
		"leftLabel": "...mean BCVA at baseline was high…",
		"yMax": 70,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": "K"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": "K"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": "K"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": "K"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": "K"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": "K"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": "K"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": "K"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "K"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "K"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "K"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": "K"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": "K"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": "K"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": "K"
		},
		{
			"value": [65.1], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 17,
			"group": "N"
		}, 
		{
			"value": [65], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 16,
			"group": "N"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": "N"
		}
	]
};

var fift = {
	"attributes": 
	{
		"leftLabel": "... and final VA was greater in all Protocol T study arms than that seen in previous clinical trials.",
		"yMax": 90,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": "D"
		}, 
		{
			"value": [60.2], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": "D"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": "D"
		}, 
		{
			"value": [56.9], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": "D"
		}, 
		{
			"value": [54.7], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": "D"
		},
		{
			"value": [57.5], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": "D"
		},  
		{
			"value": [58.8], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": "E"
		}, 
		{
			"value": [59.6], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": "E"
		}, 
		{
			"value": [63.9], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": "D"
		}, 
		{
			"value": [64.7], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": "D"
		}, 
		{
			"value": [58.8], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": "E"
		}, 
		{
			"value": [59.4], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": "E"
		}, 
		{
			"value": [58.9], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": "E"
		}, 
		{
			"value": [60.8], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": "E"
		},
		{
			"value": [65.1], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 17,
			"group": "D"
		}, 
		{
			"value": [65], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 16,
			"group": "E"
		}, 
		{
			"value": [64.8],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": "M"
		}
	]
};

var sixt = {
	"attributes": 
	{
		"leftLabel": "... and final VA was greater in all Protocol T study arms than that seen in previous clinical trials.",
		"yMax": 90,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63, 9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 11,
			"group": ["D", "B"]
		}, 
		{
			"value": [60.2, 10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 9,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.8, 6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 14,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 1,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 2,
			"group": ["D", "B"]
		}, 
		{
			"value": [54.7, 12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": ["D", "B"]
		},
		{
			"value": [57.5, 10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 3,
			"group": ["D", "B"]
		},  
		{
			"value": [58.8, 9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 4,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.6, 12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 8,
			"group": ["E", "C"]
		}, 
		{
			"value": [63.9, 6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 12,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.7, 7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 13,
			"group": ["D", "B"]
		}, 
		{
			"value": [58.8, 10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.4, 10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 7,
			"group": ["E", "C"]
		}, 
		{
			"value": [58.9, 12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 6,
			"group": ["E", "C"]
		}, 
		{
			"value": [60.8, 10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 10,
			"group": ["E", "C"]
		},
		{
			"value": [65.1, 11.2], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 17,
			"group": ["D", "B"]
		}, 
		{
			"value": [65, 13.3], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 16,
			"group": ["E", "C"]
		}, 
		{
			"value": [64.8, 9.7],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": ["M", "L"]
		}
	]
};

var sevent = {
	"attributes": 
	{
		"leftLabel": "... and final VA was greater in all Protocol T study arms than that seen in previous clinical trials.",
		"yMax": 90,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63, 9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 13,
			"group": ["D", "B"]
		}, 
		{
			"value": [60.2, 10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.8, 6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 12,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 3,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 4,
			"group": ["D", "B"]
		}, 
		{
			"value": [54.7, 12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": ["D", "B"]
		},
		{
			"value": [57.5, 10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 1,
			"group": ["D", "B"]
		},  
		{
			"value": [58.8, 9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 2,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.6, 12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 11,
			"group": ["E", "C"]
		}, 
		{
			"value": [63.9, 6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 8,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.7, 7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 14,
			"group": ["D", "B"]
		}, 
		{
			"value": [58.8, 10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.4, 10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 6,
			"group": ["E", "C"]
		}, 
		{
			"value": [58.9, 12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 10,
			"group": ["E", "C"]
		}, 
		{
			"value": [60.8, 10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 9,
			"group": ["E", "C"]
		},
		{
			"value": [65.1, 11.2], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 16,
			"group": ["D", "B"]
		}, 
		{
			"value": [65, 13.3], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 17,
			"group": ["E", "C"]
		}, 
		{
			"value": [64.8, 9.7],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": ["M", "L"]
		}
	]
};

var eighteen = {
	"attributes": 
	{
		"leftLabel": "This suggests that the Protocol T study population may be atypical in some way, and the results of the Protocol T trial should be viewed with caution.",
		"average": 70,
		"yMax": 90,
		"yMin": 40,
		"yAxis": "ETDRS letters",
		"grouper": [
		]
	},
	"data": [
		{
			"value": [63, 9], 
			"label": "",
			"labelB": "Prot. I 0.5 mg(deferred laser)",
			"index": 13,
			"group": ["D", "B"]
		}, 
		{
			"value": [60.2, 10.3], 
			"label": "",
			"labelB": "RESOLVE 0.3 mg + 0.5 mg pooled",
			"index": 7,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.8, 6.8],
			"label": "",
			"labelB": "RESTORE PRN monotherapy",
			"index": 12,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 11.9], 
			"label": "",
			"labelB": "RISE 0.5 mg*",
			"index": 3,
			"group": ["D", "B"]
		}, 
		{
			"value": [56.9, 12], 
			"label": "",
			"labelB": "RIDE 0.5 mg*",
			"index": 4,
			"group": ["D", "B"]
		}, 
		{
			"value": [54.7, 12.5], 
			"label": "",
			"labelB": "RISE 0.3 mg*",
			"index": 0,
			"group": ["D", "B"]
		},
		{
			"value": [57.5, 10.9], 
			"label": "",
			"labelB": "RIDE 0.3 mg*",
			"index": 1,
			"group": ["D", "B"]
		},  
		{
			"value": [58.8, 9.7], 
			"label": "",
			"labelB": "DA VINCI 2q8",
			"index": 2,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.6, 12], 
			"label": "",
			"labelB": "DA VINCI 2 mg PRN",
			"index": 11,
			"group": ["E", "C"]
		}, 
		{
			"value": [63.9, 6.8], 
			"label": "",
			"labelB": "RETAIN T&E",
			"index": 8,
			"group": ["D", "B"]
		}, 
		{
			"value": [64.7, 7.4], 
			"label": "",
			"labelB": "RETAIN PRN",
			"index": 14,
			"group": ["D", "B"]
		}, 
		{
			"value": [58.8, 10.7], 
			"label": "",
			"labelB": "VIVID 2q8",
			"index": 5,
			"group": ["E", "C"]
		}, 
		{
			"value": [59.4, 10.7], 
			"label": "",
			"labelB": "VISTA 2q8",
			"index": 6,
			"group": ["E", "C"]
		}, 
		{
			"value": [58.9, 12.5], 
			"label": "",
			"labelB": "VISTA 2q4",
			"index": 10,
			"group": ["E", "C"]
		}, 
		{
			"value": [60.8, 10.5], 
			"label": "",
			"labelB": "VIVID 2q4",
			"index": 9,
			"group": ["E", "C"]
		},
		{
			"value": [65.1, 11.2], 
			"label": "",
			"labelB": "Prot. T0.3 mg PRN",
			"index": 16,
			"group": ["D", "B"]
		}, 
		{
			"value": [65, 13.3], 
			"label": "",
			"labelB": "Prot. T2.0 mg PRN",
			"index": 17,
			"group": ["E", "C"]
		}, 
		{
			"value": [64.8, 9.7],
			"label": "",
			"labelB": "Prot. T1.25 mg PRNZ",
			"index": 15,
			"group": ["M", "L"]
		}
	]
};