function createBar(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 20, right: 10, bottom: 25, left: 40};
	var padding = {top: 0, right: 5, bottom: 0, left: 5};
	var width = 0;
	var height = 0;
	var gapBetween = 5;
	var data = {attributes:{},data:[]};
	var transitionSpeed = 600;
	var stagger = transitionSpeed * 0.20;
	var cornerRadius = 0.1;

	chart.append("g")
		.attr("class", "yLabel")
		.append("text")
		.attr("dy", "-4em");

	chart.append("g")
		.attr("class", "xLabel")
		.append("text")
		.attr("dy", "3.5em");

	chart.append("svg:foreignObject")
		.attr('class', 'leftLabel')
	    .append("xhtml:h2");

	chart.append("svg:foreignObject")
		.attr('class', 'rightLabel')
	    .append("xhtml:div");

	chart.append("g")
		.attr("class", "x axis");

	chart.append("g")
		.attr("class", "y axis")
		.append("line");

	var draw = chart.append("g")
		.attr("class", "draw");

	chart.append("g")
		.attr("class", "average")
		.append("line");

	function my(){
		var _margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};

		var idIndex = 0;
		var maxValuesArray = 1;

		for(var i = 0; i < data.data.length; i++){
			if(maxValuesArray < data.data[i].value.length){
				maxValuesArray = data.data[i].value.length;
			}
		}

		for(var j = 0; j < maxValuesArray; j++){
			for(var i = 0; i < data.data.length; i++){
				if(data.data[i].value[j]){
					data.data[i].value[j] = {
						id: idIndex,
						value: data.data[i].value[j]
					};
					idIndex++;
				}
			}

			idIndex = 1000;
		}

		if(data.attributes.yAxis) {
			_margin.left = _margin.left + 55;
		}
		if(data.attributes.xAxis) {
			_margin.bottom = _margin.bottom + 40;
		}

		var _width = width - _margin.left - _margin.right;
		var _height = height - _margin.top - _margin.bottom;

		var barWidth = ((_width - padding.left - padding.right - (gapBetween * (data.data.length - 1))) / data.data.length);

		var x = d3.scale.linear()
		    .domain([(data.attributes.yMin) ? data.attributes.yMin : 0, (data.attributes.yMax) ? data.attributes.yMax : d3.max(data.data, function(d) { return d.value[0].value; })])
		    .range([_height, 0]);

	    var yAxis = d3.svg.axis()
		    .scale(x)
		    .innerTickSize(-_width)
    		.outerTickSize(0)
		    .ticks(5)
		    .orient("left");

		chart.select(".yLabel").attr("transform", "translate(" + _margin.left + ", " + (_margin.top + (_height * 0.5)) + ") rotate(-90)")
			.select("text").text(data.attributes.yAxis);

		chart.select(".xLabel").attr("transform", "translate(" + (_margin.left + (_width * 0.5)) + ", " + (_height + 30) + ")")
			.select("text").text(data.attributes.xAxis);

		chart.select(".average line")
			.style("opacity", 0)
		    .attr("stroke-dasharray", "10, 5")
		    .attr("x1", (_margin.left - 1))
		    .attr("x2", (_margin.left + _width + 1))
		    .attr("y1", function(){
		    	return (data.attributes.average) ? (_margin.top + padding.top + x(data.attributes.average)) : 0;
		    })
		    .attr("y2", function(){
		    	return (data.attributes.average) ? (_margin.top + padding.top + x(data.attributes.average)) : 0;
		    })
		    .transition()
	    	.duration(transitionSpeed)
	    	.style("opacity", function(){
        		if(data.attributes.average){
        			return 1;
        		}
        		return 0;
        	});

		chart.select(".leftLabel")
			.attr("width", (_width * 0.75))
        	.attr("height", _margin.top)
        	.attr("x", _margin.left)
        	.select("h2")
        	.style("opacity", function(){
        		if(d3.select(this).html() === data.attributes.leftLabel){
        			return 1;
        		}
        		return 0;
        	})
			.html(data.attributes.leftLabel)
			.transition()
	    	.duration(transitionSpeed)
	    	.style("opacity", 1);

	   	chart.select(".rightLabel")
			.attr("width", (_width * 0.25))
        	.attr("height", _margin.top)
        	.attr("x", ((_margin.left + _width) - (_width * 0.25)))
        	.select("div")
        	.style("opacity", function(){
        		if(d3.select(this).html() === data.attributes.rightLabel){
        			return 1;
        		}
        		return 0;
        	})
			.html(data.attributes.rightLabel)
			.transition()
	    	.duration(transitionSpeed)
	    	.style("opacity", 1);

		var grouperY = (((_height + _margin.top + padding.top)) - (_height - x(d3.max(data.data, function(d) { return d.value[0].value; }))));

		var grouperHold = draw.selectAll(".grouper").data(data.attributes.grouper);

		var grouperSVG = grouperHold.enter()
			.append("g")
			.attr("class", "grouper")
			.style("opacity", 0);

		grouperHold.exit()
			.transition()
	    	.duration(transitionSpeed)
	    	.style("opacity", 0)
	    	.remove();

		grouperSVG.append("line")
			.attr('marker-end', "url(#triangleEnd)")
			.attr('marker-start', "url(#triangleStart)");

		grouperSVG.append("svg:foreignObject")
			.attr('class', 'forGroup')
			.attr("transform", function(d, i){ return "translate(" + (_margin.left + ((d.start * (barWidth + gapBetween)) + padding.left)) + ", " + (grouperY - 110) + ")"; })
		    .attr("width", function(d, i){ return ((_margin.left + ((d.end * (barWidth + gapBetween)) + padding.left + barWidth)) - (_margin.left + ((d.start * (barWidth + gapBetween)) + padding.left))); })
            .attr("height", 60)
		    .append("xhtml:div")
		    .attr('class', 'grouperHTMLHold')
		    .append("xhtml:div")
		    .attr('class', 'grouperHTML')
		    .style("height", "60px")
            .html(function(d) {return d.label;})

        grouperHold.transition()
	    	.duration(transitionSpeed)
	    	.style("opacity", 1);

		grouperHold.select(".grouper line")
			.transition()
	    	.duration(transitionSpeed)
		    .attr("x1", function(d, i){ return (_margin.left + ((d.start * (barWidth + gapBetween)) + padding.left)); })
		    .attr("x2", function(d, i){ return (_margin.left + ((d.end * (barWidth + gapBetween)) + padding.left + barWidth)); })
		    .attr("y1", (grouperY - 20))
		    .attr("y2", (grouperY - 20));

		grouperHold.select(".forGroup")
			.transition()
	    	.duration(transitionSpeed)
	    	.attr("transform", function(d, i){ return "translate(" + (_margin.left + ((d.start * (barWidth + gapBetween)) + padding.left)) + ", " + (grouperY - 110) + ")"; })
		    .attr("width", function(d, i){ return ((_margin.left + ((d.end * (barWidth + gapBetween)) + padding.left + barWidth)) - (_margin.left + ((d.start * (barWidth + gapBetween)) + padding.left))); });

		chart.transition()
		    .attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

		chart.select(".y.axis line")
		    .attr("x1", (_margin.left - 1))
		    .attr("x2", (_margin.left + _width + 1))
		    .attr("y1", (_height + _margin.top + padding.top))
		    .attr("y2", (_height + _margin.top + padding.top));

		chart.select(".x.axis")
			.attr("transform", "translate(" + _margin.left + ", " + _margin.top + ")")
			.transition()
	    	.duration(transitionSpeed)
			.call(yAxis)
			.selectAll("text")
			.attr("dx", "-0.5em");

		var bar = draw.selectAll(".bars").data(data.data);

		bar.exit().transition()
			.duration(transitionSpeed)
    		.style("opacity", 0).remove();

		// APPENDS

		var hold = bar.enter().append("g")
			.attr('class', 'bars')
			.attr('font-size', (barWidth * 0.2))
		    .attr("transform", function(d, i) { return "translate(" + ((d.index * (barWidth + gapBetween)) + (_margin.left + padding.left)) + ", " + _margin.top + ")"; });

	    bar.each(function(d, i){

	    		var barArray = d3.select(this).selectAll(".mainBarHold").data(d.value, function(d, i){
	    			return d.id;
	    		});

	    		var mainBarArray = barArray.enter().insert("g", "g").attr('class', 'mainBarHold');

	    		barArray.exit().transition()
	    			.duration(transitionSpeed)
		    		.style("opacity", 0).remove();

		    	barArray.transition()
			    	.duration(transitionSpeed)
			    	.style("opacity", 1);

	    		mainBarArray.append("path")
			    	.style("fill", function(dl, i) {
						return getColor(d.group[i]);
					})
					.attr("class", "mainBar")
			    	.attr("d", function(dl, il) {
				    	return roundedRect(0, _height, barWidth, 0, 0, 0, 0, 0);
				    });

			    barArray.select('.mainBar')
			    	.transition()
			    	.delay(function() { return d.index * stagger; })
			    	.duration(transitionSpeed)
			    	.style("fill", function(dl, i) {
						return getColor(d.group[i]);
					})
					.attr("d", function(dl, i) {
						var base = 0;
						for(var z = 0; z <= i; z++){
							base += d.value[z].value;
						}
						var cR = (( _height - x(base)) < (barWidth * cornerRadius)) ? ( _height - x(base)) : barWidth * cornerRadius;
				      	return roundedRect(0, x(base), barWidth, _height - x(base), cR, cR, 0, 0);
				    });

			    mainBarArray.append("text")
				    .attr("y", _height)
				    .attr("x", barWidth * 0.5)
				   	.attr("dy", "0.6em")
				    .attr("class", "numVal")
				    .style("opacity", 0)
				    .text(0);

				barArray.select(".numVal")
					.each(function(dl, i) {
					    /*if (this.getBBox().height >= (_height - x(dl))) {
					    	d3.select(this).style("visibility", "hidden");
					    } else {
							d3.select(this).style("visibility", "visible");
					    }*/
					})
					.transition()
					.delay(function(dl, i) { return d.index * stagger; })
					.duration(transitionSpeed)
				    .attr("y", function(dl, i){ 
				    	var base = 0;
						for(var z = 0; z <= i; z++){
							base += d.value[z].value;
						}
				    	return x(base); 
				    })
				    .attr("x", barWidth * 0.5)
				    .style("opacity", function(){
				    	if(data.attributes.displayNum === false){
				    		return 0;
				    	} else if(data.attributes.displayNum === undefined || data.attributes.displayNum === true){
				    		return 1;
				    	} else if(data.attributes.displayNum === d.group[0]){
				    		return 1;
				    	} else {
				    		return 0;
				    	}
				    })
				    .tween("text", function(dl, i) {
			            var i = d3.interpolate(this.textContent, dl.value);
			            return function(t) {
			                this.textContent = parseFloat(i(t)).toFixed(1);
			            };
			        });
	    	});

		hold.append("text")
		    .attr("y", _height)
		    .attr("x", barWidth * 0.5)
		    .attr("dy", "-0.45em")
		    .attr("class", "labelVal")
		    .style("opacity", 0)
		    .text(function(d, i){ return d.label; });

		hold.append("svg:foreignObject")
			.attr("transform", "translate(0, " + (_height + 5) + ")")
		    .attr("width", barWidth)
		    .attr("class", "labelValB")
		    .style({"opacity": 0})
            .attr("height", _margin.bottom)
		    .append("xhtml:div")
            .html(function(d) {return d.labelB;});

		// SELECTION TRANSITIONS

		bar.transition()
			.delay(function(d, i) { return d.index * stagger; })
	    	.duration(transitionSpeed)
	    	.style("opacity", 1)
	    	.attr('font-size', (barWidth * 0.2))
	    	.attr("transform", function(d, i) { return "translate(" + ((d.index * (barWidth + gapBetween)) + (_margin.left + padding.left)) + ", " + _margin.top + ")"; });

	    bar.select(".labelVal")
	    	.text(function(d, i){ return d.label; })
			.transition()
			.delay(function(d, i) { return d.index * stagger; })
			.duration(transitionSpeed)
		    .style("opacity", 1)
		    .attr("y", function(d) { return x(d.value[0].value); })
		    .attr("x", barWidth * 0.5);

		bar.select(".labelValB")
			.transition()
			.delay(function(d, i) { return d.index * stagger; })
			.duration(transitionSpeed)
			.attr("width", barWidth)
		    .style("opacity", 1);
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.padding = function(value) {
		if (!arguments.length) return padding;
		padding = value;
		return my;
	};

	my.gapBetween = function(value) {
		if (!arguments.length) return gapBetween;
		gapBetween = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	function getColor(d){
		if(d === 'A'){
			return '#AAA';
		} else if(d === 'B'){
			return '#ffa530';
		} else if(d === 'C') {
			return '#0055bc';
		} else if(d === 'D') {
			return '#ffc478';
		} else if(d === 'E') {
			return '#98c1fd';
		} else if(d === 'F') {
			return '#007b90';
		} else if(d === 'G') {
			return '#00abc8';
		} else if(d === 'H') {
			return '#00d8fc';
		} else if(d === 'I') {
			return '#99f0fe';
		} else if(d === 'J') {
			return '#8ec7cc';
		} else if(d === 'K') {
			return '#32628b';
		} else if(d === 'L') {
			return '#86c100';
		} else if(d === 'M') {
			return '#acdf38';
		} else if(d === 'N') {
			return '#b23b3b';
		}
	}

	return my;
}