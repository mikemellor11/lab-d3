function createDash(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 10, right: 30, bottom: 30, left: 10};
	var _margin = 0;
	var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = 0;
	var _width = 0;
	var height = 0;
	var _height = 0;
	var data = null; // Current level data
	var _data = null; // Filtered level data
	var _fullData = null; // Full data
	var transitionSpeed = 1100;
	var stagger = 200;
	var parseDate = d3.time.format("%b %d, %Y").parse;
	var parseDate2 = d3.time.format("%m/%d/%Y").parse;
	var x;
	var xAxis;
	var y;
	var yAxis;
	var filters = {};
	var events;
	var documents;
	var totalEvents = 0;

	chart.append("g").attr("class", "x axis");

	var bottom = chart.append('g').attr('class', 'bottom');

	tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return 	'<b>Title:</b> ' + d.Title + '</br>' +
					'<b>Status:</b> ' + d.Status + '</br>' +
					'<b>Author(s):</b> ' + d.Author + '</br>';
		});

	chart.call(tip);

	function my(){
		totalEvents = 0;

		_data = JSON.parse(JSON.stringify(data));

		for(var i = 0; i < _data.length; i++){
			_data[i].forEach(function(d, i) {
				if(parseDate(d['Start']) === null){
					d['Start'] = parseDate2(d['Start']);
					d['End'] = parseDate2(d['End']);
					d['Submission'] = parseDate2(d['Submission']);
				}else{
					d['Start'] = parseDate(d['Start']);
					d['End'] = parseDate(d['End']);
					d['Submission'] = parseDate(d['Submission']);
				}
			});

			_data[i].sort(function(a,b) {return a['Start']-b['Start'];});

			_data[i] = _data[i].filter(function(d, i){
				if((new Date(d['Start'])).getTime() < (new Date(holdMinDate)).getTime() && 
					(new Date(d['End'])).getTime() < (new Date(holdMinDate)).getTime()){
					return false;
				}
				if((new Date(d['Start'])).getTime() > (new Date(holdMaxDate)).getTime() && 
					(new Date(d['End'])).getTime() > (new Date(holdMaxDate)).getTime()){
					return false;
				}
				return true;
			}); // Removes blank rows
		}

		callUpdates();

		for(var i = 0; i < _data.length; i++){
			totalEvents += _data[i].length;
		}

		var newTotal = 0;

		_margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};
		_height = height - _margin.top - _margin.bottom;
		_width = width - _margin.left - _margin.right;

		chart.attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

		bottom.attr("transform", "translate(" + _margin.left + ", " + _margin.top + ")")

		documents = bottom.selectAll('.document').data(_data);

		var g_Documents = documents.enter().append('g').attr('class', 'document');

		documents.transition()
			.duration(transitionSpeed)
	    	.style("opacity", 1)

		documents.exit()
			/*.transition()
			.duration(transitionSpeed)*/
			.style("opacity", 0)
			.remove();

		g_Documents.append("rect").attr("class", "background")
			.on('click', docClicked)
			.on("mouseover", docOver)
			.on("mouseleave", docLeave);
		g_Documents.append("g").attr("class", "y axis");

		var biggestYAxis = 0;

		documents.each(function(docData, docIndex){

			newTotal = (docData.length / totalEvents);

			if(totalEvents <= 0){
				newTotal = 0;
			}

			var thisDoc = d3.select(this);

			y = d3.scale.ordinal()
				.domain(docData.map(function (d) {return d['Key']; }))
				.rangeBands([0, (_height * newTotal)], 0.25, 0.5);

		    yAxis = d3.svg.axis()
			    .scale(y)
			    .tickPadding(10)
			    .outerTickSize(0)
			    .innerTickSize(0)
			    .orient("left");

			thisDoc.attr('font-size', function(){ return ((500 / totalEvents) > 10) ? 10 : (500 / totalEvents)});

			thisDoc.select(".y.axis")
				.transition()
				.duration(transitionSpeed)
				.call(yAxis)
				.each(function(){if(biggestYAxis < this.getBBox().width){biggestYAxis = this.getBBox().width;} return null;});
		});

		//var minDate = d3.min(_data, function(d, i){return d3.min(d, function(d, i){return d['Start']});});
		//var maxDate = d3.max(_data, function(d, i){return d3.max(d, function(d, i){return d['End']});});

		x = d3.time.scale()
			.domain([
				holdMinDate,
				holdMaxDate])
		    .range([0, (_width - biggestYAxis)])
		    /*.nice()*/
		    .clamp(true);

	    xAxis = d3.svg.axis()
		    .scale(x)
		    .tickPadding(10)
		    .innerTickSize(-_height)
    		.outerTickSize(0)
		    .orient("bottom")
		    .tickFormat(d3.time.format("%b"));

		var holdAxis = chart.select(".x.axis")
			.attr("transform", "translate(" + (_margin.left + biggestYAxis) + ", " + (_margin.top + _height) + ")")
			.transition()
			.duration(transitionSpeed)
			.call(xAxis);

		documents.each(function(docData, docIndex){

			newTotal = (docData.length / totalEvents);

			if(totalEvents <= 0){
				newTotal = 0;
			}

			var thisDoc = d3.select(this);

			var documentYTrans = 0;
			for(var i = 0; i < docIndex; i++){
				documentYTrans += _data[i].length;
			}

			var tempCalcZero = (documentYTrans / totalEvents);

			if(totalEvents <= 0){
				tempCalcZero = 0;
			}

			thisDoc.attr("transform", "translate(" + biggestYAxis + ", " + (_height * tempCalcZero) + ")");

			y = d3.scale.ordinal()
				.domain(docData.map(function (d) {return d['Key']; }))
				.rangeBands([0, (_height * newTotal)], 0.25, 0.5);

			events = thisDoc.selectAll('.event').data(docData, function(d){return d['Key'];});

			var event_G = events.enter().append('g').attr('class', 'event');

			events.transition()
				.duration(transitionSpeed)
		    	.style("opacity", 1)

			events.exit()
				/*.transition()
				.duration(transitionSpeed)*/
	    		.style("opacity", 0)
	    		.remove();

			event_G.append('rect')
				.attr('class', 'gannt')
				.attr('x', function(d, i){return x(d['Start']);})
				.attr('y', function(d, i){return y(d['Key']);})
				.attr('rx', 6)
				.attr('ry', 6)
				.attr('fill', function(d, i){return taskStatus[d.Status];})
				.attr('width', 0)
				.attr('height', function(){return y.rangeBand();})
				.on("mouseover", mouseover)
				.on("mouseleave", mouseleave);

			event_G.append("path")
				.attr('class', 'submissionIcon')
				.attr("transform", function(d) { return "translate(" + x(d['Start']) + "," + (y(d['Key']) + (y.rangeBand() * 0.5)) + ")"; })
				.attr("d", d3.svg.symbol().type('circle').size(y.rangeBand() * 2))
				.attr('fill', function(d, i){return d3.rgb(taskStatus[d.Status]).brighter(-1);})
				.attr('opacity', 0);

			events.select('.submissionIcon')
				.transition()
				.duration(transitionSpeed)
				.delay(function(d, i) { return i * stagger; })
				.attr("d", d3.svg.symbol().type('circle').size(y.rangeBand() * 2))
				.attr("transform", function(d) { return "translate(" + x(d['Submission']) + "," + (y(d['Key']) + (y.rangeBand() * 0.5)) + ")"; })
				.attr('opacity', function(d){if(d['Submission'] === null || d['Submission'] === undefined){return 0;}return 1;});

			events.select('.gannt')
				.transition()
				.duration(transitionSpeed)
				.delay(function(d, i) { return i * stagger; })
				.attr('x', function(d, i){return x(d['Start']);})
				.attr('y', function(d, i){return y(d['Key']);})
				.attr('fill', function(d, i){return taskStatus[d.Status];})
				.attr('height', function(){return y.rangeBand();})
				.attr('width', function(d, i){return x(d['End']) - x(d['Start']);});

			thisDoc.select(".background")
				.attr('height', (_height * newTotal))
				.attr('width', (_width - biggestYAxis));
		});
	}

	function docClicked(d, i){
		if(data.length <= 1){
			data = JSON.parse(JSON.stringify(_fullData));
		}else {
			data = JSON.parse(JSON.stringify([_fullData[i]]));
		}
		pieChart.data(data).call(pieChart);
		pieChart2.data(data).call(pieChart2);
		textChart.data(data).call(textChart);
		textChart2.data(data).call(textChart2);
		textChart3.data(data).call(textChart3);
		textChart4.data(data).call(textChart4);
		my();
	}

	function docOver(d) {
		d3.select(this).style('fill', 'black');
	}

	function docLeave(d) {
		d3.select(this).style('fill', 'transparent');
	}

	function mouseover(d) {
		tip.html(function(d) {
			return 	'<b>Title:</b> ' + d.Title + '</br>' +
					'<b>Status:</b> ' + d.Status + '</br>' +
					'<b>Author(s):</b> ' + d.Author + '</br>';
		});
		d3.select(this).attr('fill', function(d, i){return taskStatus[d.Type];});
		tip.show(d);
	}

	function mouseleave(d) {
		d3.select(this).attr('fill', function(d, i){return taskStatus[d.Status];});
		tip.hide(d);
	}

	function callUpdates() {
		pieChart.data(_data).key('Status').call(pieChart);
		pieChart.stagger(false);

		pieChart2.data(_data).key('Type').call(pieChart2);
		pieChart2.stagger(false);

		textChart.data(_data).label("Rejected").filter(["Rejected"]).key('Status').call(textChart);
		textChart.transitionSpeed(1000);

		textChart2.data(_data).label("Submitted").filter(['Submitted']).key('Status').call(textChart2);
		textChart2.transitionSpeed(1000);

		textChart3.data(_data).label("Posters").filter(['Poster Presentation']).key('Type').call(textChart3);
		textChart3.transitionSpeed(1000);

		textChart4.data(_data).label("Abstracts").filter(['Abstract']).key('Type').call(textChart4);
		textChart4.transitionSpeed(1000);
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = value;

		for(var i = 0; i < data.length; i++){
			data[i].forEach(function(d, i) {
				d['End'] = d['Publication'];
				if(d['End'] === undefined || d['End'] === ''){
					d['End'] = d['Submission'];
				}
				d['Key'] = d['Document Number'] + i;
			});
		}

		_fullData = JSON.parse(JSON.stringify(data));

		brushChart.data(data).call(brushChart);

		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	return my;
}