function createPie(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 10, right: 10, bottom: 10, left: 10};
	var width = 0;
	var height = 0;
	var data = [];
	var key = null;
	var transitionSpeed = 3800;
	var arc;
	var stagger = true;

	var pie = d3.layout.pie()
	    .sort(null)
	    .value(function(d) {return d.values.length; });

	var group = chart.append("g");

	function my(){
		_margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};
		_height = height - _margin.top - _margin.bottom;
		_width = width - _margin.left - _margin.right;

		var radius = Math.min(_width, _height) / 2;

		data = d3.nest()
			.key(function(d) {
				return d[key];
			})
			.entries(data);

		data.sort(function(a,b) {return b.values.length-a.values.length;});

		arc = d3.svg.arc()
		    .outerRadius(radius)
		    .innerRadius(0);

		group.attr("transform", "translate(" + (_width + _margin.left + _margin.right) * 0.5 + "," + (_height + _margin.top + _margin.bottom) * 0.5 + ")");

		chart.transition()
		    .attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

		var pies = group.selectAll(".arc").data(pie(data));

		var gPie = pies.enter()
			.append("g")
		  	.attr("class", "arc");

		pies.exit().remove();

		gPie.append("path")
			.on("mouseover", mouseover)
			.on("mouseleave", mouseleave)
			.each(function(d) {this._current = {data:d.data, value: d.value, startAngle:d.startAngle, endAngle:d.startAngle, padAngle:d.padAngle};})
			.attr("fill", function(d, i) { return taskStatus[d.data.key]; })
			.attr("stroke", 'white');
			
		pies.select('path')
			.transition()
			.delay(function(d, i) {if(!stagger){return 0;} return i * (transitionSpeed / data.length); }).duration((transitionSpeed / data.length))
			.attrTween("d", arcTween)
			.attr("fill", function(d, i) { return taskStatus[d.data.key]; });;

		gPie.append("text")
			.attr("transform", function(d) {return "translate(" + arc.centroid(d) + ")"; })
			.style("opacity", "0");

		pies.select('text')
			.transition()
			.delay(function(d, i) {if(!stagger){return 0;} return i * (transitionSpeed / data.length); }).duration((transitionSpeed / data.length))
			.attr("transform", function(d) {return "translate(" + arc.centroid(d) + ")"; })
			.style("opacity", "1")
		  	.text(function(d, i){return d.value;});
	}

	function mouseover(d) {
		tip.html(function(d) {
			return 	'<b>' + key + ':</b> ' + d.data.key;
		});

		d3.selectAll('.gannt').each(function(dl, i){
			if(dl[key] === d.data.key){
				d3.select(this).attr("fill", function(d) { return taskStatus[d.Type]; });
			}
		});

		tip.show(d);
	}

	function mouseleave(d) {
		d3.selectAll('.gannt').each(function(dl, i){
			if(dl[key] === d.data.key){
				d3.select(this).attr('fill', function(d, i){return taskStatus[d.Status];});
			}
		});
		tip.hide(d);
	}

	function arcTween(a) {
		var i = d3.interpolate(this._current, a);
		this._current = i(0);
		return function(t) {
			return arc(i(t));
		};
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.key = function(value) {
		if (!arguments.length) return key;
		key = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;

		data = null;
		data = [];

		for(var i = 0; i < value.length; i++){
			data = data.concat(value[i]);
		}

		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	return my;
}