function createBrush(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 10, right: 30, bottom: 30, left: 10};
	var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = 0;
	var height = 0;
	var data = null;
	var transitionSpeed = 1100;
	var stagger = 200;
	var parseDate = d3.time.format("%b %d, %Y").parse;
	var parseDate2 = d3.time.format("%m/%d/%Y").parse;
	var brush;
	var group = chart.append("g");

	function my(){
		
		_data = JSON.parse(JSON.stringify(data));

		for(var i = 0; i < _data.length; i++){
			_data[i].forEach(function(d, i) {
				if(parseDate(d['Start']) === null){
					d['Start'] = parseDate2(d['Start']);
					d['End'] = parseDate2(d['End']);
					d['Submission'] = parseDate2(d['Submission']);
				}else{
					d['Start'] = parseDate(d['Start']);
					d['End'] = parseDate(d['End']);
					d['Submission'] = parseDate(d['Submission']);
				}
			});
			_data[i].sort(function(a,b) {return a['Start']-b['Start'];});
		}

		_margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};
		_height = height - _margin.top - _margin.bottom;
		_width = width - _margin.left - _margin.right;

		chart.transition()
		    .attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

		holdMinDate = d3.min(_data, function(d, i){return d3.min(d, function(d, i){return d['Start']});});
		holdMaxDate = d3.max(_data, function(d, i){return d3.max(d, function(d, i){return d['End']});});

		var x = d3.time.scale()
		    .domain([new Date(2012, 1, 1), new Date(2017, 0, 0) - 1])
		    .nice()
		    .range([0, _width]);

		brush = d3.svg.brush()
		    .x(x)
		    .extent([holdMinDate, holdMaxDate])
		    .on("brushend", brushended);

		group.attr("transform", "translate(" + _margin.left + ", " + _margin.top + ")");

		group.append("rect")
		    .attr("class", "grid-background")
		    .attr("width", _width)
		    .attr("height", _height);

		group.append("g")
		    .attr("class", "x grid")
		    .attr("transform", "translate(0," + _height + ")")
		    .call(d3.svg.axis()
		        .scale(x)
		        .orient("bottom")
		        .ticks(d3.time.months)
		        .tickSize(-_height)
		        .tickFormat(""))
		  	.selectAll(".tick")
		    .classed("minor", function(d) { return d.getHours(); });

		group.append("g")
		    .attr("class", "x axis")
		    .attr("transform", "translate(0," + _height + ")")
		    .call(d3.svg.axis()
		      .scale(x)
		      .orient("bottom")
		      .ticks(d3.time.years)
		      .tickPadding(0))
		  	.selectAll("text")
		    .attr("x", 6)
		    .style("text-anchor", null);

		var gBrush = group.append("g")
		    .attr("class", "brush")
		    .call(brush)
		    .call(brush.event);

		gBrush.selectAll("rect")
		    .attr("height", _height);
	}

	function brushended() {
		if (!d3.event.sourceEvent) return; // only transition after input
			var extent0 = brush.extent(),
			extent1 = extent0.map(d3.time.month.round);

		// if empty when rounded, use floor & ceil instead
		if (extent1[0] >= extent1[1]) {
			extent1[0] = d3.time.month.floor(extent0[0]);
			extent1[1] = d3.time.month.ceil(extent0[1]);
		}

		d3.select(this).transition()
			.call(brush.extent(extent1))
			.call(brush.event);

		holdMinDate = extent1[0];
		holdMaxDate = extent1[1];

		dashChart();
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;
		data = value;
		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	my.stagger = function(value) {
		if (!arguments.length) return stagger;
		stagger = value;
		return my;
	};

	return my;
}