function createText(selector){
	if(selector === null || selector === undefined){
		return null;
	}

	var chart = d3.selectAll(selector);
	var margin = {top: 10, right: 10, bottom: 10, left: 10};
	var width = 0;
	var height = 0;
	var data = [];
	var key = null;
	var label = null;
	var transitionSpeed = 2000;
	var filter = null;

	var bottom = chart.append('g').attr('class', 'bottom')
		.on("mouseover", mouseover)
		.on("mouseleave", mouseleave);

	bottom.append('text').attr('class', 'numText').text(0);

	bottom.append('text').attr('class', 'infoText');

	function my(){

		var totalEvents = data.length;

		data = data.filter(function(d, i)
			{
				for(var j = 0; j < filter.length; j++){
					if(d[key] === filter[j]){return true;}
				}
				return false;
			});

		var newTotal = (100 * (data.length / totalEvents));

		if(totalEvents <= 0){
			newTotal = 0;
		}

		_margin = {top: margin.top, right: margin.right, bottom: margin.bottom, left: margin.left};
		_height = height - _margin.top - _margin.bottom;
		_width = width - _margin.left - _margin.right;

		chart.attr("height", _height + _margin.top + _margin.bottom)
		    .attr("width", _width + _margin.left + _margin.right);

		bottom.attr("transform", "translate(" + _margin.left + ", " + (_margin.top + (_height * 0.5)) + ")");

		bottom.select('.numText')
			.attr('font-size', function(){return ((_width * 0.15 > 60)) ? 60 : (_width * 0.15); })
			.attr("x", _width * 0.5)
			.transition()
			.duration(transitionSpeed)
			.tween("text", function(d) {
	            var i = d3.interpolate(parseInt(this.textContent), newTotal);
	            return function(t) {
	                this.textContent = (parseFloat(i(t)).toFixed(0) + '%');
	            };
	        });

		bottom.select('.infoText').text(label)
			.attr('font-size', function(){return ((_width * 0.10 > 30)) ? 30 : (_width * 0.10); })
			.attr("x", _width * 0.5)
			.attr("y", function(){return ((_width * 0.15 > 60)) ? 60 : (_width * 0.15); });
	}

	function mouseover(d) {
		d3.selectAll('.gannt').each(function(dl, i){
			for(var j = 0; j < filter.length; j++){
				if(dl[key] === filter[j]){
					d3.select(this).attr('fill', 'blue');
				}
			}
		});
	}

	function mouseleave(d) {
		d3.selectAll('.gannt').each(function(dl, i){
			for(var j = 0; j < filter.length; j++){
				if(dl[key] === filter[j]){
					d3.select(this).attr('fill', function(d, i){return taskStatus[d.Status];});
				}
			}
		});
	}

	my.width = function(value) {
		if (!arguments.length) return width;
		width = value;
		return my;
	};

	my.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return my;
	};

	my.margin = function(value) {
		if (!arguments.length) return margin;
		margin = value;
		return my;
	};

	my.key = function(value) {
		if (!arguments.length) return key;
		key = value;
		return my;
	};

	my.filter = function(value) {
		if (!arguments.length) return filter;
		filter = value;
		return my;
	};

	my.label = function(value) {
		if (!arguments.length) return label;
		label = value;
		return my;
	};

	my.data = function(value) {
		if (!arguments.length) return data;

		data = null;
		data = [];

		for(var i = 0; i < value.length; i++){
			data = data.concat(value[i]);
		}

		return my;
	};

	my.transitionSpeed = function(value) {
		if (!arguments.length) return transitionSpeed;
		transitionSpeed = value;
		return my;
	};

	return my;
}