(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('data_world', 'data_state', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('data/data_world'), require('data/data_state'));
	} else {
		root.Random = factory(root.Data_World, root.Data_State);
	}
})(this || window, function (Data_World, Data_State) {
	"use strict";

	var keysUsed = [];

	var config = {
		level: {
			min: 0,
			max: 0
		},
		levels: 0,
		data: {
			values: 0,
			type: '',
			accum: 0
		},
		int: {
			min: 0,
			max: 0
		},
		date: {
			min: 0,
			max: 0
		},
		bool: {
			min: 0,
			max: 0
		},
		string: {
			min: 0,
			max: 0,
			words: 0
		},
		formatDate: null
	};

	var Random = {
		int: function (min, max){
		    return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		string: function (min, max, words){
			var string = '';
			for(var i = Random.int(1, words); i--;){
				string += (Math.random() + 1).toString(36).substring(2, Random.int(min + 2, max));

				if(i){
					string += ' ';
				}
			}
			return string;
		},
		bool: function(){
			return !!Random.int(0, 1);
		},
		date: function(min, max){
			return new Date(min.getTime() + Math.random() * (max.getTime() - min.getTime()));
		},
		key: function(){
			var arr;

			switch(config.data.key){
				case 'state':
					arr = Data_State.features;
					break;
				case 'world':
					arr = Data_World.features;
					break;
			}

			var chosen;

			do{
				chosen = Random.int(0, arr.length - 1);				
			} while(keysUsed.indexOf(chosen) !== -1);

			keysUsed.push(chosen);

			if(!arr[chosen]){
				console.log(arr[chosen], chosen);
			}

			return arr[chosen].id;
		},
		values: function (i, level){
			var type = config.data.type;

			var map = {
					"key": (config.data.key) ? Random.key() : i,
					"label": Random.string(config.string.min, config.string.max, config.string.words)
				};

			if(level < config.levels){
				map.values = Random.level(++level);
			} else {
				if(config.data.values){
					map.value = [];
					var temp;
					var min = config[type].min;
					var max = config[type].max;

					for(var j = 0, jlen = config.data.values; j < jlen; j++){
						if((type === 'int' || type === 'date') && config.data.accum){
							max = config[type].max;

							if(type === 'date'){
								max -= config[type].min.getTime();
							}

							max = (max / jlen) * (j + 1);

							if(type === 'date'){
								max = new Date(config[type].min.getTime() + max);
							}
						}

						temp = Random[type](min, max);

						if((type === 'int' || type === 'date') && config.data.accum){
							min = temp;
						}

						if(type === 'date' && config.formatDate){
							temp = config.formatDate(temp);
						}

						map.value.push(temp);
					}
				} else {
					map.value = Random[type](config[type].min, config[type].max);	

					if(type === 'date' && config.formatDate){
						map.value = config.formatDate(map.value);
					}
				}

				// TODO
				if(config.data.minmax && Random.bool()){
					map.minmax = ['2001-01-01', '2001-02-01'];
				}

				if(config.data.marks){
					map.marks = [{
						key: 0,
						value: config.formatDate(Random[type](config[type].min, config[type].max))
					}];
				}
			}

			return map;
		},
		level: function (level){
			return Array.apply(null, {length: Random.int(config.level.min, config.level.max)}).map(function(d, i){
				return Random.values(i, level || 0);
			});
		},
		data: function (custom){
			initConfig(custom);

	    	return Random.level();
	    }
	};

	function initConfig(custom){
		keysUsed = [];

		config.level.min = custom.level && custom.level.min || 1;
		config.level.max = custom.level && custom.level.max || 2;

		config.levels = (custom.levels != null) ? custom.levels : 2;

		config.data.values = custom.data && custom.data.values || 0;
		config.data.type = custom.data && custom.data.type || 'int';
		config.data.accum = custom.data && custom.data.accum || false;
		config.data.minmax = custom.data && custom.data.minmax || false;
		config.data.marks = custom.data && custom.data.marks || false;
		config.data.key = custom.data && custom.data.key || false;

		config.formatDate = custom.formatDate || null;

		config.int.min = custom.int && custom.int.min || 0;
		config.int.max = custom.int && custom.int.max || 100;

		config.date.min = custom.date && custom.date.min || new Date('2000-01-01');
		config.date.max = custom.date && custom.date.max || new Date('2002-01-01');

		config.bool.min = custom.bool && custom.bool.min || false;
		config.bool.max = custom.bool && custom.bool.max || true;

		config.string.min = custom.string && custom.string.min || 4;
		config.string.max = custom.string && custom.string.max || 11;
		config.string.words = custom.string && custom.string.words || 4;
	}

	return Random;
});