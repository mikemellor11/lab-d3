(function (root, factory) {
    "use strict";
    if ( typeof define === 'function' && define.amd ) {
        define('d3', 'shared', factory);
    } else if ( typeof exports === 'object' ) {
        module.exports = factory(require('d3'), require('shared'));
    } else {
        root.Chart = factory(root.d3, root.Shared);
    }
})(this || window, function (d3, Shared) {
    "use strict";

    function Chart(selector){
    	if(!selector){
			return null;
		}

    	if (!(this instanceof Chart)) { 
            return new Chart(selector);
        }

        var rendered;
        if(typeof Event === 'function'){
        	rendered = new Event('rendered');
        } else {
        	rendered = document.createEvent('Event');
			rendered.initEvent('rendered', true, true);
        }

        this.store = {
			data : [],
			dataLast: d3.local(),
			flatData: [],
			top: null,
			bottom: null,
			draw: null,
			axisHeight : 0,
			axis: {
				x: null,
				y: null
			},
			scale: {
				x: null,
				y: null,
			},
			label: {
				x: null,
				y: null
			},
			height : 0,
			width : 0,
			margin : {
				bottom: 0,
				left: 0,
				right: 0,
				top: 0
			},
			plotWidth: {
				x: 0,
				y: 0
			},
			prevLevels: null,
			levels: null,
			rendered: rendered
		};

		if(typeof selector === 'string'){
			this.store.chart = d3.selectAll(selector);
		} else {
			this.store.chart = d3.select(selector);
		}

		this.store.att = {
			margin : {top: 10, right: 10, bottom: 10, left: 10},
			padding : {outer: 0, inner: 0},
			colors :['fill1', 'fill2', 'fill3', 'fill4'],
			symbols: ['Cross'],
			symbolColors : ['fill2'],
			symbolsSize: 100,
			colorsKey : 'key',
			symbolsKey : 'key',
			symbolColorsKey : 'key',
			minmaxKey : 'key',
			aspectRatio : 0.5625,
			width: 0,
			height: 0,
			autoSize: false,
			transitionType: 'CubicInOut',
			transitionSpeed: 800,
			delaySpeed : 0,
			stagger : 800,
			startOpacity: 0,
			hide: {},
			radius: {
				top: {
					left: 4,
					right: 4
				},
				bottom: {
					left: 4,
					right: 4
				}
			},
			min: {
				x: null,
				y: null
			},
			max: {
				x: null,
				y: null
			},
			label: {
				x: null,
				y: null
			},
			axis: {
				x: {
					flip: false,
					reverse: false,
					rotate: false,
					inside: false,
					hide: false,
					ticks: 5,
					tickFormat: null
				},
				y: {
					flip: false,
					reverse: false,
					rotate: false,
					inside: false,
					hide: false,
					ticks: 5,
					tickFormat: null
				}
			},
			scale: {
				x: 'band',
				y: 'linear'
			},
			autoAxis: 'x',
			labelWidth: 'auto',
			parseDate: d3.timeParse("%Y-%m-%d"),
			formatDate: d3.timeFormat('%Y-%m-%d'),
			spacePadding: 10,
			roundPoints: false,
			cb: null,
			plot: {
				x: 'key',
				y: 'value',
				value: 'value'
			},
			tooltip: {
			},
			decimalPlaces: 0,
			totalCount: 100,
			valueLabel: "{0}",
			chockData: false
		};
    }

    Chart.tooltip = d3.select("body")
	    .append("div")
    	.attr('class', 'labD3__tooltip')
	    .style("position", "absolute")
	    .style("z-index", "10")
	    .style("visibility", "hidden");

    Chart.tooltipDismiss = null;

    Chart.showTip = function(html, cb){
    	Chart.tooltip.style("visibility", "visible").html(html);
    	Chart.tooltipDismiss = cb;
    };

    Chart.hideTip = function(){
		Chart.tooltip.style("visibility", "hidden")
			.style("left", "0px")
    		.style("top", "0px");
    };

    Chart.positionTip = function(x, y){
    	Chart.tooltip.style("left", (x - 15) + "px")
    		.style("top", (y + 15) + "px");
    };

	window.addEventListener('click', function(e){
		var node = e.target;

		while(node.nodeName !== 'BUTTON' && node.nodeName !== 'BODY' && (!node.classList || !node.classList.contains('js-button'))){
			if(node.nodeName === 'HTML' || (node.classList && node.classList.contains('js-stop'))){
				return;
			}

			node = node.parentNode;

			if(!node){
				return;
			}
		}

		if(node.classList.contains('labD3__js-dismiss')){
			Chart.hideTip();

			if(Chart.tooltipDismiss){
				Chart.tooltipDismiss();
				Chart.tooltipDismiss = null;
			}
		}
	});

    Chart.prototype = {
    	// Public //
    	init: function(){
    		var local = this.store, chart = local.chart;

    		if(local.inited){
    			return this;
    		}
	
			chart.append("g")
				.attr("class", "y label")
				.append("text")
				.attr('y', '1em')
				.attr('x', 0);

			chart.append("g")
				.attr("class", "x label")
				.append("text")
				.attr('y', '1em')
				.attr('x', 0);

			chart.append("g")
				.attr("class", "y axis");

			chart.append("g")
				.attr("class", "x axis");

			local.bottom = chart.append("g")
				.attr("class", "bottom");

			local.draw = chart.append("g")
				.attr("class", "draw");

			local.top = chart.append("g")
				.attr("class", "top");

			this.resize();

			local.inited = true;

			return this;
    	},
    	resize: function(){
    		var local = this.store;

    		local.textHeight = Shared.textHeight(local.chart, 'test', false);

			this._updateViewBox();

			return this;
    	},
    	render: function(){
			var local = this.store, att = local.att, chart = local.chart;

			local.margin.left = att.margin.left;
			local.margin.right = att.margin.right;
			local.margin.bottom = att.margin.bottom;
			local.margin.top = att.margin.top;

			if(att.autoAxis === 'x'){
				local.width = att.width - local.margin.left - local.margin.right;

				this._initScale('x');
				
				local.margin[att.axis.x.flip ? 'top' : 'bottom'] += this._calculateAxis('x');

				local.height = att.height - local.margin.top - local.margin.bottom;

				this._initScale('y');
			} else if(att.autoAxis === 'y') {
				local.height = att.height - local.margin.top - local.margin.bottom;

				this._initScale('y');

				local.margin[att.axis.y.flip ? 'right' : 'left'] += this._calculateAxis('y');

				local.width = att.width - local.margin.left - local.margin.right;

				this._initScale('x');
			} else {
				local.height = att.height - local.margin.top - local.margin.bottom;
				local.width = att.width - local.margin.left - local.margin.right;

				this._initScale('y');
				this._initScale('x');
			}

			this._renderAxis('y');
			this._renderAxis('x');

			local.bottom.attr("transform", "translate(" + local.margin.left + "," + local.margin.top + ")");
			local.draw.attr("transform", "translate(" + local.margin.left + "," + local.margin.top + ")");
			local.top.attr("transform", "translate(" + local.margin.left + "," + local.margin.top + ")");

			if(this._additionalSetup){
				this._additionalSetup();
			}

			this._buildLevels(local.draw, local.data, 0, 0);

			chart.each(function(){
				this.dispatchEvent(local.rendered);
			});

			return this;
		},
		renderSync: function(){
			var att = this.store.att;

			var save = {
				transitionSpeed: att.transitionSpeed,
				delaySpeed: att.delaySpeed
			};

			this.att({
				transitionSpeed: 0,
				delaySpeed: 0	
			});

			this.render();

			this.att(save);

			return this;
		},
    	att: function(json) {
			if (!arguments.length) {
				return this.store.att;
			}

			Shared.extend(this.store.att, json);

		    return this;
		},
		data: function (value) {
			if (!arguments.length) {
				return this.store.data;
			}

			var lastEmpty = !(this.store.data.length);

			this.store.data = value;

			var tempFlat = Shared.flattenValues(value);

			if(this.store.levels != null){
				this.store.prevLevels = this.store.levels;
			}

			this.store.flatData = tempFlat[0];
			this.store.levels = tempFlat[1];

			if(!lastEmpty && this.store.draw && this.store.levels !== this.store.prevLevels){
				this.store.draw.selectAll('*').remove();
			}

			return this;
		},
		destroy: function(){
			this.store.chart.attr('class', '')
				.selectAll('*')
				.remove();
		},
		// Private //
		_buildLevels: function(parent, localData, localIndex, holdLevelIndex){
			var local = this.store, att = local.att;
			var levelIndex = local.levels - holdLevelIndex;

			var selection = parent.selectAll(".level--" + levelIndex)
				.data((local.modify) ? local.modify(localData) : localData, function(d, i){return (d.key != null) ? d.key : i;});
			
		    var enter = selection.enter()
		    	.append("g")
		    	.attr('class', 'level--' + levelIndex)
		    	.attr('opacity', att.startOpacity);

	    	var update = enter.merge(selection);

	    	var anim = update;

	    	var inner = null;

	    	if(levelIndex){
	    		enter.append('g')
		    		.attr('class', 'level--' + levelIndex + '--hold');

	    		inner = update.select('.level--' + levelIndex + '--hold');
	    	}

	    	var remove = selection.exit();
	    	if(att.transitionSpeed || att.delaySpeed){
	    		remove = remove.transition()
					.ease(d3['ease' + att.transitionType])
					.delay(att.delaySpeed)
					.duration(att.transitionSpeed)
					.attr("opacity", 0);

				anim = anim.transition()
					.ease(d3['ease' + att.transitionType])
					.duration(att.transitionSpeed);

				if(!att.stagger){
					anim.delay(att.delaySpeed);
				}
	    	} else {
	    		remove.interrupt();
	    		anim.interrupt();
	    	}

	    	remove.remove();

	    	anim.attr('opacity', 1);

	    	var hide = att.hide['level' + levelIndex] || false;

			if(this._shared){
	    		this._shared(enter, update, inner, anim, localData, localIndex, levelIndex, hide);
	    	}

	    	if(this['_level' + levelIndex]){
	    		this['_level' + levelIndex](enter, update, inner, anim, localData, localIndex, levelIndex, hide);
	    	}

			var that = this;

			if(holdLevelIndex < local.levels){
				holdLevelIndex += 1;

				inner.each(function(tempData, tempIndex) {
					that._buildLevels(d3.select(this), tempData.values || [], tempIndex, holdLevelIndex);
				});
			}
		},
		_buildElements: function(element, levelIndex, enter, update, hide, defineLocalVariables, localVariables, first){
			var local = this.store, att = local.att;
			var noLocal;

			if(!defineLocalVariables){
				noLocal = {};
			}

			element[1] = 'level--' + levelIndex + '--' + element[1];

			var added;

			if(typeof element[0] === 'string'){
				added = enter.append(element[0]);
			} else {
				added = enter.insert(element[0][0], '.level--' + levelIndex + '--' + element[0][1]);
			}
    		
			added.attr('class', element[1])
    			.each(function(d, i){
    				if(!noLocal && first){
    					defineLocalVariables.call(enter, d, i);
    				}

    				element[2].call(this, d3.select(this), d, i, 'enter', element[1], noLocal || localVariables[i]);
		    	})
		    	.attr('opacity', att.startOpacity);

	    	update.select('.' + element[1])
	    		.each(function(d, i){
	    			if(!noLocal && first && !localVariables[i]){
    					defineLocalVariables.call(update, d, i);
    				}

	    			var temp = d3.select(this);

    				if((element[3]) ? element[3](d, i, noLocal || localVariables[i]) : hide){
			    		if(att.transitionSpeed || att.delaySpeed){
			    			temp = temp.transition()
								.ease(d3['ease' + att.transitionType])
								.delay((local.delaySpeed != null) ? local.delaySpeed(d, i) : att.delaySpeed)
								.duration((local.transitionSpeed != null) ? local.transitionSpeed(d, i) : att.transitionSpeed);
			    		} else {
			    			temp.interrupt();
			    		}

			    		temp.attr('opacity', 0).on("end", function(){
			    			this.style.display = 'none';
			    		});

	    			} else {
			    		element[2].call(this, temp, d, i, 'update', element[1], noLocal || localVariables[i]);

			    		temp.style('display', '');

			    		if(att.transitionSpeed || att.delaySpeed){
			    			temp = temp.transition()
								.ease(d3['ease' + att.transitionType])
								.delay((local.delaySpeed != null) ? local.delaySpeed(d, i) : att.delaySpeed)
								.duration((local.transitionSpeed != null) ? local.transitionSpeed(d, i) : att.transitionSpeed);
			    		} else {
			    			temp.interrupt();
			    		}

			    		temp.attr('opacity', 1);

			    		element[2].call(this, temp, d, i, 'anim', element[1], noLocal || localVariables[i]);
	    			}
		    	});
		},
		_updateViewBox: function(){
			var local = this.store, att = local.att, data = local.data, chart = local.chart;

			if(att.autoSize || !att.height){
				att.autoSize = true;
				att.height = (att.width * att.aspectRatio);
			}

			chart.each(function(){
				this.parentNode.style.paddingBottom = att.height + 'px';
			});

			this.store.chart.attr("viewBox", "0 0 " + att.width + " " + att.height);
		},
		_initScale: function(scale){
			var local = this.store, att = local.att, data = local.data;

			if(!att.scale[scale]){
		    	return;
		    }

		    var getLimit;

		   if(att.scale[scale] === 'point'){
		    	local.scale[scale] = d3.scalePoint()
				    .domain(local.flatData.map(function(d){return Shared[scale](d, att.plot[scale]);}))
				    .padding(att.padding.outer);

		    	if(att.roundPoints){
					local.scale[scale].round();
		    	}

		    } else if(att.scale[scale] === 'band'){
		    	local.scale[scale] = d3.scaleBand()
				    .domain(local.flatData.map(function(d){return Shared[scale](d, att.plot[scale]);}))
				    .paddingInner(att.padding.inner)
				    .paddingOuter(att.padding.outer);

		    	if(att.roundPoints){
					local.scale[scale].round();
		    	}

		    } else if(att.scale[scale] === 'date') {
		    	getLimit = function(minmax){
		    		return (att[minmax][scale]) ? att.parseDate(att[minmax][scale]) : Shared.minmax(minmax, data, scale, att.plot[scale], att.parseDate);
		    	};

		    	local.scale[scale] = d3.scaleTime()
					.domain([
						getLimit('min'), 
						getLimit('max')
					])/*
					.nice(d3.timeWeek)*/;

		    } else if(att.scale[scale] === 'linear') {
		    	getLimit = function(minmax){
		    		return (att[minmax][scale] != null) ? att[minmax][scale] : Shared.minmax(minmax, data, scale, att.plot[scale]);
		    	};

				local.scale[scale] = d3.scaleLinear()
				    .domain([
						getLimit('min'),
						getLimit('max')
					]);

		    } else if(att.scale[scale] === 'group'){
		    	return;
		    } else {
		    	local.scale[scale] = att.scale[scale](att, data);
		    }

		    if(scale === 'x'){
		    	local.scale[scale].range(((att.axis[scale].reverse ? [local.width, 0] : [0, local.width])));
		    } else {
		    	local.scale[scale].range(((att.axis[scale].reverse) ? [0, local.height] : [local.height, 0]));
		    }

		    if(att.labelWidth !== 'auto'){
	    		local.plotWidth[scale] = +att.labelWidth;
	    	} else if(att.scale[scale] === 'band'){
	    		local.plotWidth[scale] = local.scale[scale].bandwidth();
	    	} else if(att.scale[scale] === 'linear'){
	    		local.plotWidth[scale] = local.scale[scale].range()[(scale === 'x') ? 1 : 0] / att.axis[scale].ticks;
	    	} else {
	    		local.plotWidth[scale] = local.scale[scale].range()[(scale === 'x') ? 1 : 0] / data.length;
	    	}
		},
		_renderAxis: function(scale) {
			var local = this.store, att = local.att, data = local.data, chart = local.chart, select = null;

			if(att.axis[scale].hide){
				return;
			}

			if(att.label[scale]){
				select = chart.select("." + scale + ".label");

				select = select.select('text')
					.text(att.label[scale]);

				if(scale === 'x'){
					select.call(Shared.wrap, local.width)
						.attr("transform", "translate(" + (local.margin.left + (local.width * 0.5)) + ", " + (att.axis.x.flip ? 0 : (local.height + local.margin.top + local.margin.bottom - local.textHeight)) + ")");
				} else {
					select.call(Shared.wrap, local.height)
						.attr("transform", "translate(" + (att.axis.y.flip ? (local.width + local.margin.left + local.margin.right) : 0) + ", " + (local.margin.top + (local.height * 0.5)) + ") rotate(" + (att.axis.y.flip ? 90 : -90) + ")");
				}
			}

			if(!att.scale[scale] || att.scale[scale] === 'group'){
		    	return;
		    }

		    var dir = 'axis';
		    var anchor = null;

		    if(scale === 'x'){
		    	if(att.axis[scale].flip){
		    		if(att.axis[scale].inside){
		    			dir += 'Bottom';
		    		} else {
		    			dir += 'Top';
		    		}
		    	} else {
		    		if(att.axis[scale].inside){
		    			dir += 'Top';
		    		} else {
		    			dir += 'Bottom';
		    		}
		    	}
		    } else {
	    		if(att.axis[scale].flip){
		    		if(att.axis[scale].inside){
		    			dir += 'Left';
		    			anchor = 'end';
		    		} else {
		    			dir += 'Right';
		    			anchor = 'start';
		    		}
		    	} else {
		    		if(att.axis[scale].inside){
		    			dir += 'Right';
		    			anchor = 'start';
		    		} else {
		    			dir += 'Left';
		    			anchor = 'end';
		    		}
		    	}
		    }

			local.axis[scale] = d3[dir]()
			    .scale(local.scale[scale])
				.tickSizeOuter((att.axis[scale].tickSizeOuter != null) ? att.axis[scale].tickSizeOuter : 0)
				.tickPadding(att.spacePadding)
				.ticks(att.axis[scale].ticks)
				.tickFormat(att.axis[scale].tickFormat);

			select = chart.select("." + scale + ".axis");

			if(scale === 'x'){
			    local.axis[scale].tickSizeInner((att.axis[scale].tickSizeInner != null) ? att.axis[scale].tickSizeInner : -local.height);

				select.attr("transform", "translate(" + local.margin.left + ", " + (local.margin.top + ((att.axis[scale].flip) ? 0 : local.height)) + ")");
		    } else {
		    	local.axis[scale].tickSizeInner((att.axis[scale].tickSizeInner != null) ? att.axis[scale].tickSizeInner : -local.width);

				select.attr("transform", "translate(" + (local.margin.left + ((att.axis[scale].flip) ? local.width : 0)) + ", " + local.margin.top + ")");
		    }

		    /*if(att.transitionSpeed || att.delaySpeed){
		    	
		    } else {
		    	//select = select.interrupt();
		    }*/

		    // NEED TO LOOK INTO WHY INTERUPPT BREAKS ON SCALE
		    /*select = select.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed);*/

		    select.call(local.axis[scale])
				.attr('font-size', null)
				.attr('font-family', null)
				.selectAll(".tick text")
				.style('text-anchor', anchor)
      			.call(Shared.wrap, local.plotWidth[scale]);

  			// Need to minus the height of all rows of text after the first if the text hangs from the top. Wrap will always hang downwards
  			if(scale === 'x' && att.axis.x.flip){
  				select.selectAll(".tick text")
  					.attr("transform", function(){
  						return "translate(0, " + -(this.getBBox().height - local.textHeight) + ")";
  					});
  			}
		},
		_calculateAxis: function(scale){
			var local = this.store, att = local.att, data = local.data, chart = local.chart;

			var calcHeight = 0;

			local.axisHeight = 0;

			if(att.label[scale]){
				local.axisHeight = Shared.textHeight(chart, att.label[scale], local[(scale === 'x') ? 'width' : 'height']) + att.spacePadding;
			}

			var biggest = 0;
			for(var i = local.flatData.length; i--;){
				var tempHeight = Shared.textHeight(chart, local.flatData[i][att.plot[scale]], local.plotWidth[scale]);

				if(tempHeight > biggest){
					biggest = tempHeight;
				}
			}

			local.plotHeight = biggest + att.spacePadding;

			calcHeight += local.axisHeight;
			calcHeight += local.plotHeight;

			return calcHeight;
		}
    };

    return Chart;
});