(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_pie', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_pie'));
	} else {
		root.Harveyball = factory(root.d3, root.Shared, root.Chart_Pie);
	}
})(this || window, function (d3, Shared, Chart_Pie) {
	"use strict";

	function Harveyball(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Harveyball)) { 
			return new Harveyball(selector);
		}

		Chart_Pie.call(this, selector);

		this.store.chart.classed('labD3__harveyball', true);

		Shared.extend(this.store, {
		});

		this.att({
		});
	}

	Harveyball.prototype = Object.create(Chart_Pie.prototype);

	Harveyball.prototype.init = function(){
		var local = this.store;

		Chart_Pie.prototype.init.call(this);

		local.bottom.append('circle')
			.attr('class', 'fade');

		return this;
	};

	Harveyball.prototype.data = function (value) {
		var local = this.store, att = local.att;

		Chart_Pie.prototype.data.call(this, value);

		local.data = local.data.slice();

		local.data.push({
			key: 'harveyball_remaining',
			value: att.totalCount - local.data[0].value
		});

		return this;
	};

	Harveyball.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, data = local.data;

		Chart_Pie.prototype._additionalSetup.call(this);

		local.bottom.select('.fade')
			.attr('cx', 0)
			.attr('cy', 0)
			.attr('r', local.radius);
	};

	Harveyball.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		// ELEMENTS
    	[
	    	[
	    		'path',
	    		'slice',
	    		function(element, d, i, status, name, _){
	    			if(status === 'update'){
	    				element.attr('class', name + ' ' + Shared.getKey(att, d, i, 'colors'));
	    			}

	    			if(status === 'anim'){
	    				if(element.attrTween){
	    					element.attrTween("d", function() {
		    					var last = local.dataLast.get(this) || {
										value: d.value, 
										startAngle: d.startAngle, 
										endAngle: d.startAngle, 
										padAngle: d.padAngle
									};

								var i = d3.interpolate(last, d);

								local.dataLast.set(this, d);

								return function(t) {
									return local.arc(i(t));
								};
							});
	    				} else {
	    					element.attr("d", local.arc);
	    				}
	    			}
	    		},
	    		function(d, i){
	    			return i;
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	return Harveyball;
});