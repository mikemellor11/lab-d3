(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_map', 'data_world', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_map'), require('data/data_world'));
	} else {
		root.Worldmap = factory(root.d3, root.Shared, root.Chart_Map, root.Data_World);
	}
})(this || window, function (d3, Shared, Chart_Map, Data_World) {
	"use strict";

	function Worldmap(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Worldmap)) { 
			return new Worldmap(selector);
		}

		Chart_Map.call(this, selector);

		this.store.chart.each(function(){
			d3.select(this).classed('labD3__worldmap', true);
		});

		Shared.extend(this.store, {
			projection: d3.geoMercator(),
			geoJson: Data_World
		});

		this.att({	
		});
	}

	Worldmap.prototype = Object.create(Chart_Map.prototype);

	return Worldmap;
});