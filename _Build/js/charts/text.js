(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Text = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Text(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Text)) { 
			return new Text(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__text', true);

		this.att({
			textFormat: "{value}",
			threshhold: {
				upper: 0.8,
				middle: 0.6,
				lower: 0
			}
		});

		Shared.extend(this.store, {
		});
	}

	Text.prototype = Object.create(Chart.prototype);

	Text.prototype.init = function(){
		return this;
	};

	Text.prototype.render = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		chart.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
			.tween("text", function() {
	            var that = this;
	            var _that = d3.select(this);
	            var i = d3.interpolate(
	            		local.dataLast.get(chart) || 0, 
	            		data[0] && data[0].value || 0
            		);

	            return function(t) {
	            	var value = parseFloat(i(t));
	            	var total = att.totalCount;
	            	var normal = value / total;
	            	var percent = Math.round(normal * 100);

	            	if(normal > att.threshhold.upper){
	            		_that.classed('lower middle', false).classed('upper', true);
	            	} else if(normal > att.threshhold.middle) {
	            		_that.classed('lower upper', false).classed('middle', true);
	            	} else if(normal > att.threshhold.lower) {
	            		_that.classed('middle upper', false).classed('lower', true);
	            	}

	            	local.dataLast.set(chart, value);

	            	var textObject = {
	            		value: value.toFixed(att.decimalPlaces),
	            		percent: percent,
	            		total: total
	            	};

	                that.textContent = Shared.formatKeys(att.textFormat, textObject);
	            };
	        });

		return this;
	};

	Text.prototype.destroy = function(){
		this.store.chart.interrupt()
			.attr('class', '')
			.html('');
	};

	return Text;
});