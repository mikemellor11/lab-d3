(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_map', 'data_state', 'geoAlbersUsaPr', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_map'), require('data/data_state'), require('data/geoAlbersUsaPr'));
	} else {
		root.Statemap = factory(root.d3, root.Shared, root.Chart_Map, root.Data_State, root.geoAlbersUsaPr);
	}
})(this || window, function (d3, Shared, Chart_Map, Data_State, geoAlbersUsaPr) {
	"use strict";

	function Statemap(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Statemap)) { 
			return new Statemap(selector);
		}

		Chart_Map.call(this, selector);

		this.store.chart.each(function(){
			d3.select(this).classed('labD3__statemap', true);
		});

		Shared.extend(this.store, {
			projection: geoAlbersUsaPr(),
			geoJson: Data_State
		});

		this.att({
			margin: {
				right: 10
			}
		});
	}

	Statemap.prototype = Object.create(Chart_Map.prototype);

	return Statemap;
});