(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_pie', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_pie'));
	} else {
		root.Pie = factory(root.d3, root.Shared, root.Chart_Pie);
	}
})(this || window, function (d3, Shared, Chart_Pie) {
	"use strict";

	function Pie(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Pie)) { 
			return new Pie(selector);
		}

		Chart_Pie.call(this, selector);

		this.store.chart.classed('labD3__pie', true);

		Shared.extend(this.store, {
		});

		this.att({
		});
	}

	Pie.prototype = Object.create(Chart_Pie.prototype);

	Pie.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, data = local.data;

		Chart_Pie.prototype._additionalSetup.call(this);

		local.transitionSpeed = function(d, i){
			return att.transitionSpeed / data.length;
		};

		local.delaySpeed = function(d, i){
			return att.delaySpeed + (local.transitionSpeed() * i);
		};
	};

	Pie.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		// ELEMENTS
    	[
	    	[
	    		'path',
	    		'slice',
	    		function(element, d, i, status, name, _){
	    			if(status === 'update'){
	    				element.attr('class', name + ' ' + Shared.getKey(att, d, i, 'colors'));
	    			}

	    			if(status === 'anim'){
	    				if(element.attrTween){
	    					element.attrTween("d", function() {
		    					var last = local.dataLast.get(this) || {
										value: d.value, 
										startAngle: d.startAngle, 
										endAngle: d.startAngle, 
										padAngle: d.padAngle
									};

								var i = d3.interpolate(last, d);

								local.dataLast.set(this, d);

								return function(t) {
									return local.arc(i(t));
								};
							});
	    				} else {
	    					element.attr("d", local.arc);
	    				}
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	return Pie;
});