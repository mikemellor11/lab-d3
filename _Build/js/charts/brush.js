(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Brush = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	var stopRecurse = false;

	function Brush(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Brush)) { 
			return new Brush(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__brush', true);

		this.att({
			aspectRatio: 0.2,
			margin: {
				bottom: 30
			},
			scale: {
				x: 'date',
				y: null
			},
			autoAxis: null,
			handles: false,
			trigger: 'brush end'
		});

		Shared.extend(this.store, {
			brush: null,
			handle: null,
			prevLevels: 0,
			showing: true
		});
	}

	Brush.prototype = Object.create(Chart.prototype);

	var brushed = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var s = d3.event.selection;

		if(!s){
			s = local.scale.x.range();
			local.draw.select(".brush").call(local.brush.move, s);
		}

		local.handle.attr("display", null).attr("transform", function(d, i) { return "translate(" + (s[i] - ((i) ? 30 : 0)) + ", 0)"; });

		var temp;

		if(local.showing && (s[1] - s[0] < 60)){
			temp = local.handle.selectAll('line');

			if(att.transitionSpeed || att.delaySpeed){
	    		temp = temp.transition()
					.duration(400);
			} else {
				temp.interrupt();
			}

			temp.attr('opacity', 0);
			local.showing = false;
		} else if(!local.showing && (s[1] - s[0] > 60)){
			temp = local.handle.selectAll('line');

			if(att.transitionSpeed || att.delaySpeed){
	    		temp = temp.transition()
					.duration(400);
			} else {
				temp.interrupt();
			}

			temp.attr('opacity', 1);
			local.showing = true;
		}

		if(att.trigger.indexOf(d3.event.type) > -1){
			if(att.cb && d3.event.sourceEvent){
				data[0].value[0] = att.formatDate(local.scale.x.invert(s[0]));
				data[0].value[1] = att.formatDate(local.scale.x.invert(s[1]));
				
				att.cb(s.map(local.scale.x.invert));
			}
		}
	};

	Brush.prototype.init = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		Chart.prototype.init.call(this);

		local.brush = d3.brushX()
			.on('brush end', brushed.bind(this))
			.extent([[0, 0], [local.width, local.height]]);

	    var gBrush = local.draw.append("g")
			.attr("class", "brush")
			.call(local.brush);

		local.handle = gBrush.selectAll(".handle--custom")
		  .data([{type: "w"}, {type: "e"}])
		  .enter().append("g")
		    .attr("class", "handle--custom");

	    local.handle.append('rect')
			.attr("cursor", "ew-resize")
			.attr('x', 0)
			.attr('y', 0)
			.attr('width', 30)
			.attr('height', local.height);

		local.handle.append('line')
			.attr('opacity', 1)
			.attr("cursor", "ew-resize")
		    .attr('x1', 10)
		    .attr('x2', 10);

		local.handle.append('line')
			.attr('opacity', 1)
			.attr("cursor", "ew-resize")
		    .attr('x1', 20)
		    .attr('x2', 20);

		return this;
	};

	Brush.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, data = local.data;

		local.brush.extent([[0, 0], [local.width, local.height]]);

		var range = [];

		if(data.length){
			if(data[0].value[0]){
				range.push(local.scale.x(att.parseDate(data[0].value[0])));
			} else {
				range.push(0);
			}

			if(data[0].value[1]){
				range.push(local.scale.x(att.parseDate(data[0].value[1])));
			} else {
				range.push(local.width);
			}

			if(range[0] < 0){
				range[0] = 0;
			}

			if(range[0] > local.width){
				range[0] = local.width;
			}

			if(range[1] > local.width){
				range[1] = local.width;
			}

			if(range[1] < 0){
				range[1] = 0;
			}

			if(range[1] < range[0]){
				range[1] = range[0];
			}
		} else {
			range = local.scale.x.range();
		}

		local.handle.select('rect')
			.attr('height', local.height);

		local.handle.selectAll('line')
			.attr('y1', (local.height * 0.5) - 10)
			.attr('y2', (local.height * 0.5) + 10);

	    var temp = local.draw.select(".brush")
	    	.call(local.brush);

    	if(att.transitionSpeed || att.delaySpeed){
    		temp = temp.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed);
		} else {
			temp.interrupt();
		}
	    	
		temp.call(local.brush.move, range);
	};

	return Brush;
});