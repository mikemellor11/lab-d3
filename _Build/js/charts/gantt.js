(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Gantt = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Gantt(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Gantt)) { 
			return new Gantt(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__gantt', true);

		this.att({
			itemSize: 35,
			padding: {
				inner: 5,
				outer: 5
			},
			scale: {
				x: 'date',
				y: null
			},
			plot: {
				x: 'value'
			},
			autoAxis: null,
			margin: {
				bottom: 30
			},
			autoScrollbar: true,
			showCurrentDay: false,
			minHeight: 0,
			minmaxWidth: 20
		});

		Shared.extend(this.store, {
		});
	}

	Gantt.prototype = Object.create(Chart.prototype);

	Gantt.prototype.init = function(){
		Chart.prototype.init.call(this);

		this.store.bottom.append('line')
			.attr('class', 'today')
			.attr('opacty', 0);

		return this;
	};

	Gantt.prototype._additionalSetup = function(){
		var local = this.store, att = local.att;

		var today = local.bottom.select('line');

		if(att.showCurrentDay){
			today.attr('y1', 0)
				.attr('y2', local.height)
				.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed)
				.attr('x1', local.scale.x(new Date()))
				.attr('x2', local.scale.x(new Date()))
				.attr('opacty', 1);
		} else {
			today.attr('opacty', 0);
		}
	};

	Gantt.prototype._updateViewBox = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var height = this.y(data, data.length);

		height += att.margin.top + att.margin.bottom;

		height -= att.padding.outer; // Removed the padding after the last gantt item

		if(height < att.minHeight){
			height = att.minHeight;
		}

		att.height = height;

		var hasScrollbar = 0;

		if(att.autoScrollbar){
			hasScrollbar = chart.node().parentNode.clientWidth;	
		}

		chart.node().parentNode.style.paddingBottom = att.height + 'px';

		if(att.autoScrollbar){
			hasScrollbar -= chart.node().parentNode.clientWidth;
		}

		att.width -= hasScrollbar;

		chart.attr("viewBox", "0 0 " + att.width + " " + att.height);
	};

	Gantt.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		// LEVEL SETUP
		[enter, anim].forEach(function(d){
			d.attr("transform", function(d, i){
				var y = i * (att.itemSize + att.padding.outer);

				for(var j = 0; j < i; j++){
					if(localData[j].minmax && !att.hide.minmax){
						y += att.itemSize + att.padding.outer;
					}
				}

				return "translate(0, " + y + ")";
			});
		});

		if(att.tooltip.level0){
			enter.on('click', function(d){
					var node = d3.select(this).select('.level--0--gantt');
					node.attr('stroke', 'red');

				    Chart.showTip(att.tooltip.level0(d), function(){
				    	node.attr('stroke', null);
				    });
				});
		}

		// LOCAL VARIABLES
    	var localVariables = [];

    	var defineLocalVariables = function(d, i){
			var _ = localVariables[i];

			if(!_){
				_ = localVariables[i] = {};
			}

			_.x = local.scale.x(att.parseDate(Shared.x(d, att.plot.x)[0]));

			if(_.x < 0){
				_.x = 0;
			}

			_.width = local.scale.x(att.parseDate(Shared.x(d, att.plot.x)[1])) - _.x;

			if(_.x <= 0){
				if(_.width < 100){
					_.width = 100;
				}
			}

			_.xminmax = [0, 0, 0];
			_.widthminmax = 0;

			if(d.minmax){
				_.xminmax[0] = local.scale.x(att.parseDate(d.minmax[0]));
				_.xminmax[1] = local.scale.x(att.parseDate(d.minmax[1]));
				_.xminmax[2] = local.scale.x(att.parseDate(d.minmax[2]));

				_.widthminmax = _.xminmax[1] - _.xminmax[0];
			}

			if(_.widthminmax < att.minmaxWidth){
				_.widthminmax = att.minmaxWidth;
			}
		};

    	// ELEMENTS
    	[
	    	[
	    		'path',
	    		'gantt',
	    		function(element, d, i, status, name, _){
	    			if(status === 'update'){
	    				element.attr('class', function(){
								var color = Shared.getKey(att, d, i, 'colors');

								if(_.x === 0 && _.width === 100){
									color = 'fade';
								}

								return name + ' ' + color;
							});
	    			}

	    			if(status === 'enter' || status === 'anim'){
	    				element.attr("d", Shared.roundedRect(
					      		_.x, 
					      		0,
					      		(_.width < (att.radius.top.left * 2)) ? (att.radius.top.left * 2) : _.width,
					      		att.itemSize,
					      		att.radius.top.left, 
					      		att.radius.top.right, 
					      		att.radius.bottom.right, 
					      		att.radius.bottom.left
				      		));
	    			}
	    		}
    		],
    		[
	    		'text',
	    		'gantt--text',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('dy', '1em')
							.style('text-anchor', 'start');
					}

	    			if(status === 'update'){
	    				element.attr('class', function(){
								if(_.x <= 0 && _.width <= 100){
									return name;
								}

								return name + ' reverseColor';
							})
							.text(d.label)
							.each(function(){
								if(_.x <= 0 && _.width <= 100){
									Shared.ellipse(this, d.label, att.width * 0.25);
								} else {
									Shared.ellipse(this, d.label, _.width - att.padding.inner * 3);
								}
							});
	    			}

	    			if(status === 'enter' || status === 'anim'){
	    				element.attr('transform', function() {
								var tempX = _.x;

								if(_.x <= 0 && _.width <= 100){
									tempX = 100;
								}

								return "translate(" + 
										(tempX + att.padding.inner) + 
										", " + 
										((att.itemSize * 0.5) - (local.textHeight * 0.5)) + 
									")";
							})
	    					.attr('opacity', (att.itemSize > (local.textHeight + att.padding.inner * 2)) ? 1 : 0);
	    			}
	    		}
    		],
    		[
	    		'text',
	    		'gantt--date',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('class', name + ' reverseColor')
							.attr('dy', '1em')
							.style('text-anchor', 'start');
					}

	    			if(status === 'update'){
	    				element.text(att.formatDate(att.parseDate(Shared.x(d, att.plot.x)[0])));
	    			}

	    			if(status === 'enter' || status === 'anim'){
						element.attr("transform", "translate(" + 
									(_.x + att.padding.inner) + 
									", " + 
									((att.itemSize * 0.5) - (local.textHeight * 0.5)) + 
								")")
							.attr('opacity', ((_.x <= 0 && _.width <= 100) && (att.itemSize > (local.textHeight + att.padding.inner * 2))) ? 1 : 0);
	    			}
	    		}
    		],
    		[
	    		'path',
	    		'minmax',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
						element.attr("opacity", (d.minmax) ? 1 : 0)
							.attr("d", (d.minmax) ? Shared.roundedRect(
					      		_.xminmax[0], 
					      		att.itemSize + att.padding.outer,
					      		_.widthminmax,
					      		att.itemSize,
					      		att.radius.top.left * 3, 
					      		att.radius.top.right * 3, 
					      		att.radius.bottom.right * 3, 
					      		att.radius.bottom.left * 3
				      		) : null);
	    			}
	    		},
	    		function(d, i, _){
	    			return att.hide.minmax;
    			}
    		],
    		[
	    		'text',
	    		'minmax--text',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('class', name + ' reverseColor')
							.attr('dy', '1em');
					}

	    			if(status === 'update'){
	    				element.text(Shared.propertyAtString(d, att.minmaxKey))
							.each(function(){
								Shared.ellipse(this, this.textContent, _.widthminmax - att.padding.inner * 3);
							});
	    			}

	    			if(status === 'enter' || status === 'anim'){
						element.attr("transform", "translate(" + 
									(_.xminmax[0] + (_.widthminmax * 0.5)) + 
									", " + 
									((att.itemSize + att.padding.outer) + (att.itemSize * 0.5) - (local.textHeight * 0.5)) + 
								")")
							.attr('opacity', (d.minmax && att.itemSize > (local.textHeight + att.padding.inner * 2)) ? 1 : 0);
	    			}
	    		},
	    		function(d, i, _){
	    			return att.hide.minmax;
    			}
    		],
    		[
	    		'line',
	    		'minmax--line',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
						element.attr("opacity", (d.minmax && d.minmax.length > 2 && (_.xminmax[0] - _.xminmax[2] > att.itemSize * 0.5)) ? 1 : 0)
							.attr('x1', _.xminmax[2])
							.attr('x2', _.xminmax[0])
							.attr('y1', att.itemSize + att.padding.outer + (att.itemSize * 0.5))
							.attr('y2', att.itemSize + att.padding.outer + (att.itemSize * 0.5));
	    			}
	    		},
	    		function(d, i, _){
	    			return att.hide.minmax;
    			}
    		],
    		[
	    		'circle',
	    		'minmax--circle',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
						element.attr("opacity", (d.minmax && d.minmax.length > 2 && (_.xminmax[0] - _.xminmax[2] > att.itemSize * 0.5)) ? 1 : 0)
							.attr('cx', _.xminmax[2])
							.attr('cy', att.itemSize + att.padding.outer + (att.itemSize * 0.5))
							.attr('r', att.itemSize * 0.5);
	    			}
	    		},
	    		function(d, i, _){
	    			return att.hide.minmax;
    			}
    		],
    		[
	    		'text',
	    		'minmax--notif',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('class', name + ' reverseColor')
							.attr('dy', '1em')
							.text('!');
					}

	    			if(status === 'enter' || status === 'anim'){
						element.attr("opacity", (d.minmax && d.minmax.length > 2 && (_.xminmax[0] - _.xminmax[2] > att.itemSize * 0.5)) ? 1 : 0)
							.attr('transform', "translate(" + 
									_.xminmax[2] + 
									", " + 
									((att.itemSize + att.padding.outer) + (att.itemSize * 0.5) - (local.textHeight * 0.5)) + 
								")");
	    			}
	    		},
	    		function(d, i, _){
	    			return att.hide.minmax;
    			}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, defineLocalVariables, localVariables, !i);
    	});

    	var selection = update.selectAll('.level--' + levelIndex + '--mark')
			.data(function(d, i){ return d.marks || [];}, function(d, i){return (d.key != null) ? d.key : i;});

		enter = selection.enter()
	    	.insert("path", '.level--0--gantt--text')
	    	.attr('opacity', 0)
	    	.attr("transform", function(d, i){
				return "translate(" + local.scale.x(att.parseDate(d.value)) + ", " + (att.itemSize * 0.5) + ")";
			});

    	update = enter.merge(selection);

    	update.attr('class', function(d, i){
	    		return 'level--' + levelIndex + '--mark ' + Shared.getKey(att, d, i, 'symbolColors');
	    	})
			.attr('d', d3.symbol().type(function(d, i){
				return d3['symbol' + Shared.getKey(att, d, i, 'symbols')];
			}).size(att.symbolsSize));

    	anim = update;

    	var remove = selection.exit();
    	if(att.transitionSpeed || att.delaySpeed){
    		remove = remove.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed)
				.attr("opacity", 0);

			anim = anim.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed);
    	} else {
    		remove.interrupt();
    		anim.interrupt();
    	}

    	remove.remove();

    	anim.attr("transform", function(d, i){
				return "translate(" + local.scale.x(att.parseDate(d.value)) + ", " + (att.itemSize * 0.5) + ")";
			})
			.attr('opacity', ((att.itemSize - (att.padding.inner * 2)) > local.textHeight) ? 1 : 0);
	};

	Gantt.prototype._level1 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		// LOCAL VARIABLES
    	var localVariables = [];

    	var defineLocalVariables = function(d, i){
			var _ = localVariables[i];

			if(!_){
				_ = localVariables[i] = {};
			}

			_.x = local.scale.x(Shared.min(d.values, 'x', 'value', att.parseDate));

			if(_.x < 0){
				_.x = 0;
			}

			_.width = local.scale.x(Shared.max(d.values, 'x', 'value', att.parseDate)) - _.x;

			if(_.x <= 0){
				if(_.width < 100){
					_.width = 100;
				}
			}

			_.height = 0;

			d.values.forEach(function(dd, ii){
				_.height += att.itemSize + att.padding.outer;

				if(dd.minmax && !att.hide.minmax){
					_.height += att.itemSize + att.padding.outer;
				}
			});

			if(hide){
				_.height -= att.padding.outer;
			} else {
				_.height += att.itemSize * 0.5;
			}
		};

    	// ELEMENTS
    	[
	    	[
	    		['rect', 'hold'],
	    		'container',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
	    				element.attr('x', _.x)
							.attr('width', _.width)
							.attr('height', _.height);
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, defineLocalVariables, localVariables, !i);
    	});
	};

	Gantt.prototype._shared = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		if(!levelIndex){return;}

		var local = this.store, att = local.att, that = this;

		// LEVEL SETUP
		[enter, anim].forEach(function(d){
			d.attr('transform', function(d, i) {
				return "translate(0, " + (((hide) ? 0 : (att.itemSize * 0.5)) + that.y(localData, i)) + ")";
			});
		});

		if(att.transitionSpeed || att.delaySpeed){
    		inner = inner.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed);
    	} else {
    		inner.interrupt();
    	}

    	inner.attr("transform", "translate(0, " + ((hide) ? 0 : ((att.itemSize * 0.5) + att.padding.outer)) + ")");

    	// LOCAL VARIABLES
    	var localVariables = [];

    	var defineLocalVariables = function(d, i){
			var _ = localVariables[i];

			if(!_){
				_ = localVariables[i] = {};
			}

			_.x = local.scale.x(Shared.min(d.values, 'x', 'value', att.parseDate));
				
			if(_.x < 0){
				_.x = 0;
			}

			_.width = Shared.textWidth(d.label);
		};

    	// ELEMENTS
    	[
	    	[
	    		'path',
	    		'group',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
	    				element.attr("d", Shared.roundedRect(
					      		_.x,
					      		-(att.itemSize * 0.5), 
					      		_.width + (att.padding.inner * 2) + (att.itemSize * 0.8) + att.padding.inner, 
					      		att.itemSize, 
					      		att.radius.top.left, 
					      		att.radius.top.right, 
					      		att.radius.bottom.right, 
					      		att.radius.bottom.left
				      		));
	    			}
	    		}
    		],
    		[
	    		'text',
	    		'group--text',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('dy', '1em')
							.style('text-anchor', 'start');
					}

	    			if(status === 'update'){
	    				element.text(d.label);
	    			}

	    			if(status === 'enter' || status === 'anim'){
	    				element.attr('transform', "translate(" + 
		    					(_.x + att.padding.inner) + 
		    					", " + 
		    					-(local.textHeight * 0.5) + 
	    					")")
	    					.attr('opacity', (att.itemSize > (local.textHeight + att.padding.inner * 2)) ? 1 : 0);
	    			}
	    		}
    		],
    		[
	    		'circle',
	    		'group--circle',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter' || status === 'anim'){
						element.attr('cx', _.x + _.width + (att.padding.inner * 5))
							.attr('cy', 0)
							.attr('r', '0.75em')
							.attr('opacity', (att.itemSize > (local.textHeight + att.padding.inner * 2)) ? 1 : 0);
	    			}
	    		}
    		],
    		[
	    		'text',
	    		'group--notif',
	    		function(element, d, i, status, name, _){
					if(status === 'enter'){
						element.attr('class', name + ' reverseColor')
							.attr('dy', '1em')
							.text('i');
					}

	    			if(status === 'enter' || status === 'anim'){
						element.attr('transform', "translate(" + 
									(_.x + _.width + (att.padding.inner * 5)) + 
									", " + 
									-(local.textHeight * 0.5) + 
								")")
							.attr('opacity', (att.itemSize > (local.textHeight + att.padding.inner * 2)) ? 1 : 0);
	    			}
	    		}
    		],
    		[
	    		'circle',
	    		'hover--circle',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter'){
	    				if(att.tooltip['level' + levelIndex]){
	    					element.on("mouseover", function(d){
		    						Chart.showTip(att.tooltip['level' + levelIndex](d));
	    						})
								.on("mousemove", function(){return Chart.positionTip(d3.event.pageX, d3.event.pageY); })
								.on("mouseout", Chart.hideTip);
	    				}
	    			}

	    			if(status === 'enter' || status === 'anim'){
						element.attr('cx', _.x + _.width + (att.padding.inner * 5))
							.attr('cy', 0)
							.attr('r', '0.75em');
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, defineLocalVariables, localVariables, !i);
    	});
	};

	Gantt.prototype.y = function(localData, index){
		var local = this.store, att = local.att;

		var sum = 0;

		var arr = Shared.flattenValuesLengths(localData, [], index);

		for(var level = 0, ilen = arr.length; level < ilen; level++){
			for(var i = 0, jlen = arr[level].length; i < jlen; i++){

				if((level + 1) >= ilen && i >= index){ break; }

				if(!att.hide['level' + level]){
					sum += att.itemSize + att.padding.outer;
				}

				if(arr[level][i].minmax && !att.hide.minmax){
					sum +=att.itemSize + att.padding.outer;
				}
			}
		}
		return sum;
	};

	Gantt.prototype.data = function (value) {
		Chart.prototype.data.call(this, value);	

		this.resize();

		return this;
	};

	return Gantt;
});