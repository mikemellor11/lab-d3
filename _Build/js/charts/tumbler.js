(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Tumbler = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Tumbler(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Tumbler)) { 
			return new Tumbler(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__tumbler', true);

		this.att({
			textFormat: "{value}",
			threshhold: {
				upper: 0.8,
				middle: 0.6,
				lower: 0
			},
			leadingZeros: 0
		});

		Shared.extend(this.store, {
		});
	}

	Tumbler.prototype = Object.create(Chart.prototype);

	Tumbler.prototype.init = function(){
		if(this.store.inited){
			return this;
		}

		this.store.chart.html('');

		this.store.inited = true;

		return this;
	};

	Tumbler.prototype.render = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var value = data[0] && data[0].value || 0;
		var split = value.toString().split('');

		var leadingZeros = att.leadingZeros - split.length;

		if(leadingZeros > 0){
			for(var i = leadingZeros; i--;){
				split.unshift("0");
			}
		}

		chart.transition()
			.ease(d3['ease' + att.transitionType])
			.delay(att.delaySpeed)
			.duration(att.transitionSpeed)
			.tween("text", function() {
	            var that = this;
	            var _that = d3.select(this);
	            var i = d3.interpolate(
	            		local.dataLast.get(chart) || 0, 
	            		data[0] && data[0].value || 0
            		);

	            return function(t) {
	            	var value = parseFloat(i(t));
	            	var total = att.totalCount;
	            	var normal = value / total;

	            	if(normal > att.threshhold.upper){
	            		_that.classed('lower middle', false).classed('upper', true);
	            	} else if(normal > att.threshhold.middle) {
	            		_that.classed('lower upper', false).classed('middle', true);
	            	} else if(normal > att.threshhold.lower) {
	            		_that.classed('middle upper', false).classed('lower', true);
	            	}

	            	local.dataLast.set(chart, value);
	            };
	        });

		var selection = chart.selectAll('.tumble').data(split);

		var enter = selection.enter()
	    	.append("span")
	    	.attr('class', 'tumble')
	    	.attr('style', 'transform: translateY(-5em);')
	    	.style("opacity", 0);

    	var update = enter.merge(selection);

    	var anim = update;

    	var remove = selection.exit();
    	if(att.transitionSpeed || att.delaySpeed){
    		remove = remove.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed)
				.style("opacity", 0);

			anim = anim.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed);
    	} else {
    		remove.interrupt();
    		anim.interrupt();
    	}

    	remove.remove();

    	anim.attr('style', function(d, i){
				return 'transform: translateY(' + (+d - 5) + 'em);';
			})
    		.style("opacity", 1);


    	selection = update.selectAll('.digit').data(function(d, i){
			var arr = Array(11);
			for(var j = arr.length; j--;){
				arr[j] = +d;
			}
			return arr; 
		});			

    	enter = selection.enter().append('span');

    	update = enter.merge(selection);
    	
    	update.attr('class', function(d, i){
    			var name = 'digit';

    			if(i === 10){
    				name += ' placeholder';
    			}

    			if(i === d){
    				name += ' active';
    			}

    			return name;
    		})
    		.text(function(d, i){
	    		return (i === 10) ? 0 : i;
	    	})
	    	.style('top', function(d, i){
	    		var pos = i;

	    		pos -= 5;

	    		return -pos + 'em';
	    	});

		return this;
	};

	Tumbler.prototype.destroy = function(){
		this.store.chart.interrupt()
			.attr('class', '')
			.html('');
	};

	return Tumbler;
});