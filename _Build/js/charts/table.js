(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Table = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Table(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Table)) { 
			return new Table(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__table', true);

		this.att({
			updated: null
		});

		Shared.extend(this.store, {
		});
	}

	Table.prototype = Object.create(Chart.prototype);

	Table.prototype.init = function(){
		return this;
	};

	Table.prototype.render = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		this.buildTable(chart, data);

		return this;
	};

	Table.prototype.destroy = function(){
		this.store.chart.interrupt()
			.attr('class', '')
			.html('');
	};

	Table.prototype.buildTable = function(element, data) {
		var local = this.store, att = local.att;

		var selection = element.selectAll("tr")
				.data(data, function(d, i){return (d.key != null) ? d.key : i;});

		selection.order();

		var enter = selection.enter()
	    	.append("tr")
	    	.attr('class', function (d) { if (d.classes) { return d.classes.join(' '); } });

    	var update = enter.merge(selection);

    	selection.exit().remove();

    	var inner = update.selectAll("td")
    		.data(function(d){ return d.values;}, function(d, i){return (d.key != null) ? d.key : i;});

		selection = inner.enter()
	    	.append("td")
	    	.classed('new', true);

    	inner.classed('new', false);

    	update = inner.merge(selection);

    	inner.exit().remove();

    	update.attr('rowspan', function (d) { if (d.rowspan) { return d.rowspan; } })
			.attr('colspan', function (d) { if (d.colspan) { return d.colspan; } })
	    	.attr('class', function (d) {
	    		var temp = '';

	    		if(d3.select(this).classed('new')){
	    			temp += 'new ';
	    		}

	    		var last = local.dataLast.get(this);

	    		if(last != null && last !== d.value){
	    			temp += 'updated';

	    			if(att.updated){
	    				att.updated(this, d, last);
	    			}
	    		}

	    		if (d.classes) { temp += d.classes.join(' '); }

	    		return temp;
	    	})
	    	.each(function(d){
	    		var select = d3.select(this);

	    		local.dataLast.set(this, d.value);

	    		select.html(function(d){
		    		return d.value;
		    	});

	    		if(d.values){
	    			var selection = select.selectAll("table")
							.data(d.values, function(d, i){return (d.key != null) ? d.key : i;});

					var enter = selection.enter()
				    	.append("table")
				    	.attr('class', function (d) { if (d.classes) { return d.classes.join(' '); } });

			    	var update = enter.merge(selection);

			    	selection.exit().remove();

		    		this.buildTable(update, d.values);
	    		}
	    	});
    };

	return Table;
});