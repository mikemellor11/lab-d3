(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', 'moment', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'), require('moment'));
	} else {
		root.Datepicker = factory(root.d3, root.Shared, root.Chart, root.moment);
	}
})(this || window, function (d3, Shared, Chart, Moment) {
	"use strict";

	function Datepicker(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Datepicker)) { 
			return new Datepicker(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__datepicker', true);

		this.att({
		});

		Shared.extend(this.store, {
			level: null,
			picked: {
				day: null,
				month: null,
				year: null
			}
		});
	}

	Datepicker.prototype = Object.create(Chart.prototype);

	Datepicker.prototype.init = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var that = this;

		local.level = 2;

		chart.each(function(){
			this.onclick = function(e){
				var date = e.target.dataset.date;
				var level = e.target.dataset.level;
				var dir = e.target.dataset.dir;
				var temp;

				if(dir){
					var forward = (dir === 'right');
					if(local.level === 2){
						if(forward){
							local.picked.year += 10;
						} else {
							local.picked.year -= 10;
						}
						that.yearpick();
					} else if(local.level === 1){
						if(forward){
							local.picked.year += 1;
						} else {
							local.picked.year -= 1;
						}
						that.monthpick();
					} else {
						if(forward){
							local.picked.month += 1;
						} else {
							local.picked.month -= 1;
						}
						if(local.picked.month > 12){
							local.picked.month = 1;
							local.picked.year += 1;
						}
						if(local.picked.month < 1){
							local.picked.month = 12;
							local.picked.year -= 1;
						}
						that.daypick();
					}
				} else if(level != null){
					level = +level;

					if(level === 1){
						local.level = 2;
						that.yearpick();
					} else if(level === 0){
						local.level = 1;
						that.monthpick();
					}
				}
				else if(date){
					if(local.level === 0){
						local.picked.day = Moment(date).date();
						local.picked.month = Moment(date).month() + 1;
						local.picked.year = Moment(date).year();
						that.daypick();

						if(att.cb){
							att.cb(date);
						}
					} else if(local.level === 1) {
						temp = Moment(date).month() + 1;

						if(temp !== local.picked.month){
							local.picked.day = 1;
						}

						local.picked.month = temp;
						local.level -= 1;
						that.daypick();
					} else if(local.level === 2) {
						temp = Moment(date).year();

						if(temp !== local.picked.year){
							local.picked.day = 1;
							local.picked.month = 1;
						}
						
						local.picked.year = temp;
						local.level -= 1;
						that.monthpick();
					}
				}
		    };
		});

		return this;
	};

	Datepicker.prototype.render = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		if(!data.length){
			this.data([]);
		}

		if(local.level === 0){
			this.daypick();
		} else if(local.level === 1){
			this.monthpick();
		} else {
			this.yearpick();
		}

		return this;
	};

	Datepicker.prototype.yearpick = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var that = this;

		var decade = (Math.floor(local.picked.year / 10) * 10) - 1;

		chart.html('');

		var html = '';

			html += '<tr>';

				html += '<th><button data-dir="left">&larr;</button></th>';

				html += '<th colspan="2" class="root">';

					html += '<button data-level="2">';

						html += decade + '-' + (decade + 10);

					html += '</button>';

				html += '</th>';

				html += '<th><button data-dir="right">&rarr;</button></th>';

			html += '</tr>';

			var count = 0;

			for(var i = 0; i < 12; i++){
				var d = decade + i;

				if(!count){
					html += '<tr>';
				}

				html += '<td class="select ';

				if(local.picked.year === d){
					html += 'today ';
				}

				if(Moment(that.constructDate(d, 1, 1)).isBefore(Moment(att.min.x), 'year')){
					html += 'limit ';
				}

				if(Moment(that.constructDate(d, 1, 1)).isAfter(Moment(att.max.x), 'year')){
					html += 'limit ';
				}

				if(i > 10){
					html += 'different ';
				}

				html += '">';

				html += '<div><div><button data-date="' + that.constructDate(d, 1, 1) + '">';

				html += d + '</button></div></div></td>';

				if(++count > 3){
					html += '</tr>';
					count = 0;
				}
			}

		chart.html(html);
	};

	Datepicker.prototype.monthpick = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var that = this;

		chart.html('');

		var html = '';

		html += '<tr>';

		html += '<th><button data-dir="left">&larr;</button></th>';

		html += '<th colspan="2" class="root"><button data-level="1">' +  Moment(that.pickedDate()).format('YYYY') + '</button></th>';

		html += '<th><button data-dir="right">&rarr;</button></th>';

		html += '</tr>';

		var count = 0;

		Moment().localeData().monthsShort().forEach(function(d, i){
			if(!count){
				html += '<tr>';
			}

			html += '<td class="select ';

			if(Moment(that.pickedDate()).isSame(Moment(that.constructDate(local.picked.year, i + 1, 1)), 'month')){
				html += 'today ';
			}

			if(Moment(that.constructDate(local.picked.year, i + 1, 1)).isBefore(Moment(att.min.x), 'month')){
				html += 'limit ';
			}

			if(Moment(that.constructDate(local.picked.year, i + 1, 1)).isAfter(Moment(att.max.x), 'month')){
				html += 'limit ';
			}

			html += '"';

			html += '><div><div><button data-date="' + that.constructDate(local.picked.year, i + 1, 1) + '">';

			html += d + '</button></div></div></td>';

			if(++count > 3){
				html += '</tr>';
				count = 0;
			}
		});

		chart.html(html);
	};

	Datepicker.prototype.daypick = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		chart.html('');

		var startWeek = Moment(this.pickedDate()).startOf('month').week();
		var endWeek = Moment(this.pickedDate()).endOf('month').week();

		if(endWeek < startWeek){
			endWeek = Moment().weeksInYear() + 1;
		}

		var calendar = [];
		var week;

		var that = this;

		var func = function(n, i){
			return Moment(that.pickedDate()).startOf('month').week(week).startOf('week').clone().add(n + i, 'day');
		};

		for(week = startWeek; week <= endWeek; week++){
			calendar.push({
				days: [0, 0, 0, 0, 0, 0, 0].map(func)
			});
		}

		var html = '';

		html += '<tr>';

		html += '<th><button data-dir="left">&larr;</button></th>';

		html += '<th colspan="5" class="root"><button data-level="0">' + Moment(that.pickedDate()).format('YYYY MMMM') + '</button></th>';

		html += '<th><button data-dir="right">&rarr;</button></th>';

		html += '</tr>';

		html += '<tr>';

		html += calendar[0].days.reduce(function(html, day){
					return html + '<td class="label">' + day.format('ddd') + '</td>';
				}, '');

		html += '</tr>';

		html += calendar.reduce(function(html, day){
			var count = 0;
			return html + day.days.reduce(function(html, day){
					if(!count){
						html += '<tr>';
					}

					var newHtml = '<td class="select ';

					if(day.isSame(Moment(that.pickedDate()), 'day')){
						newHtml += 'today ';
					}

					if(!day.isSame(Moment(that.pickedDate()), 'month')){
						newHtml += 'different ';
					}

					if(day.isBefore(Moment(att.min.x), 'day')){
						newHtml += 'limit ';
					}

					if(day.isAfter(Moment(att.max.x), 'day')){
						newHtml += 'limit ';
					}

					newHtml += '"';

					newHtml += '><div><div><button data-date="' + day.format('YYYY-MM-DD') + '">';

					newHtml += day.format('D') + '</button></div></div></td>';

					if(++count > 7){
						html += '</tr>';
						count = 0;
					}

					return html + newHtml;
				}, '');
			}, '');

		chart.html(html);
	};

	Datepicker.prototype.pickedDate = function(year, month, day){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		return this.constructDate(local.picked.year, local.picked.month, local.picked.day);
	};

	Datepicker.prototype.constructDate = function(year, month, day){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		var string = '' + year;

		string += '-' + ((month < 10) ? '0' : '') + month;

		string += '-' + ((day < 10) ? '0' : '') + day;

		return string;
	};

	Datepicker.prototype.data = function (value) {
		if (!arguments.length) {
			return this.store.data;
		}

		var initialDate;

		if(value.length){
			initialDate = new Date(value[0].value);
		} else {
			initialDate = new Date();
		}

		this.store.picked = {
			day: initialDate.getDate(),
			month: initialDate.getMonth() + 1,
			year: initialDate.getFullYear()
		};

		this.store.data = value;

		return this;
	};

	return Datepicker;
});