(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_group', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_group'));
	} else {
		root.Block = factory(root.d3, root.Shared, root.Chart_Group);
	}
})(this || window, function (d3, Shared, Chart_Group) {
	"use strict";

	function Block(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Block)) { 
			return new Block(selector);
		}

		Chart_Group.call(this, selector);

		this.store.chart.classed('labD3__block', true);

		Shared.extend(this.store, {
			baseBlock: 0
		});

		this.att({
			scale: {
				x: 'group',
				y: null
			},
			blockGap: 0.05
		});
	}

	Block.prototype = Object.create(Chart_Group.prototype);

	Block.prototype.init = function(){
		var local = this.store, chart = local.chart;

		if(local.inited){
			return this;
		}

		Chart_Group.prototype.init.call(this);

		chart.select(".y.axis")
			.append("g")
			.attr('class', 'tick')
			.append("line")
			.attr('x1', "0.5")
			.attr('x2', "0.5");

		return this;
	};

	Block.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		Chart_Group.prototype._additionalSetup.call(this);

		if(att.axis.x.tickSizeInner == null || att.axis.x.tickSizeInner >= 0){
			chart.select(".y.axis")
				.attr('transform', 'translate(' + att.margin.left + ', ' + att.margin.top + ')')
				.select(".tick")
				.attr('transform', 'translate(0, 0)')
				.select('line')
				.attr('y2', local.height);
		}

		var levelData = Shared.flattenValuesLengths(data, [], data.length);

		for(var levelIndex = levelData.length; levelIndex--;){
			var blocks = d3.max(local.flatData, function(d){
				return d.value;
			});

			var width = local.levelWidth[levelIndex];
			var height = local.height - 37 - (att.spacePadding * 2); // Fix

			local.baseBlock = 1;
			var blockWidth = width / local.baseBlock;
			var blockHeight = (blocks / local.baseBlock) * blockWidth;

			while(blockHeight > height && blockWidth > 1){
				local.baseBlock++;
				blockWidth = width / local.baseBlock;
				blockHeight = (blocks / local.baseBlock) * blockWidth;
			}
		}
	};

	Block.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		var width = local.levelWidth[levelIndex];
		var blockWidth = parseInt(width / local.baseBlock, 10);
		var blockWidthHalf = blockWidth * 0.5;
		var blockGap = att.blockGap * blockWidthHalf;
		var blockGapDouble = blockGap * 2;
		var base = local.height - (blockWidth - blockGapDouble);

		// LOCAL VARIABLES
    	var localVariables = [];

    	var defineLocalVariables = function(d, i){
			var _ = localVariables[i];

			if(!_){
				_ = localVariables[i] = {};
			}

			_.x = width * 0.5;
			_.y = base - (blockWidth * parseInt((d.value - 1) / local.baseBlock, 10));
		};

		// ELEMENTS
    	[
	    	[
	    		'text',
	    		'value',
	    		function(element, d, i, status, name, _){
	    			if(status === 'enter'){
						element.attr('dy', -(att.spacePadding * 2))
							.attr('opacity', 0)
							.attr('transform', "translate(" + _.x + ", " + base + ")")
							.text(0);
					}

	    			if(status === 'enter' || status === 'update'){
	    				element.style('font-size', 30 + 'px')
	    					.attr('class', name + ' ' + Shared.getKey(att, d, i, 'colors'));
	    			}

	    			if(status === 'anim'){
	    				if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (i * att.stagger);
							});
						}

	    				element.attr('transform', "translate(" + _.x + ", " + _.y + ")")
	    					.attr('opacity', 1);

    					if(element.tween){
    						element.tween("text", function(d, i) {
						            var j = d3.interpolate(local.dataLast.get(this) || 0, +d.value);
						            var that = this;
						            return function(t) {
						            	var value = parseFloat(j(t));

						            	local.dataLast.set(that, d.value);

						                that.textContent = Shared.format(
							                	d.valueLabel || att.valueLabel, 
							                	value.toFixed(att.decimalPlaces), 
							                	Math.round(((value.toFixed(0) / att.totalCount) * att.totalCount)), 
							                	att.totalCount
						                	);
						            };
						        });
    					} else {
    						element.text(Shared.format(
				                	d.valueLabel || att.valueLabel, 
				                	parseFloat(d.value).toFixed(att.decimalPlaces), 
				                	Math.round(((parseFloat(d.value).toFixed(0) / att.totalCount) * att.totalCount)), 
				                	att.totalCount
			                	));
    					}
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, defineLocalVariables, localVariables, !i);
    	});

	    var totalTime = 0;

    	var selection = update.selectAll('.level--0--block')
    		.data(function(d, i){
    			var arr = Array(Math.round(d.value));

    			for(var j = arr.length; j--;){
    				arr[j] = {
	    				length: d.value,
	    				i: i
	    			};
    			}

    			return arr; 
    		});
    	
    	enter = selection.enter()
    		.append('rect')
    		.attr('class', function(d, i){
    			return 'level--0--block ' + Shared.getKey(att, d, d.i, 'colors');
    		})
    		.attr('opacity', 0)
    		.attr('y', 0)
    		.attr('width', 0)
	    	.attr('height', 0)
	    	.attr('x', function(d, i){
	    		return (blockWidth * (i % local.baseBlock)) + blockGap + ((blockWidth - blockGapDouble) * 0.5);
	    	});

	    update = enter.merge(selection);

	    anim = update;

    	var remove = selection.exit();
    	if(att.transitionSpeed || att.delaySpeed){
    		remove = remove.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(att.delaySpeed)
				.duration(att.transitionSpeed)
				.attr("opacity", 0);

			anim = anim.transition()
				.ease(d3['ease' + att.transitionType])
				.delay(function(d, i){
					totalTime = att.transitionSpeed / d.length;

					return att.delaySpeed + (d.i * att.stagger) + (totalTime * i);
				})
				.duration(att.transitionSpeed);
    	} else {
    		remove.interrupt();
    		anim.interrupt();
    	}

    	remove.remove();

		anim.attr('x', function(d, i){
	    		return (blockWidth * (i % local.baseBlock)) + blockGap;
	    	})
			.attr('width', (blockWidth - blockGapDouble))
	    	.attr('height', (blockWidth - blockGapDouble))
			.attr('opacity', 1)
	    	.attr('y', function(d, i){
	    		return base - (parseInt(i / local.baseBlock, 10) * blockWidth);
	    	});
	};

	return Block;
});