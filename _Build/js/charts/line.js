(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Line = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Line(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Line)) { 
			return new Line(selector);
		}

		Chart.call(this, selector);

		this.store.chart.classed('labD3__line', true);

		Shared.extend(this.store, {
		});

		this.att({
			scale: {
				x: 'band'
			},
			interpolation : 'Linear'
		});
	}

	Line.prototype = Object.create(Chart.prototype);

	Line.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

    	// ELEMENTS
    	[
    		[
	    		'path',
	    		'plot',
	    		function(element, d, i, status, name, _){
	    			if(status === 'update'){
	    				element.attr('opacity', 0)
	    					.attr('class', name + ' ' + att.colors[localIndex % att.colors.length])
	    					.attr('d', d3.symbol()
	    						.type(d3['symbol' + Shared.getKey(att, d, i, 'symbols')])
	    						.size(att.symbolsSize)
	    					)
	    					.attr("transform", "translate(" + 
									(local.scale.x(Shared.x(d, att.plot.x)) + (local.scale.x.bandwidth && local.scale.x.bandwidth() * 0.5 || 0)) + ", " + 
									(local.scale.y(Shared.y(d, att.plot.y)) + (local.scale.y.bandwidth && local.scale.y.bandwidth() * 0.5 || 0)) + 
								")");
	    			}

	    			if(status === 'anim'){
	    				if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (localIndex * att.stagger);
							});
						}

						element.attr('opacity', 1);
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	Line.prototype._level1 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;

		var line = d3.line()
			.curve(d3['curve' + att.interpolation])
			.defined(function(d) { 
				return d.value !== null; 
			})
		    .x(function(d) {
		    	return local.scale.x(Shared.x(d, att.plot.x)) + (local.scale.x.bandwidth && local.scale.x.bandwidth() * 0.5 || 0);
		    })
		    .y(function(d) { 
		    	return local.scale.y(Shared.y(d, att.plot.y)) + (local.scale.y.bandwidth && local.scale.y.bandwidth() * 0.5 || 0); 
		    });

    	// ELEMENTS
    	[
    		[
	    		['path', 'hold'],
	    		'line',
	    		function(element, d, i, status, name, _){
	    			if(status === 'update'){
	    				element.classed('line__stroke ' + att.colors[i % att.colors.length] + '-stroked', true)
	    					.attr("d", line(d.values));
	    			}

	    			if(status === 'update'){
	    				var totalLength = element.node().getTotalLength();

	    				element.attr("stroke-dasharray", totalLength + " " + totalLength)
      						.attr("stroke-dashoffset", totalLength); // DASHOFFSET CANNOT BE INTERRUPTED
	    			}

	    			if(status === 'anim'){
	    				if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (i * att.stagger);
							});
						}
	    				
	    				element.attr("stroke-dashoffset", 0);
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	return Line;
});