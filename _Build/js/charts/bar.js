(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart_group_x', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart_group'));
	} else {
		root.Bar = factory(root.d3, root.Shared, root.Chart_Group);
	}
})(this || window, function (d3, Shared, Chart_Group) {
	"use strict";

	function Bar(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Bar)) { 
			return new Bar(selector);
		}

		Chart_Group.call(this, selector);

		this.store.chart.classed('labD3__bar', true);

		Shared.extend(this.store, {
		});

		this.att({
			scale: {
				x: 'group'
			},
			padding: {
				outer: 0.025
			},
			minmax: 0.1
		});
	}

	Bar.prototype = Object.create(Chart_Group.prototype);

	Bar.prototype._level0 = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this;
		var tempScale;
		var tempReverse;

		if(att.scale.x === 'group'){
			tempScale = local.scale.y;
			tempReverse = att.axis.y;
		} else if(att.scale.y === 'group'){
			tempScale = local.scale.x;
			tempReverse = att.axis.x;
		} else {
			return;
		}

		var minValue = tempScale.domain()[0];
	    			
		if(minValue < 0){
			minValue = 0;
		}

		var base = tempScale(minValue);
		var width = local.levelWidth[levelIndex];
		var x = width * 0.5;

    	// ELEMENTS
    	[
	    	[
	    		'path',
	    		'bar',
	    		function(element, d, i, status, name, _){
	    			var val = att.chockData ? minValue : d.value;

	    			if(status === 'enter'){
	    				if(att.scale.x === 'group'){
		    				element.attr("d", Shared.roundedRect(0, base, width, 0, 0, 0, 0, 0));
		    			} else if(att.scale.y === 'group'){
		    				element.attr("d", Shared.roundedRect(base, 0, 0, width, 0, 0, 0, 0));
		    			} else {

		    			}
	    			}

	    			if(status === 'enter' || status === 'update'){
	    				element.attr('class', name + ' ' + Shared.getKey(att, d, i, 'colors'))
	    					.attr('opacity', 1);
	    			}

	    			if(status === 'anim'){
	    				if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								// localLevels should be an array of all localIndexs from root
								return att.delaySpeed + (((local.levels) ? localIndex : i) * att.stagger);
							});
						}

						var y = tempScale(val);
		    			var height = Math.abs(base - y);

		    			var radius = [
		    				att.radius.top.left,
		    				att.radius.top.right,
		    				att.radius.bottom.right,
		    				att.radius.bottom.left
	    				];

						var reversed;
						var calcY;

	    				if(val > 0){
	    					reversed = tempReverse.reverse;
	    				} else {
	    					reversed = !tempReverse.reverse;
	    				}

	    				if(reversed){
	    					if(att.scale.x === 'group'){
		    					radius[0] = 0;
				    			radius[1] = 0;
				    			calcY = base;
				    		} else if(att.scale.y === 'group'){
				    			radius[1] = 0;
				    			radius[2] = 0;
				    			calcY = y;
				    		} else {

				    		}
	    				} else {
	    					if(att.scale.x === 'group'){
		    					radius[2] = 0;
				    			radius[3] = 0;
		    					calcY = y;
				    		} else if(att.scale.y === 'group'){
				    			radius[0] = 0;
				    			radius[3] = 0;
		    					calcY = base;
				    		} else {

				    		}
	    				}

	    				for(var j = radius.length; j--;){
	    					if(radius[j] > height){
	    						radius[j] = height;
	    					}
	    				}

	    				if(att.scale.x === 'group'){
	    					element.attr("d", Shared.roundedRect(
						      		0, calcY, width, height,
						      		radius[0], radius[1], radius[2], radius[3]
					      		));
		    			} else if(att.scale.y === 'group'){
		    				element.attr("d", Shared.roundedRect(
						      		calcY, 0, height, width,
						      		radius[0], radius[1], radius[2], radius[3]
					      		));
		    			} else {

		    			}
	    			}
	    		}
    		],
    		[
	    		'text',
	    		'value',
	    		function(element, d, i, status, name, _){
	    			var val = att.chockData ? minValue : d.value;
	    			var y = (status === 'enter') ? base : tempScale(val);

	    			var reversed;

					if(d.value >= 0){
    					reversed = tempReverse.reverse;
    				} else {
    					reversed = !tempReverse.reverse;
    				}

    				// 15 half of fontsize above TODO - make var / customizable in css
    				// textWidth below doubles it, rought approx, needs changing
    				if(reversed){
    					if(att.scale.x === 'group'){
	    					y += (att.spacePadding + 15);
		    			} else if(att.scale.y === 'group'){
		    				y += -(((Shared.textWidth(val) * 2) * 0.5) + att.spacePadding);
		    			} else {
		    				
		    			}
    				} else {
    					if(att.scale.x === 'group'){
	    					y += -(att.spacePadding + 15); 
		    			} else if(att.scale.y === 'group'){
		    				y += ((Shared.textWidth(val) * 2) * 0.5) + att.spacePadding;
		    			} else {
		    				
		    			}
    				}

	    			if(status === 'enter'){
	    				if(att.scale.x === 'group'){
	    					element.attr('dy', "0.3em")
								.attr('opacity', 0)
								.attr('transform', "translate(" + x + ", " + y + ")")
								.text(0);
		    			} else if(att.scale.y === 'group'){
		    				element.attr('dy', "0.3em")
								.attr('opacity', 0)
								.attr('transform', "translate(" + y + ", " + x + ")")
								.text(0);
		    			} else {

		    			}
					}

	    			if(status === 'enter' || status === 'update'){
	    				element.style('font-size', 30 + 'px')
	    					.attr('class', name + ' ' + Shared.getKey(att, d, i, 'colors'));
	    			}

	    			if(status === 'anim'){
	    				if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (((local.levels) ? localIndex : i) * att.stagger);
							});
						}

	    				if(att.scale.x === 'group'){
	    					element.attr('transform', "translate(" + x + ", " + y + ")")
	    						.attr('opacity', 1);
		    			} else if(att.scale.y === 'group'){
		    				element.attr('transform', "translate(" + y + ", " + x + ")")
	    						.attr('opacity', 1);
		    			} else {
		    				
		    			}
	    				

    					if(element.tween){
    						element.tween("text", function(d, i) {
					            var j = d3.interpolate(local.dataLast.get(this) || 0, +val);
					            var that = this;
					            return function(t) {
					            	var value = parseFloat(j(t));

					            	local.dataLast.set(that, val);

					                that.textContent = Shared.format(
						                	d.valueLabel || att.valueLabel, 
						                	value.toFixed(att.decimalPlaces), 
						                	Math.round(((value.toFixed(0) / att.totalCount) * att.totalCount)), 
						                	att.totalCount
					                	);
					            };
					        });
    					} else {
    						element.text(Shared.format(
				                	d.valueLabel || att.valueLabel, 
				                	parseFloat(val).toFixed(att.decimalPlaces), 
				                	Math.round(((parseFloat(val).toFixed(0) / att.totalCount) * att.totalCount)), 
				                	att.totalCount
			                	));
    					}
	    			}
	    		}
    		],
    		[
    			'line',
    			'max',
    			function(element, d, i, status, name, _){
    				var tempWidth = x * att.minmax;
	    			var y = tempScale(d.max) || 0;

	    			if(status === 'enter'){
	    				element.attr(local.pA[1] + '1', base)
							.attr(local.pA[1] + '2', base);
	    			}

    				if(status === 'anim'){
    					if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (((local.levels) ? localIndex : i) * att.stagger);
							});
						}
						
    					element.attr(local.pA[1] + '1', y)
							.attr(local.pA[1] + '2', y);
    				}

    				if(status === 'enter' || status === 'anim'){
    					element.attr(local.pA[0] + '1', x + tempWidth)
							.attr(local.pA[0] + '2', x - tempWidth);
    				}
    			},
    			function(d, i, _){
	    			return d.max == null;
    			}
    		],
    		[
    			'line',
    			'min',
    			function(element, d, i, status, name, _){
    				var tempWidth = x * att.minmax;
	    			var y = tempScale(d.min) || 0;

	    			if(status === 'enter'){
	    				element.attr(local.pA[1] + '1', base)
							.attr(local.pA[1] + '2', base);
	    			}

    				if(status === 'anim'){
    					if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (((local.levels) ? localIndex : i) * att.stagger);
							});
						}
						
    					element.attr(local.pA[1] + '1', y)
							.attr(local.pA[1] + '2', y);
    				}

    				if(status === 'enter' || status === 'anim'){
    					element.attr(local.pA[0] + '1', x + tempWidth)
							.attr(local.pA[0] + '2', x - tempWidth);
    				}
    			},
    			function(d, i, _){
	    			return d.min == null;
    			}
    		],
    		[
    			'line',
    			'connect',
    			function(element, d, i, status, name, _){
    				var val = att.chockData ? minValue : d.value;
    				
    				var yMin = tempScale(d.min) || tempScale(val);
    				var yMax = tempScale(d.max) || tempScale(val);

	    			if(status === 'enter'){
	    				element.attr(local.pA[1] + '1', base)
							.attr(local.pA[1] + '2', base);
	    			}

    				if(status === 'anim'){
    					if(att.stagger && att.transitionSpeed){
							element.delay(function(){
								return att.delaySpeed + (((local.levels) ? localIndex : i) * att.stagger);
							});
						}
						
    					element.attr(local.pA[1] + '1', yMin)
							.attr(local.pA[1] + '2', yMax);
    				}

    				if(status === 'enter' || status === 'anim'){
    					element.attr(local.pA[0] + '1', x)
							.attr(local.pA[0] + '2', x);
    				}
    			},
    			function(d, i, _){
	    			return d.min == null && d.max == null;
    			}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	return Bar;
});