"use strict";

(function(){
    if(navigator.userAgent === 'jsdom'){ return; }
    
	var FastClick = require('fastclick');
	var d3 = require('d3');
	var Table = require('table');

	document.querySelector('html').classList.remove('no-js');
    document.querySelector('html').classList.add('js');

    FastClick(document.body);

    d3.json('media/content/archive/pneumovax/piano.json', function(err, json){
    	if(!err){
    		new Table('.labD3__piano')
				.data(json.data)
				.init()
				.render();
    	}
    });

    var cycle = 0;
    var modifiers = [
    	'pcv13',
    	'pneu23',
    	'both'
    ];

    setInterval(function(){
    	document.querySelector('.labD3__piano').className = 'labD3__piano labD3__table';
    	document.querySelector('.labD3__piano').classList.add(modifiers[cycle]);

    	cycle++;

    	if(cycle > (modifiers.length - 1)){
    		cycle = 0;
    	}
    }, 2000);
})();