(function (root, factory) {
    "use strict";
    if ( typeof define === 'function' && define.amd ) {
        define('d3', 'shared', factory);
    } else if ( typeof exports === 'object' ) {
        module.exports = factory(require('d3'), require('shared'));
    } else {
        root.Atts = factory(root.d3, root.Shared);
    }
})(this || window, function (d3, Shared) {
    "use strict";

    var formatDate = d3.timeFormat('%Y-%m-%d');
    var parseDate = d3.timeParse("%Y-%m-%d");

    return {
		Gantt: {
			class: require('charts/gantt.js'),
			data: {
				data: {
					values: 2,
					type: 'date',
					accum: true,
					marks: true,
					minmax: true
				},
				levels: 1,
				level: {
					min: 1,
					max: 3
				},
				formatDate: formatDate
			},
			att: {
				min: {
					x: '2000-01-01'
				},
				max: {
					x: '2002-01-01'
				}
			}
		},
		Bar: {
			class: require('charts/bar.js'),
			data: {
			},
			att: {
				min: {
					y: 0
				},
				margin: {
					left: 40,
					bottom: 0
				}
			}
		},
		Block: {
			class: require('charts/block.js'),
			data: {
			},
			att: {
			}
		},
		Line: {
			class: require('charts/line.js'),
			data: {
				levels: 1,
				level: {
					min: 2,
					max: 4
				}
			},
			att: {
				margin: {
					left: 40
				}
			}
		},
		"Map - World": {
			class: require('charts/worldmap.js'),
			data: {
				data: {
					key: 'world'
				},
				levels: 0,
				level: {
					min: 1,
					max: 10
				}
			},
			att: {
			}
		},
		"Map - State": {
			class: require('charts/statemap.js'),
			data: {
				data: {
					key: 'state'
				},
				levels: 0,
				level: {
					min: 1,
					max: 10
				}
			},
			att: {
			}
		},
		Pie: {
			class: require('charts/pie.js'),
			data: {
				levels: 0,
				level: {
					min: 2,
					max: 5
				}
			}
		},
		Harveyball: {
			class: require('charts/harveyball.js'),
			data: {
				levels: 0,
				level: {
					min: 1,
					max: 1
				}
			}
		},
		Brush: {
			class: require('charts/brush.js'),
			data: {
				data: {
					type: 'date',
					values: 2,
					accum: true
				},
				levels: 0,
				level: {
					min: 1,
					max: 1
				},
				formatDate: formatDate
			},
			att: {
				min: {
					x: '2000-01-01'
				},
				max: {
					x: '2002-01-01'
				},
				formatDate: formatDate
			}
		},
		Table: {
			class: require('charts/table.js'),
			data: {
				levels: 1,
				level: {
					min: 5,
					max: 5
				}
			},
			att: {
			},
			root: 'table'
		},
		Text: {
			class: require('charts/text.js'),
			data: {
				levels: 0,
				level: {
					min: 1,
					max: 1
				}
			},
			att: {
			},
			root: 'p'
		},
		Tumbler: {
			class: require('charts/tumbler.js'),
			data: {
				levels: 0,
				level: {
					min: 1,
					max: 1
				}
			},
			att: {
				leadingZeros: 3
			},
			root: 'p'
		},
		Datepicker: {
			class: require('charts/datepicker.js'),
			data: {
				data: {
					type: 'date'
				},
				levels: 0,
				level: {
					min: 1,
					max: 1
				},
				formatDate: formatDate
			},
			att: {
			},
			root: 'table'
		}
    };
});