"use strict";

require('prettyprint');

(function(){
	if(navigator.userAgent === 'jsdom'){ return; }
	
	var FastClick = require('fastclick');
	var Random = require('random');
	var Charts = require('atts');
	var d3 = require('d3');
	var State = require('state');

	document.querySelector('html').classList.remove('no-js');
    document.querySelector('html').classList.add('js');

    FastClick(document.body);

	var selectorTypes = {
		svg: document.getElementById('chart--svg'),
		table: document.getElementById('chart--table'),
		p: document.getElementById('chart--p')
	};

	// Only run the following code on main index page
	if(!selectorTypes.svg){
		return;
	}

	var myChart;
	var chart;
	var selector;
	var interval = null;

	init();

	loadInterval(State.get('toggleRandom'));

	function init(){
		chart = Charts[decodeURIComponent(window.location.hash.substr(1)) || 'Bar'];
		selector = selectorTypes[chart.root || 'svg'];

		myChart = new chart.class(selector);
		
		myChart.att({
			width: selector.parentNode.clientWidth,
			stagger: 0
		})
		.att(chart.att)
		.init()
		.renderSync();

		/*setTimeout(function(){
			myChart.data([{value: 10}])
				.render();
		}, 1000);

		setTimeout(function(){
			myChart.data([{value: 55555555}])
				.render();
		}, 2000);*/
	}

	function update(){
		if(myChart){
			myChart.data(Random.data(chart.data))
				.render();
		}
	}

	function loadInterval(on){
		if(on){
			document.querySelector('#toggleRandom').classList.add('active');
			document.querySelector('html').classList.add('toggleRandom');
		} else {
			document.querySelector('#toggleRandom').classList.remove('active');
			document.querySelector('html').classList.remove('toggleRandom');
		}

        State.set('toggleRandom', on);

		if(interval){
			clearInterval(interval);
			interval = null;
		} else {
			update();
		}

		if(on){
			interval = setInterval(function(){
				update();
			}, 2000);
		}
	}

	document.querySelector('#toggleRandom').onclick = function(){
        loadInterval(!State.get('toggleRandom'));
    };

	window.onresize = function() {
		myChart.att({
				width: selector.parentNode.clientWidth
			})
			.resize()
			.renderSync();
    };

	window.onhashchange = function(){
		myChart.destroy();

		selector.parentNode.style.padding = 0;

		myChart = null;
		
		init();
		update();
	};
})();