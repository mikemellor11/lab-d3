(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Chart_Pie = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Chart_Pie(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Chart_Pie)) { 
			return new Chart_Pie(selector);
		}

		Chart.call(this, selector);

		Shared.extend(this.store, {
			modify: d3.pie().sort(null).value(function(d){ return d.value; }),
			arc: null,
			radius: null,
			delaySpeed: null,
			transitionSpeed: null
		});

		this.att({
			innerRadius: 0,
			startAngle: 0,
			clockwise : true,
			showLabels : false,
			scale: {
				x: null,
				y: null
			},
			startOpacity: 1
		});
	}

	Chart_Pie.prototype = Object.create(Chart.prototype);

	Chart_Pie.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, data = local.data;

		local.bottom.attr("transform", "translate(" + (att.width * 0.5) + "," + (att.height * 0.5) + ")");
		local.draw.attr("transform", "translate(" + (att.width * 0.5) + "," + (att.height * 0.5) + ")");
		local.top.attr("transform", "translate(" + (att.width * 0.5) + "," + (att.height * 0.5) + ")");

		local.radius = Math.min(local.width, local.height) * 0.5;

		local.arc = d3.arc()
		    .outerRadius(local.radius)
		    .innerRadius(local.radius * att.innerRadius)
		    .startAngle(function(d) { return d.startAngle + (att.startAngle * (Math.PI/180)); })
	        .endAngle(function(d) { return d.endAngle + (att.startAngle * (Math.PI/180)); });
	};

	return Chart_Pie;
});