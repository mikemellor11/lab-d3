(function (root, factory) {
	"use strict";
	if ( typeof define === 'function' && define.amd ) {
		define('d3', 'shared', 'chart', factory);
	} else if ( typeof exports === 'object' ) {
		module.exports = factory(require('d3'), require('shared'), require('chart'));
	} else {
		root.Chart_Group = factory(root.d3, root.Shared, root.Chart);
	}
})(this || window, function (d3, Shared, Chart) {
	"use strict";

	function Chart_Group(selector){
		if(!selector){
			return null;
		}

		if (!(this instanceof Chart_Group)) { 
			return new Chart_Group(selector);
		}

		Chart.call(this, selector);

		Shared.extend(this.store, {
			levelWidth: {},
			levelHeight: {},
			pA: null, // primaryAxis
			pD: null, // primaryDir
			pX: false // primaryX, shortcut for transforms that still need if/else
		});

		this.att({
			padding: {
				inner: 0.05
			}
		});
	}

	Chart_Group.prototype = Object.create(Chart.prototype);

	Chart_Group.prototype.init = function(){
		var local = this.store, att = local.att, chart = local.chart;

		if(local.inited){
			return this;
		}

		Chart.prototype.init.call(this);

		if(att.scale.x === 'group'){
			local.pA = ['x', 'y'];
			local.pD = 'width';
			local.pX = true;
		} else if(att.scale.y === 'group'){
			local.pA = ['y', 'x'];
			local.pD = 'height';
		}

		// Draw line for custom axis if scale set to group
		if(local.pA){
			chart.select("." + local.pA[0] + ".axis")
				.append("g")
				.attr('class', 'tick')
				.append("line")
				.attr(local.pA[1] + '1', "0.5")
				.attr(local.pA[1] + '2', "0.5");
		}

		return this;
	};

	Chart_Group.prototype._additionalSetup = function(){
		var local = this.store, att = local.att, chart = local.chart;

		if(!att.autoAxis){
			this._calculateAxis('y');
		}

		if(att.axis.y.tickSizeInner == null || att.axis.y.tickSizeInner >= 0){
			if(local.pA){
				var temp = chart.select("." + local.pA[0] + ".axis")
					.attr('transform', 'translate(' + local.margin.left + ', ' + local.margin.top + ')')
					.select(".tick");

				if(local.pX){
					temp.attr('transform', 'translate(0, ' + (att.axis.x.flip ? 0 : local.height) + ')');
				} else {
					temp.attr('transform', 'translate(' + (att.axis.y.flip ? local.width : 0) + ', 0)');
				}
					
				temp.select('line')
					.attr(local.pA[0] + '2', local[local.pD]);
			}
		}
	};

	Chart_Group.prototype._shared = function(enter, update, inner, anim, localData, localIndex, levelIndex, hide){
		var local = this.store, att = local.att, that = this, chart = local.chart;

		if(!local.pA){
			return;
		}

		var outerPad = 0;
		var innerPad = att.padding[levelIndex] || att.padding.inner;
		
		var levelWidth = 0;

		if(levelIndex === local.levels){
			outerPad = local[local.pD] * att.padding.outer;
			levelWidth = local[local.pD] - (outerPad * 2);
		} else {
			levelWidth = local.levelWidth[levelIndex + 1];
		}

		innerPad *= levelWidth;

		[enter, anim].forEach(function(d){
			d.attr("transform", function(d, i){
				var levelPos = (((local.levelWidth[levelIndex] + innerPad) * i) + outerPad);

				if(local.pX){
					return "translate(" + levelPos + ", 0)";
				} else{
					return "translate(0, " + levelPos + ")";
				}
			});
		});

		// ELEMENTS
    	[
	    	[
	    		'text',
	    		'label',
	    		function(element, d, i, status, name, _){
	    			var x = local.levelWidth[levelIndex] * 0.5;
	    			var y = att.spacePadding;

	    			if(status === 'enter'){
						element.attr('dy', '1em');
					}

					if(status === 'enter' || status === 'update'){
	    				element.text(d.label);
	    			}

	    			for(var j = 0; j < levelIndex; j++){
	    				y += local.levelHeight[j];
	    			}

	    			if(local.pX){
		    			if(att.axis.x.rotate){
			    			x -= (local.textHeight * 0.5);
			    			y += (Shared.textWidth(d.label) * 0.5);
			    		} else {
			    			if(att.axis.x.flip){
			    				y += Shared.textHeight(chart, d.label, local.levelWidth[levelIndex]);
			    			}

			    			if(status === 'enter' || status === 'update'){
			    				element.call(Shared.wrap, local.levelWidth[levelIndex]);
			    			}
			    		}

			    		if(att.axis.x.flip){
		    				y *= -1;
		    			} else {
		    				y += local.height;
		    			}

			    		if(status === 'enter' || status === 'anim'){
		    				element.attr('transform', "translate(" + x + ", " + y + ")" + ((att.axis.x.rotate) ? " rotate(-90)" : ""));
		    			}
	    			} else{
		    			if(att.axis.y.rotate){
		    				x -= (local.textHeight * 0.5);
			    			y += (Shared.textWidth(d.label) * 0.5);
			    		} else {
			    			y += Shared.textHeight(chart, d.label, local.levelWidth[levelIndex]);
			    			
			    			if(status === 'enter' || status === 'update'){
			    				element.call(Shared.wrap, local.levelWidth[levelIndex]);
			    			}
			    		}

		    			if(!att.axis.y.flip){
		    				y *= -1;
		    			} else {
		    				y += local.width;
		    			}

			    		if(status === 'enter' || status === 'anim'){
		    				element.attr('transform', "translate(" + y + ", " + x + ")" + ((att.axis.y.rotate) ? "" : " rotate(" + (att.axis.y.flip ? 90 : -90) + ")"));
		    			}
	    			}
	    		}
    		]
    	].forEach(function(element, i){
    		that._buildElements(element, levelIndex, enter, update, hide, null, null, !i);
    	});
	};

	Chart_Group.prototype._calculateAxis = function(scale){
		var local = this.store, att = local.att, data = local.data, chart = local.chart;

		if(!local.pA){
			return Chart.prototype._calculateAxis.call(this, scale);
		} else {
			var calcHeight = 0;

			local.axisHeight = 0;

			if(att.label[scale]){
				local.axisHeight = Shared.textHeight(chart, att.label[scale], local[local.pD]) + att.spacePadding;
			}

			local.plotHeight = 0;

			var levelData = Shared.flattenValuesLengths(data, [], data.length);

			for(var levelIndex = levelData.length; levelIndex--;){
				var outerPad = 0;
				var innerPad = att.padding[levelIndex] || att.padding.inner;
				
				var levelWidth = 0;

				if(levelIndex === local.levels){
					outerPad = local[local.pD] * att.padding.outer;
					levelWidth = local[local.pD] - (outerPad * 2);
				} else {
					levelWidth = local.levelWidth[levelIndex + 1];
				}

				innerPad *= levelWidth;

				var biggestDataset = 0;

				if(levelIndex !== local.levels){
					for(var i = levelData[levelIndex + 1].length; i--;){
						if(levelData[levelIndex + 1][i].values.length > biggestDataset){
							biggestDataset = levelData[levelIndex + 1][i].values.length;
						}
					}
				} else {
					biggestDataset = data.length;
				}

				local.levelWidth[levelIndex] = (levelWidth - (innerPad * (biggestDataset - 1))) / biggestDataset;

				var biggest = 0;
				var tempBiggest = 0;
				var label = '';
				for(var j = levelData[levelIndex].length; j--;){
					var tempLabel = levelData[levelIndex][j].label;
					if(tempLabel){
						tempBiggest = Shared.textWidth(tempLabel);

						if(tempBiggest > biggest){
							biggest = tempBiggest;
							label = tempLabel;
						}
					}
				}

				if(att.axis[local.pA[0]].rotate){
					local.levelHeight[levelIndex] = Shared.textWidth(label) + att.spacePadding;
				} else {
					local.levelHeight[levelIndex] = Shared.textHeight(chart, label, local.levelWidth[levelIndex]) + att.spacePadding;
				}
				local.plotHeight += local.levelHeight[levelIndex];
			}

			calcHeight += local.axisHeight;
			calcHeight += local.plotHeight;

			return calcHeight;
		} 
	};

	return Chart_Group;
});