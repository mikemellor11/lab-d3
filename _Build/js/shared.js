(function (root, factory) {
    "use strict";
    if ( typeof define === 'function' && define.amd ) {
        define('d3', factory);
    } else if ( typeof exports === 'object' ) {
        module.exports = factory(require('d3'));
    } else {
        root.Shared = factory(root.d3);
    }
})(this || window, function (d3) {
    "use strict";

    var element = document.createElement('canvas');
    var context = element.getContext("2d");
    var fontSize = 16;
    var fontFamily = 'Arial';

    if(document.querySelector('.labD3')){
    	var temp = window.getComputedStyle(document.querySelector('.labD3'), null);
    	fontSize = parseFloat(temp.getPropertyValue('font-size'));
    	fontFamily = temp.getPropertyValue('font-family');
    }

    context.font = fontSize + "px " + fontFamily;

	function canavsWidth(txt) {
	    return context.measureText(txt).width;
	}

    function p(x,y){
		return x+" "+y+" ";
	}

    var Shared = {
    	propertyAtString: function(o, s) {
		    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
		    s = s.replace(/^\./, '');           // strip a leading dot
		    var a = s.split('.');
		    for (var i = 0, n = a.length; i < n; ++i) {
		        var k = a[i];
		        if (k in o) {
		            o = o[k];
		        } else {
		            return;
		        }
		    }
		    return o;
		},
		formatKeys: function(format) {
			var args = arguments[1];
			return String(format.replace(/{(\w+)}/g, function(match, key) { 
				return typeof args[key] !== 'undefined' ? args[key] : match;
			}));
		},
		format: function(format) {
		    var args = Array.prototype.slice.call(arguments, 1);
		    return String(format.replace(/{(\d+)}/g, function(match, number) { 
				return typeof args[number] !== 'undefined' ? args[number] : match;
		    }));
		},
		roundedRect: function(x, y, w, h, r1, r2, r3, r4){
			if(r4 == null){return null;}

			var strPath = "M"+p(x+r1,y); //A
			strPath+="L"+p(x+w-r2,y)+"Q"+p(x+w,y)+p(x+w,y+r2); //B
			strPath+="L"+p(x+w,y+h-r3)+"Q"+p(x+w,y+h)+p(x+w-r3,y+h); //C
			strPath+="L"+p(x+r4,y+h)+"Q"+p(x,y+h)+p(x,y+h-r4); //D
			strPath+="L"+p(x,y+r1)+"Q"+p(x,y)+p(x+r1,y); //A
			strPath+="Z";

			return strPath;
		},
		wrap: function(text, width) {
		  text.each(function() {
		    var text = d3.select(this),
		        words = text.text().split(/\s+/).reverse(),
		        word = words.pop(),
		        line = [],
		        lineNumber = 0,
		        lineHeight = 1.1, // ems
		        x = text.attr("x") || 0,
		        y = text.attr("y") || 0,
		        dy = parseFloat(text.attr("dy")) || 0,
		        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

		    while (word) {
		      line.push(word);
		      tspan.text(line.join(" "));
		      if (tspan.node().getComputedTextLength() > width && line.length > 1){
		        line.pop();
		        tspan.text(line.join(" "));
		        line = [word];
		        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
		      }
		      word = words.pop();
		    }
		  });
		},
		ellipse: function(el, text, width) {
			var len = text.length;

			if(width < 1 || len < 1){
				el.textContent = '';
				return;
			} else if(width > canavsWidth(text)){
				el.textContent = text;
				return;
			}

			while(canavsWidth(text.slice(0, len--)) > width){}

			if(len < 1){
				el.textContent = '';
			} else {
				el.textContent = text.slice(0, len) + "...";
			}
		},
		midAngle: function(d, startAngle) {
		    return (d.startAngle + (startAngle * (Math.PI/180))) + (d.endAngle - d.startAngle) / 2;
		},
		getOrderIndex: function(i, order){
			return ((order) ? order[i] : i);
		},
		extend: function (base, json) {
			for(var keys in json){
				if(json.hasOwnProperty(keys)){
					if(Array.isArray(base[keys])){ // Override entire array, so setting colors = ['fillA'] will override ['fillA', 'fillB', 'fillC']
						base[keys] = json[keys];
					} else if(base[keys] && typeof base[keys] === 'object' && !json[keys].nodeName){ // if base[keys] is an object need to recurse and not a dom node element
						this.extend(base[keys], json[keys]);
					} else {
						base[keys] = json[keys];
					}
				}	
			}
		},
		textHeight: function(svg, text, tempWidth){
			var height = 0;

			var temp = svg.append('text')	
	    		.attr('y', '1em')
				.attr('x', 0)
				.text(text);

			if(tempWidth > 0){
				temp.call(this.wrap, tempWidth);
			}
				
			temp.each(function(){
				height = this.getBBox().height;
			})
			.remove();

			return height;
		},
		textWidth: function(text){
		    return canavsWidth(text);
		},
		y: function(d, plot){
			return d[plot];
		},
		x: function(d, plot){
			return d[plot];
		},
		min: function(d, scale, plot, parse){
			return Shared.minmax('min', d, scale, plot, parse);
		},
		max: function(d, scale, plot, parse){
			return Shared.minmax('max', d, scale, plot, parse);
		},
		minmax: function(minmax, d, scale, plot, parse){
			return d3[minmax](d, function(dd){
				var preparsed = false;
				if(dd.hasOwnProperty('values')){
					return Shared.minmax(minmax, dd.values, scale, plot, parse);
				} else {
					var value;

					if(typeof dd === 'object' && !Array.isArray(dd)){
						value = Shared[scale](dd, plot);

						if(dd.hasOwnProperty('minmax')){
							value = Shared[minmax]([value, dd.minmax], scale, plot, parse);
							preparsed = true;
						}
					} else {
						value = dd;
					}

					if(Array.isArray(value)){
						value = value[(minmax === 'min') ? 0 : 1];
					}

					if(!preparsed && parse){
						return parse(value);
					}
					
					return value;
				}
			});
		},
		flattenValues: function(data, levels){
			if(!this.findKey(data, 'values')){
				return [data, 0];
			}

			if(!levels){
				levels = 1;
			}

		    var flatData = [];
		    
		    for(var i = 0, ilen = data.length; i < ilen; i++){
		    	if(data[i].values){
		    		flatData = flatData.concat(data[i].values);
		    	}
		    }

		    if(this.findKey(flatData, 'values')){
		    	var tempFlat = this.flattenValues(flatData, ++levels);
		    	flatData = tempFlat[0];
		    	levels = tempFlat[1];
		    }

		    return [flatData, levels];
		},
		flattenValuesLengths: function(data, levels, index){
			if(!data.length){
				return [];
			}
			
			if(!levels){
				levels = [];
			}

			var flatData = [];

			for(var i = 0, ilen = data.length; i < ilen; i++){
				if(i >= index){ continue; }

				if(data[i].values){
					flatData = flatData.concat(data[i].values);
				}
			}

		    if(flatData.length){
		    	levels.unshift(flatData);
		    }

		    if(this.findKey(flatData, 'values')){
				this.flattenValuesLengths(flatData, levels);
			}

			if(index != null){
				levels.push(data);
			}

		    return levels;
		},
		getKey: function(att, d, i, type){
			var values = att[type];

			if(!Array.isArray(values)){
				return values[this.propertyAtString(d, att[type + 'Key'])];
			}

			return values[(i % values.length)];
		},
		findKey: function(arr, key){
			for(var i = 0, len = arr.length; i < len; i++){
				if(arr[i][key]){
					return true;
				}
			}
			return false;
		}
    };

    return Shared;
});